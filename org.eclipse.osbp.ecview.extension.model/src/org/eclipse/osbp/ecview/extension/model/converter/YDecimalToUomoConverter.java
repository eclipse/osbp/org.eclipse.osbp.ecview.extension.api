/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YDecimal To Uomo Converter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYDecimalToUomoConverter()
 * @model
 * @generated
 */
public interface YDecimalToUomoConverter extends YConverter {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

}

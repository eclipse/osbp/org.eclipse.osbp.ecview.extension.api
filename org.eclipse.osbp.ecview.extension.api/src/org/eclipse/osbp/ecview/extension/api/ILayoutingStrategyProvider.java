/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.api;

/**
 * Is used to provide the default strategy. It needs to be passed in
 * IViewContext.
 */
public interface ILayoutingStrategyProvider {

	/** The Constant ACTIVE_PROVIDER_TOPIC. */
	public static final String ACTIVE_PROVIDER_TOPIC = "org/eclipse/osbp/ecview/layouting/strategyprovider";

	/**
	 * Returns the ILayoutingStrategy that is assigned to this provider.
	 *
	 * @return the strategy
	 */
	ILayoutingStrategy getStrategy();

}

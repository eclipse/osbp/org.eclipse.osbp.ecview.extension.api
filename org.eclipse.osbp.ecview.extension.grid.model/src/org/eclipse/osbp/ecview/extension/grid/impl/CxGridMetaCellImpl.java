/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.impl;

import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.util.CxGridUtil;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YStringToStringMapImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Cell</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#isUseHTML <em>Use HTML</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl#getElement <em>Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridMetaCellImpl extends MinimalEObjectImpl.Container implements CxGridMetaCell {
	
	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected CxGridColumn target;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabelI18nKey() <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelI18nKey()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_I1_8N_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabelI18nKey() <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelI18nKey()
	 * @generated
	 * @ordered
	 */
	protected String labelI18nKey = LABEL_I1_8N_KEY_EDEFAULT;

	/**
	 * The default value of the '{@link #isUseHTML() <em>Use HTML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseHTML()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USE_HTML_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUseHTML() <em>Use HTML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseHTML()
	 * @generated
	 * @ordered
	 */
	protected boolean useHTML = USE_HTML_EDEFAULT;

	/**
	 * The cached value of the '{@link #getElement() <em>Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement()
	 * @generated
	 * @ordered
	 */
	protected YEmbeddable element;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected CxGridMetaCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridPackage.Literals.CX_GRID_META_CELL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getId() <em>Id</em>}' attribute
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newId
	 *            the new cached value of the '{@link #getId() <em>Id</em>}'
	 *            attribute
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getName() <em>Name</em>}'
	 *         attribute
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newName
	 *            the new cached value of the '{@link #getName() <em>Name</em>}'
	 *            attribute
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTags() <em>Tags</em>}'
	 *         attribute list
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, CxGridPackage.CX_GRID_META_CELL__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getProperties()
	 *         <em>Properties</em>}' map
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP, YStringToStringMapImpl.class, this, CxGridPackage.CX_GRID_META_CELL__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTarget() <em>Target</em>}'
	 *         reference
	 * @generated
	 */
	public CxGridColumn getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (CxGridColumn)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CxGridPackage.CX_GRID_META_CELL__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column
	 * @generated
	 */
	public CxGridColumn basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTarget
	 *            the new target
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetTarget(CxGridColumn newTarget, NotificationChain msgs) {
		CxGridColumn oldTarget = target;
		target = newTarget;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__TARGET, oldTarget, newTarget);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTarget
	 *            the new cached value of the '{@link #getTarget()
	 *            <em>Target</em>}' reference
	 * @generated
	 */
	public void setTarget(CxGridColumn newTarget) {
		if (newTarget != target) {
			NotificationChain msgs = null;
			if (target != null)
				msgs = ((InternalEObject)target).eInverseRemove(this, CxGridPackage.CX_GRID_COLUMN__USED_IN_META_CELLS, CxGridColumn.class, msgs);
			if (newTarget != null)
				msgs = ((InternalEObject)newTarget).eInverseAdd(this, CxGridPackage.CX_GRID_COLUMN__USED_IN_META_CELLS, CxGridColumn.class, msgs);
			msgs = basicSetTarget(newTarget, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__TARGET, newTarget, newTarget));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getLabel() <em>Label</em>}'
	 *         attribute
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLabel
	 *            the new cached value of the '{@link #getLabel()
	 *            <em>Label</em>}' attribute
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getLabelI18nKey()
	 *         <em>Label I1 8n Key</em>}' attribute
	 * @generated
	 */
	public String getLabelI18nKey() {
		return labelI18nKey;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLabelI18nKey
	 *            the new cached value of the '{@link #getLabelI18nKey()
	 *            <em>Label I1 8n Key</em>}' attribute
	 * @generated
	 */
	public void setLabelI18nKey(String newLabelI18nKey) {
		String oldLabelI18nKey = labelI18nKey;
		labelI18nKey = newLabelI18nKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__LABEL_I1_8N_KEY, oldLabelI18nKey, labelI18nKey));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #isUseHTML() <em>Use HTML</em>}'
	 *         attribute
	 * @generated
	 */
	public boolean isUseHTML() {
		return useHTML;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newUseHTML
	 *            the new cached value of the '{@link #isUseHTML()
	 *            <em>Use HTML</em>}' attribute
	 * @generated
	 */
	public void setUseHTML(boolean newUseHTML) {
		boolean oldUseHTML = useHTML;
		useHTML = newUseHTML;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__USE_HTML, oldUseHTML, useHTML));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getElement() <em>Element</em>}'
	 *         containment reference
	 * @generated
	 */
	public YEmbeddable getElement() {
		return element;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newElement
	 *            the new element
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetElement(YEmbeddable newElement, NotificationChain msgs) {
		YEmbeddable oldElement = element;
		element = newElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__ELEMENT, oldElement, newElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newElement
	 *            the new cached value of the '{@link #getElement()
	 *            <em>Element</em>}' containment reference
	 * @generated
	 */
	public void setElement(YEmbeddable newElement) {
		if (newElement != element) {
			NotificationChain msgs = null;
			if (element != null)
				msgs = ((InternalEObject)element).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CxGridPackage.CX_GRID_META_CELL__ELEMENT, null, msgs);
			if (newElement != null)
				msgs = ((InternalEObject)newElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CxGridPackage.CX_GRID_META_CELL__ELEMENT, null, msgs);
			msgs = basicSetElement(newElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_META_CELL__ELEMENT, newElement, newElement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_META_CELL__TARGET:
				if (target != null)
					msgs = ((InternalEObject)target).eInverseRemove(this, CxGridPackage.CX_GRID_COLUMN__USED_IN_META_CELLS, CxGridColumn.class, msgs);
				return basicSetTarget((CxGridColumn)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_META_CELL__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case CxGridPackage.CX_GRID_META_CELL__TARGET:
				return basicSetTarget(null, msgs);
			case CxGridPackage.CX_GRID_META_CELL__ELEMENT:
				return basicSetElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_META_CELL__TAGS:
				return getTags();
			case CxGridPackage.CX_GRID_META_CELL__ID:
				return getId();
			case CxGridPackage.CX_GRID_META_CELL__NAME:
				return getName();
			case CxGridPackage.CX_GRID_META_CELL__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
			case CxGridPackage.CX_GRID_META_CELL__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case CxGridPackage.CX_GRID_META_CELL__LABEL:
				return getLabel();
			case CxGridPackage.CX_GRID_META_CELL__LABEL_I1_8N_KEY:
				return getLabelI18nKey();
			case CxGridPackage.CX_GRID_META_CELL__USE_HTML:
				return isUseHTML();
			case CxGridPackage.CX_GRID_META_CELL__ELEMENT:
				return getElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_META_CELL__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__ID:
				setId((String)newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__NAME:
				setName((String)newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__TARGET:
				setTarget((CxGridColumn)newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__LABEL:
				setLabel((String)newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__LABEL_I1_8N_KEY:
				setLabelI18nKey((String)newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__USE_HTML:
				setUseHTML((Boolean)newValue);
				return;
			case CxGridPackage.CX_GRID_META_CELL__ELEMENT:
				setElement((YEmbeddable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_META_CELL__TAGS:
				getTags().clear();
				return;
			case CxGridPackage.CX_GRID_META_CELL__ID:
				setId(ID_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_META_CELL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_META_CELL__PROPERTIES:
				getProperties().clear();
				return;
			case CxGridPackage.CX_GRID_META_CELL__TARGET:
				setTarget((CxGridColumn)null);
				return;
			case CxGridPackage.CX_GRID_META_CELL__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_META_CELL__LABEL_I1_8N_KEY:
				setLabelI18nKey(LABEL_I1_8N_KEY_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_META_CELL__USE_HTML:
				setUseHTML(USE_HTML_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_META_CELL__ELEMENT:
				setElement((YEmbeddable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_META_CELL__TAGS:
				return tags != null && !tags.isEmpty();
			case CxGridPackage.CX_GRID_META_CELL__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case CxGridPackage.CX_GRID_META_CELL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CxGridPackage.CX_GRID_META_CELL__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case CxGridPackage.CX_GRID_META_CELL__TARGET:
				return target != null;
			case CxGridPackage.CX_GRID_META_CELL__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case CxGridPackage.CX_GRID_META_CELL__LABEL_I1_8N_KEY:
				return LABEL_I1_8N_KEY_EDEFAULT == null ? labelI18nKey != null : !LABEL_I1_8N_KEY_EDEFAULT.equals(labelI18nKey);
			case CxGridPackage.CX_GRID_META_CELL__USE_HTML:
				return useHTML != USE_HTML_EDEFAULT;
			case CxGridPackage.CX_GRID_META_CELL__ELEMENT:
				return element != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", label: ");
		result.append(label);
		result.append(", labelI18nKey: ");
		result.append(labelI18nKey);
		result.append(", useHTML: ");
		result.append(useHTML);
		result.append(')');
		return result.toString();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridProvider#getGrid()
	 */
	@Override
	public CxGrid getGrid() {
		return CxGridUtil.getGrid(this);
	}

}
/**
 * All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.
 * 
 * Contributors:
 *       Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.extension.model.converter.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToByteArrayConverter;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YString To Byte Array Converter</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YStringToByteArrayConverterTest extends TestCase {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The fixture for this YString To Byte Array Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YStringToByteArrayConverter fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YStringToByteArrayConverterTest.class);
	}

	/**
	 * Constructs a new YString To Byte Array Converter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YStringToByteArrayConverterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YString To Byte Array Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YStringToByteArrayConverter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YString To Byte Array Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YStringToByteArrayConverter getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(YConverterFactory.eINSTANCE.createYStringToByteArrayConverter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YStringToByteArrayConverterTest

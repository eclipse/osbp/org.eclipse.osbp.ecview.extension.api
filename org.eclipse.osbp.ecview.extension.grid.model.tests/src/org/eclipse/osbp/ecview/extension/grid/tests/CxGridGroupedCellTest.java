/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Grouped Cell</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridGroupedCellTest extends CxGridGroupableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridGroupedCellTest.class);
	}

	/**
	 * Constructs a new Grouped Cell test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridGroupedCellTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Grouped Cell test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CxGridGroupedCell getFixture() {
		return (CxGridGroupedCell)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridFactory.eINSTANCE.createCxGridGroupedCell());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridGroupedCellTest

/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YDelegating Focusing Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy#getDelegateStrategyId <em>Delegate Strategy Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYDelegatingFocusingStrategy()
 * @model
 * @generated
 */
public interface YDelegatingFocusingStrategy extends YFocusingStrategy {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Delegate Strategy Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delegate Strategy Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delegate Strategy Id</em>' attribute.
	 * @see #setDelegateStrategyId(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYDelegatingFocusingStrategy_DelegateStrategyId()
	 * @model required="true"
	 * @generated
	 */
	String getDelegateStrategyId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy#getDelegateStrategyId <em>Delegate Strategy Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delegate Strategy Id</em>' attribute.
	 * @see #getDelegateStrategyId()
	 * @generated
	 */
	void setDelegateStrategyId(String value);

}

/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Number Renderer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNumberFormat <em>Number Format</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNullRepresentation <em>Null Representation</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNumberRenderer()
 * @model
 * @generated
 */
public interface CxGridNumberRenderer extends CxGridRenderer {
	
	/**
	 * Returns the value of the '<em><b>Number Format</b></em>' attribute.
	 * The default value is <code>"#,##0.00"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Format</em>' attribute.
	 * @see #setNumberFormat(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNumberRenderer_NumberFormat()
	 * @model default="#,##0.00"
	 * @generated
	 */
	String getNumberFormat();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNumberFormat <em>Number Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Format</em>' attribute.
	 * @see #getNumberFormat()
	 * @generated
	 */
	void setNumberFormat(String value);

	/**
	 * Returns the value of the '<em><b>Null Representation</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Representation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Representation</em>' attribute.
	 * @see #setNullRepresentation(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNumberRenderer_NullRepresentation()
	 * @model default=""
	 * @generated
	 */
	String getNullRepresentation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNullRepresentation <em>Null Representation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Representation</em>' attribute.
	 * @see #getNullRepresentation()
	 * @generated
	 */
	void setNullRepresentation(String value);

}
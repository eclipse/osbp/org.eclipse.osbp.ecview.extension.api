/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage
 * @generated
 */
public interface CxGridRendererFactory extends EFactory {
	
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CxGridRendererFactory eINSTANCE = org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Cx Grid Delegate Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Delegate Renderer</em>'.
	 * @generated
	 */
	CxGridDelegateRenderer createCxGridDelegateRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Date Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Date Renderer</em>'.
	 * @generated
	 */
	CxGridDateRenderer createCxGridDateRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Html Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Html Renderer</em>'.
	 * @generated
	 */
	CxGridHtmlRenderer createCxGridHtmlRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Number Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Number Renderer</em>'.
	 * @generated
	 */
	CxGridNumberRenderer createCxGridNumberRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Progress Bar Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Progress Bar Renderer</em>'.
	 * @generated
	 */
	CxGridProgressBarRenderer createCxGridProgressBarRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Text Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Text Renderer</em>'.
	 * @generated
	 */
	CxGridTextRenderer createCxGridTextRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Button Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Button Renderer</em>'.
	 * @generated
	 */
	CxGridButtonRenderer createCxGridButtonRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Blob Image Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Blob Image Renderer</em>'.
	 * @generated
	 */
	CxGridBlobImageRenderer createCxGridBlobImageRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Image Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Image Renderer</em>'.
	 * @generated
	 */
	CxGridImageRenderer createCxGridImageRenderer();

	/**
	 * Returns a new object of class '<em>Click Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Click Event</em>'.
	 * @generated
	 */
	CxGridRendererClickEvent createCxGridRendererClickEvent();

	/**
	 * Returns a new object of class '<em>Cx Grid Boolean Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Boolean Renderer</em>'.
	 * @generated
	 */
	CxGridBooleanRenderer createCxGridBooleanRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Quantity Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Quantity Renderer</em>'.
	 * @generated
	 */
	CxGridQuantityRenderer createCxGridQuantityRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Price Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Price Renderer</em>'.
	 * @generated
	 */
	CxGridPriceRenderer createCxGridPriceRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Indicator Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Indicator Renderer</em>'.
	 * @generated
	 */
	CxGridIndicatorRenderer createCxGridIndicatorRenderer();

	/**
	 * Returns a new object of class '<em>Cx Grid Nested Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid Nested Converter</em>'.
	 * @generated
	 */
	CxGridNestedConverter createCxGridNestedConverter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CxGridRendererPackage getCxGridRendererPackage();

}
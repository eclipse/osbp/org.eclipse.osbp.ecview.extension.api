/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.datatypes.DatatypesPackage;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent;
import org.eclipse.osbp.ecview.extension.model.YCustomDecimalField;
import org.eclipse.osbp.ecview.extension.model.YDefaultLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YI18nComboBox;
import org.eclipse.osbp.ecview.extension.model.YIconComboBox;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSubTypeBaseSuspect;
import org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;
import org.eclipse.osbp.ecview.extension.model.YTypedCompoundSuspect;
import org.eclipse.osbp.ecview.extension.model.YTypedSuspect;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityPackage;
import org.eclipse.osbp.ecview.extension.model.visibility.impl.YVisibilityPackageImpl;
//lunifera@80.156.28.28/osbpgit/org.eclipse.osbp.ecview.extension.git
import org.eclipse.osbp.ecview.extension.model.YCollectionSuspect;
import org.eclipse.osbp.ecview.extension.model.YColumnInfo;
import org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout;
import org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField;
import org.eclipse.osbp.ecview.extension.model.YMaskedNumericField;
import org.eclipse.osbp.ecview.extension.model.YMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YPairComboBox;
import org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YQuantityTextField;
import org.eclipse.osbp.ecview.extension.model.YRichTextArea;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class YECviewPackageImpl extends EPackageImpl implements YECviewPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yStrategyLayoutEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yLayoutingStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yDefaultLayoutingStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yFocusingStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yDelegatingLayoutingStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yDelegatingFocusingStrategyEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySuspectEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yLayoutingInfoEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySuspectInfoEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yBlobUploadComponentEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTypedSuspectEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTypedCompoundSuspectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ySubTypeBaseSuspectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ySubTypeSuspectEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yCustomDecimalFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yi18nComboBoxEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yIconComboBoxEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yQuantityTextFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yCollectionSuspectEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yColumnInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yContentSensitiveLayoutEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yRichTextAreaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yMaskedTextFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yPrefixedMaskedTextFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yMaskedNumericFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yMaskedDecimalFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType numberEDataType = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	private EClass yPairComboBoxEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private YECviewPackageImpl() {
		super(eNS_URI, YECviewFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link YECviewPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the YE cview package
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static YECviewPackage init() {
		if (isInited) return (YECviewPackage)EPackage.Registry.INSTANCE.getEPackage(YECviewPackage.eNS_URI);

		// Obtain or create and register package
		YECviewPackageImpl theYECviewPackage = (YECviewPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof YECviewPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new YECviewPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ExtDatatypesPackage.eINSTANCE.eClass();
		ExtensionModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		YConverterPackageImpl theYConverterPackage = (YConverterPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(YConverterPackage.eNS_URI) instanceof YConverterPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(YConverterPackage.eNS_URI) : YConverterPackage.eINSTANCE);
		YVisibilityPackageImpl theYVisibilityPackage = (YVisibilityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(YVisibilityPackage.eNS_URI) instanceof YVisibilityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(YVisibilityPackage.eNS_URI) : YVisibilityPackage.eINSTANCE);

		// Create package meta-data objects
		theYECviewPackage.createPackageContents();
		theYConverterPackage.createPackageContents();
		theYVisibilityPackage.createPackageContents();

		// Initialize created meta-data
		theYECviewPackage.initializePackageContents();
		theYConverterPackage.initializePackageContents();
		theYVisibilityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theYECviewPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(YECviewPackage.eNS_URI, theYECviewPackage);
		return theYECviewPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y strategy layout
	 * @generated
	 */
	public EClass getYStrategyLayout() {
		return yStrategyLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y strategy layout_ layouting strategy
	 * @generated
	 */
	public EReference getYStrategyLayout_LayoutingStrategy() {
		return (EReference)yStrategyLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y strategy layout_ focusing strategies
	 * @generated
	 */
	public EReference getYStrategyLayout_FocusingStrategies() {
		return (EReference)yStrategyLayoutEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y strategy layout_ suspects
	 * @generated
	 */
	public EReference getYStrategyLayout_Suspects() {
		return (EReference)yStrategyLayoutEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y strategy layout_ layouting info
	 * @generated
	 */
	public EReference getYStrategyLayout_LayoutingInfo() {
		return (EReference)yStrategyLayoutEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y strategy layout_ default focusing enhancer id
	 * @generated
	 */
	public EAttribute getYStrategyLayout_DefaultFocusingEnhancerId() {
		return (EAttribute)yStrategyLayoutEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYStrategyLayout_NumberColumns() {
		return (EAttribute)yStrategyLayoutEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYStrategyLayout_SaveAndNew() {
		return (EAttribute)yStrategyLayoutEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting strategy
	 * @generated
	 */
	public EClass getYLayoutingStrategy() {
		return yLayoutingStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting strategy_ trigger
	 * @generated
	 */
	public EAttribute getYLayoutingStrategy_Trigger() {
		return (EAttribute)yLayoutingStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y default layouting strategy
	 * @generated
	 */
	public EClass getYDefaultLayoutingStrategy() {
		return yDefaultLayoutingStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y focusing strategy
	 * @generated
	 */
	public EClass getYFocusingStrategy() {
		return yFocusingStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y focusing strategy_ key stroke definition
	 * @generated
	 */
	public EReference getYFocusingStrategy_KeyStrokeDefinition() {
		return (EReference)yFocusingStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y focusing strategy_ temp stroke definition
	 * @generated
	 */
	public EReference getYFocusingStrategy_TempStrokeDefinition() {
		return (EReference)yFocusingStrategyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y delegating layouting strategy
	 * @generated
	 */
	public EClass getYDelegatingLayoutingStrategy() {
		return yDelegatingLayoutingStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y delegating layouting strategy_ delegate strategy id
	 * @generated
	 */
	public EAttribute getYDelegatingLayoutingStrategy_DelegateStrategyId() {
		return (EAttribute)yDelegatingLayoutingStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y delegating focusing strategy
	 * @generated
	 */
	public EClass getYDelegatingFocusingStrategy() {
		return yDelegatingFocusingStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y delegating focusing strategy_ delegate strategy id
	 * @generated
	 */
	public EAttribute getYDelegatingFocusingStrategy_DelegateStrategyId() {
		return (EAttribute)yDelegatingFocusingStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect
	 * @generated
	 */
	public EClass getYSuspect() {
		return ySuspectEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ label i18n key
	 * @generated
	 */
	public EAttribute getYSuspect_LabelI18nKey() {
		return (EAttribute)ySuspectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ image i18n key
	 * @generated
	 */
	public EAttribute getYSuspect_ImageI18nKey() {
		return (EAttribute)ySuspectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ value binding endpoints
	 * @generated
	 */
	public EReference getYSuspect_ValueBindingEndpoints() {
		return (EReference)ySuspectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ visibility processors
	 * @generated
	 */
	public EReference getYSuspect_VisibilityProcessors() {
		return (EReference)ySuspectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ commands
	 * @generated
	 */
	public EReference getYSuspect_Commands() {
		return (EReference)ySuspectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ assoc newiated elements
	 * @generated
	 */
	public EReference getYSuspect_AssocNewiatedElements() {
		return (EReference)ySuspectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ associated bindings
	 * @generated
	 */
	public EReference getYSuspect_AssociatedBindings() {
		return (EReference)ySuspectEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect_ label
	 * @generated
	 */
	public EAttribute getYSuspect_Label() {
		return (EAttribute)ySuspectEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYSuspect_ContainerValueBindingEndpoint() {
		return (EReference)ySuspectEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSuspect_GroupName() {
		return (EAttribute)ySuspectEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSuspect_StyleName() {
		return (EAttribute)ySuspectEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting info
	 * @generated
	 */
	public EClass getYLayoutingInfo() {
		return yLayoutingInfoEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting info_ layout
	 * @generated
	 */
	public EReference getYLayoutingInfo_Layout() {
		return (EReference)yLayoutingInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting info_ content
	 * @generated
	 */
	public EReference getYLayoutingInfo_Content() {
		return (EReference)yLayoutingInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting info_ active suspect infos
	 * @generated
	 */
	public EReference getYLayoutingInfo_ActiveSuspectInfos() {
		return (EReference)yLayoutingInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting info_ first focus
	 * @generated
	 */
	public EReference getYLayoutingInfo_FirstFocus() {
		return (EReference)yLayoutingInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info
	 * @generated
	 */
	public EClass getYSuspectInfo() {
		return ySuspectInfoEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info_ suspect
	 * @generated
	 */
	public EReference getYSuspectInfo_Suspect() {
		return (EReference)ySuspectInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info_ bindings
	 * @generated
	 */
	public EReference getYSuspectInfo_Bindings() {
		return (EReference)ySuspectInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info_ next focus
	 * @generated
	 */
	public EReference getYSuspectInfo_NextFocus() {
		return (EReference)ySuspectInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info_ previous focus
	 * @generated
	 */
	public EReference getYSuspectInfo_PreviousFocus() {
		return (EReference)ySuspectInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info_ target
	 * @generated
	 */
	public EReference getYSuspectInfo_Target() {
		return (EReference)ySuspectInfoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYSuspectInfo_VisibilityProcessors() {
		return (EReference)ySuspectInfoEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y blob upload component
	 * @generated
	 */
	public EClass getYBlobUploadComponent() {
		return yBlobUploadComponentEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y blob upload component_ value
	 * @generated
	 */
	public EAttribute getYBlobUploadComponent_Value() {
		return (EAttribute)yBlobUploadComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y blob upload component_ display resolution id
	 * @generated
	 */
	public EAttribute getYBlobUploadComponent_DisplayResolutionId() {
		return (EAttribute)yBlobUploadComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYBlobUploadComponent_FirmlyLinked() {
		return (EAttribute)yBlobUploadComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYBlobUploadComponent_UniqueNameEnabled() {
		return (EAttribute)yBlobUploadComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y typed suspect
	 * @generated
	 */
	public EClass getYTypedSuspect() {
		return yTypedSuspectEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y typed suspect_ type qualified name
	 * @generated
	 */
	public EAttribute getYTypedSuspect_TypeQualifiedName() {
		return (EAttribute)yTypedSuspectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y typed suspect_ type
	 * @generated
	 */
	public EAttribute getYTypedSuspect_Type() {
		return (EAttribute)yTypedSuspectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y typed compound suspect
	 * @generated
	 */
	public EClass getYTypedCompoundSuspect() {
		return yTypedCompoundSuspectEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y typed compound suspect_ children
	 * @generated
	 */
	public EReference getYTypedCompoundSuspect_Children() {
		return (EReference)yTypedCompoundSuspectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYSubTypeBaseSuspect() {
		return ySubTypeBaseSuspectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYSubTypeSuspect() {
		return ySubTypeSuspectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYSubTypeSuspect_BeanSlot() {
		return (EReference)ySubTypeSuspectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal field
	 * @generated
	 */
	public EClass getYCustomDecimalField() {
		return yCustomDecimalFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal field_ datatype
	 * @generated
	 */
	public EReference getYCustomDecimalField_Datatype() {
		return (EReference)yCustomDecimalFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal field_ datadescription
	 * @generated
	 */
	public EReference getYCustomDecimalField_Datadescription() {
		return (EReference)yCustomDecimalFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal field_ value
	 * @generated
	 */
	public EAttribute getYCustomDecimalField_Value() {
		return (EAttribute)yCustomDecimalFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYI18nComboBox() {
		return yi18nComboBoxEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYI18nComboBox_Datadescription() {
		return (EReference)yi18nComboBoxEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYI18nComboBox_Datatype() {
		return (EReference)yi18nComboBoxEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYI18nComboBox_Selection() {
		return (EAttribute)yi18nComboBoxEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYI18nComboBox_Type() {
		return (EAttribute)yi18nComboBoxEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYI18nComboBox_EmfNsURI() {
		return (EAttribute)yi18nComboBoxEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYI18nComboBox_TypeQualifiedName() {
		return (EAttribute)yi18nComboBoxEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box
	 * @generated
	 */
	public EClass getYIconComboBox() {
		return yIconComboBoxEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ datadescription
	 * @generated
	 */
	public EReference getYIconComboBox_Datadescription() {
		return (EReference)yIconComboBoxEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ datatype
	 * @generated
	 */
	public EReference getYIconComboBox_Datatype() {
		return (EReference)yIconComboBoxEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ selection
	 * @generated
	 */
	public EAttribute getYIconComboBox_Selection() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ type
	 * @generated
	 */
	public EAttribute getYIconComboBox_Type() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ emf ns uri
	 * @generated
	 */
	public EAttribute getYIconComboBox_EmfNsURI() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ type qualified name
	 * @generated
	 */
	public EAttribute getYIconComboBox_TypeQualifiedName() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ caption property
	 * @generated
	 */
	public EAttribute getYIconComboBox_CaptionProperty() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ image property
	 * @generated
	 */
	public EAttribute getYIconComboBox_ImageProperty() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ description property
	 * @generated
	 */
	public EAttribute getYIconComboBox_DescriptionProperty() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box_ description
	 * @generated
	 */
	public EAttribute getYIconComboBox_Description() {
		return (EAttribute)yIconComboBoxEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity text field
	 * @generated
	 */
	public EClass getYQuantityTextField() {
		return yQuantityTextFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity text field_ datadescription
	 * @generated
	 */
	public EReference getYQuantityTextField_Datadescription() {
		return (EReference)yQuantityTextFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity text field_ value
	 * @generated
	 */
	public EAttribute getYQuantityTextField_Value() {
		return (EAttribute)yQuantityTextFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y collection suspect
	 * @generated
	 */
	public EClass getYCollectionSuspect() {
		return yCollectionSuspectEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y collection suspect_ columns
	 * @generated
	 */
	public EReference getYCollectionSuspect_Columns() {
		return (EReference)yCollectionSuspectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column info
	 * @generated
	 */
	public EClass getYColumnInfo() {
		return yColumnInfoEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column info_ name
	 * @generated
	 */
	public EAttribute getYColumnInfo_Name() {
		return (EAttribute)yColumnInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column info_ type
	 * @generated
	 */
	public EAttribute getYColumnInfo_Type() {
		return (EAttribute)yColumnInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column info_ type qualified name
	 * @generated
	 */
	public EAttribute getYColumnInfo_TypeQualifiedName() {
		return (EAttribute)yColumnInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column info_ properties
	 * @generated
	 */
	public EReference getYColumnInfo_Properties() {
		return (EReference)yColumnInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column info_ label i18n key
	 * @generated
	 */
	public EAttribute getYColumnInfo_LabelI18nKey() {
		return (EAttribute)yColumnInfoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYColumnInfo_SourceType() {
		return (EAttribute)yColumnInfoEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYContentSensitiveLayout() {
		return yContentSensitiveLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYRichTextArea() {
		return yRichTextAreaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYRichTextArea_Datadescription() {
		return (EReference)yRichTextAreaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYRichTextArea_BlobValue() {
		return (EAttribute)yRichTextAreaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYRichTextArea_Value() {
		return (EAttribute)yRichTextAreaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYRichTextArea_UseBlob() {
		return (EAttribute)yRichTextAreaEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYMaskedTextField() {
		return yMaskedTextFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYMaskedTextField_Datadescription() {
		return (EReference)yMaskedTextFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedTextField_Value() {
		return (EAttribute)yMaskedTextFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedTextField_Mask() {
		return (EAttribute)yMaskedTextFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYPrefixedMaskedTextField() {
		return yPrefixedMaskedTextFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYPrefixedMaskedTextField_Datadescription() {
		return (EReference)yPrefixedMaskedTextFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPrefixedMaskedTextField_Value() {
		return (EAttribute)yPrefixedMaskedTextFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPrefixedMaskedTextField_Mask() {
		return (EAttribute)yPrefixedMaskedTextFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYPrefixedMaskedTextField_Prefixes() {
		return (EReference)yPrefixedMaskedTextFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYMaskedNumericField() {
		return yMaskedNumericFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYMaskedNumericField_Datadescription() {
		return (EReference)yMaskedNumericFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedNumericField_Value() {
		return (EAttribute)yMaskedNumericFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedNumericField_Mask() {
		return (EAttribute)yMaskedNumericFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYMaskedDecimalField() {
		return yMaskedDecimalFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYMaskedDecimalField_Datadescription() {
		return (EReference)yMaskedDecimalFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedDecimalField_Value() {
		return (EAttribute)yMaskedDecimalFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedDecimalField_Mask() {
		return (EAttribute)yMaskedDecimalFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedDecimalField_DecimalSeparator() {
		return (EAttribute)yMaskedDecimalFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYMaskedDecimalField_GroupingSeparator() {
		return (EAttribute)yMaskedDecimalFieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNumber() {
		return numberEDataType;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */

	public EClass getYPairComboBox() {
		return yPairComboBoxEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYPairComboBox_Datadescription() {
		return (EReference)yPairComboBoxEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYPairComboBox_Datatype() {
		return (EReference)yPairComboBoxEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_Selection() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_Collection() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_Type() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_EmfNsURI() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_TypeQualifiedName() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_CaptionProperty() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_ImageProperty() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_DescriptionProperty() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPairComboBox_Description() {
		return (EAttribute)yPairComboBoxEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the YE cview factory
	 * @generated
	 */
	public YECviewFactory getYECviewFactory() {
		return (YECviewFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		yStrategyLayoutEClass = createEClass(YSTRATEGY_LAYOUT);
		createEReference(yStrategyLayoutEClass, YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY);
		createEReference(yStrategyLayoutEClass, YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES);
		createEReference(yStrategyLayoutEClass, YSTRATEGY_LAYOUT__SUSPECTS);
		createEReference(yStrategyLayoutEClass, YSTRATEGY_LAYOUT__LAYOUTING_INFO);
		createEAttribute(yStrategyLayoutEClass, YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID);
		createEAttribute(yStrategyLayoutEClass, YSTRATEGY_LAYOUT__NUMBER_COLUMNS);
		createEAttribute(yStrategyLayoutEClass, YSTRATEGY_LAYOUT__SAVE_AND_NEW);

		yLayoutingStrategyEClass = createEClass(YLAYOUTING_STRATEGY);
		createEAttribute(yLayoutingStrategyEClass, YLAYOUTING_STRATEGY__TRIGGER);

		yDefaultLayoutingStrategyEClass = createEClass(YDEFAULT_LAYOUTING_STRATEGY);

		yFocusingStrategyEClass = createEClass(YFOCUSING_STRATEGY);
		createEReference(yFocusingStrategyEClass, YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION);
		createEReference(yFocusingStrategyEClass, YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION);

		yDelegatingLayoutingStrategyEClass = createEClass(YDELEGATING_LAYOUTING_STRATEGY);
		createEAttribute(yDelegatingLayoutingStrategyEClass, YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID);

		yDelegatingFocusingStrategyEClass = createEClass(YDELEGATING_FOCUSING_STRATEGY);
		createEAttribute(yDelegatingFocusingStrategyEClass, YDELEGATING_FOCUSING_STRATEGY__DELEGATE_STRATEGY_ID);

		ySuspectEClass = createEClass(YSUSPECT);
		createEAttribute(ySuspectEClass, YSUSPECT__LABEL_I1_8N_KEY);
		createEAttribute(ySuspectEClass, YSUSPECT__IMAGE_I1_8N_KEY);
		createEReference(ySuspectEClass, YSUSPECT__VALUE_BINDING_ENDPOINTS);
		createEReference(ySuspectEClass, YSUSPECT__VISIBILITY_PROCESSORS);
		createEReference(ySuspectEClass, YSUSPECT__COMMANDS);
		createEReference(ySuspectEClass, YSUSPECT__ASSOC_NEWIATED_ELEMENTS);
		createEReference(ySuspectEClass, YSUSPECT__ASSOCIATED_BINDINGS);
		createEAttribute(ySuspectEClass, YSUSPECT__LABEL);
		createEReference(ySuspectEClass, YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT);
		createEAttribute(ySuspectEClass, YSUSPECT__GROUP_NAME);
		createEAttribute(ySuspectEClass, YSUSPECT__STYLE_NAME);

		yTypedSuspectEClass = createEClass(YTYPED_SUSPECT);
		createEAttribute(yTypedSuspectEClass, YTYPED_SUSPECT__TYPE_QUALIFIED_NAME);
		createEAttribute(yTypedSuspectEClass, YTYPED_SUSPECT__TYPE);

		yTypedCompoundSuspectEClass = createEClass(YTYPED_COMPOUND_SUSPECT);
		createEReference(yTypedCompoundSuspectEClass, YTYPED_COMPOUND_SUSPECT__CHILDREN);

		ySubTypeBaseSuspectEClass = createEClass(YSUB_TYPE_BASE_SUSPECT);

		ySubTypeSuspectEClass = createEClass(YSUB_TYPE_SUSPECT);
		createEReference(ySubTypeSuspectEClass, YSUB_TYPE_SUSPECT__BEAN_SLOT);

		yLayoutingInfoEClass = createEClass(YLAYOUTING_INFO);
		createEReference(yLayoutingInfoEClass, YLAYOUTING_INFO__LAYOUT);
		createEReference(yLayoutingInfoEClass, YLAYOUTING_INFO__CONTENT);
		createEReference(yLayoutingInfoEClass, YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS);
		createEReference(yLayoutingInfoEClass, YLAYOUTING_INFO__FIRST_FOCUS);

		ySuspectInfoEClass = createEClass(YSUSPECT_INFO);
		createEReference(ySuspectInfoEClass, YSUSPECT_INFO__SUSPECT);
		createEReference(ySuspectInfoEClass, YSUSPECT_INFO__BINDINGS);
		createEReference(ySuspectInfoEClass, YSUSPECT_INFO__NEXT_FOCUS);
		createEReference(ySuspectInfoEClass, YSUSPECT_INFO__PREVIOUS_FOCUS);
		createEReference(ySuspectInfoEClass, YSUSPECT_INFO__TARGET);
		createEReference(ySuspectInfoEClass, YSUSPECT_INFO__VISIBILITY_PROCESSORS);

		yBlobUploadComponentEClass = createEClass(YBLOB_UPLOAD_COMPONENT);
		createEAttribute(yBlobUploadComponentEClass, YBLOB_UPLOAD_COMPONENT__VALUE);
		createEAttribute(yBlobUploadComponentEClass, YBLOB_UPLOAD_COMPONENT__DISPLAY_RESOLUTION_ID);
		createEAttribute(yBlobUploadComponentEClass, YBLOB_UPLOAD_COMPONENT__FIRMLY_LINKED);
		createEAttribute(yBlobUploadComponentEClass, YBLOB_UPLOAD_COMPONENT__UNIQUE_NAME_ENABLED);

		yCustomDecimalFieldEClass = createEClass(YCUSTOM_DECIMAL_FIELD);
		createEReference(yCustomDecimalFieldEClass, YCUSTOM_DECIMAL_FIELD__DATATYPE);
		createEReference(yCustomDecimalFieldEClass, YCUSTOM_DECIMAL_FIELD__DATADESCRIPTION);
		createEAttribute(yCustomDecimalFieldEClass, YCUSTOM_DECIMAL_FIELD__VALUE);

		yi18nComboBoxEClass = createEClass(YI1_8N_COMBO_BOX);
		createEReference(yi18nComboBoxEClass, YI1_8N_COMBO_BOX__DATADESCRIPTION);
		createEReference(yi18nComboBoxEClass, YI1_8N_COMBO_BOX__DATATYPE);
		createEAttribute(yi18nComboBoxEClass, YI1_8N_COMBO_BOX__SELECTION);
		createEAttribute(yi18nComboBoxEClass, YI1_8N_COMBO_BOX__TYPE);
		createEAttribute(yi18nComboBoxEClass, YI1_8N_COMBO_BOX__EMF_NS_URI);
		createEAttribute(yi18nComboBoxEClass, YI1_8N_COMBO_BOX__TYPE_QUALIFIED_NAME);

		yIconComboBoxEClass = createEClass(YICON_COMBO_BOX);
		createEReference(yIconComboBoxEClass, YICON_COMBO_BOX__DATADESCRIPTION);
		createEReference(yIconComboBoxEClass, YICON_COMBO_BOX__DATATYPE);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__SELECTION);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__TYPE);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__EMF_NS_URI);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__TYPE_QUALIFIED_NAME);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__CAPTION_PROPERTY);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__IMAGE_PROPERTY);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__DESCRIPTION_PROPERTY);
		createEAttribute(yIconComboBoxEClass, YICON_COMBO_BOX__DESCRIPTION);

		yQuantityTextFieldEClass = createEClass(YQUANTITY_TEXT_FIELD);
		createEReference(yQuantityTextFieldEClass, YQUANTITY_TEXT_FIELD__DATADESCRIPTION);
		createEAttribute(yQuantityTextFieldEClass, YQUANTITY_TEXT_FIELD__VALUE);

		yCollectionSuspectEClass = createEClass(YCOLLECTION_SUSPECT);
		createEReference(yCollectionSuspectEClass, YCOLLECTION_SUSPECT__COLUMNS);

		yColumnInfoEClass = createEClass(YCOLUMN_INFO);
		createEAttribute(yColumnInfoEClass, YCOLUMN_INFO__NAME);
		createEAttribute(yColumnInfoEClass, YCOLUMN_INFO__TYPE);
		createEAttribute(yColumnInfoEClass, YCOLUMN_INFO__TYPE_QUALIFIED_NAME);
		createEReference(yColumnInfoEClass, YCOLUMN_INFO__PROPERTIES);
		createEAttribute(yColumnInfoEClass, YCOLUMN_INFO__LABEL_I1_8N_KEY);
		createEAttribute(yColumnInfoEClass, YCOLUMN_INFO__SOURCE_TYPE);

		yContentSensitiveLayoutEClass = createEClass(YCONTENT_SENSITIVE_LAYOUT);

		yRichTextAreaEClass = createEClass(YRICH_TEXT_AREA);
		createEReference(yRichTextAreaEClass, YRICH_TEXT_AREA__DATADESCRIPTION);
		createEAttribute(yRichTextAreaEClass, YRICH_TEXT_AREA__BLOB_VALUE);
		createEAttribute(yRichTextAreaEClass, YRICH_TEXT_AREA__VALUE);
		createEAttribute(yRichTextAreaEClass, YRICH_TEXT_AREA__USE_BLOB);

		yMaskedTextFieldEClass = createEClass(YMASKED_TEXT_FIELD);
		createEReference(yMaskedTextFieldEClass, YMASKED_TEXT_FIELD__DATADESCRIPTION);
		createEAttribute(yMaskedTextFieldEClass, YMASKED_TEXT_FIELD__VALUE);
		createEAttribute(yMaskedTextFieldEClass, YMASKED_TEXT_FIELD__MASK);

		yPrefixedMaskedTextFieldEClass = createEClass(YPREFIXED_MASKED_TEXT_FIELD);
		createEReference(yPrefixedMaskedTextFieldEClass, YPREFIXED_MASKED_TEXT_FIELD__DATADESCRIPTION);
		createEAttribute(yPrefixedMaskedTextFieldEClass, YPREFIXED_MASKED_TEXT_FIELD__VALUE);
		createEAttribute(yPrefixedMaskedTextFieldEClass, YPREFIXED_MASKED_TEXT_FIELD__MASK);
		createEReference(yPrefixedMaskedTextFieldEClass, YPREFIXED_MASKED_TEXT_FIELD__PREFIXES);

		yMaskedNumericFieldEClass = createEClass(YMASKED_NUMERIC_FIELD);
		createEReference(yMaskedNumericFieldEClass, YMASKED_NUMERIC_FIELD__DATADESCRIPTION);
		createEAttribute(yMaskedNumericFieldEClass, YMASKED_NUMERIC_FIELD__VALUE);
		createEAttribute(yMaskedNumericFieldEClass, YMASKED_NUMERIC_FIELD__MASK);

		yMaskedDecimalFieldEClass = createEClass(YMASKED_DECIMAL_FIELD);
		createEReference(yMaskedDecimalFieldEClass, YMASKED_DECIMAL_FIELD__DATADESCRIPTION);
		createEAttribute(yMaskedDecimalFieldEClass, YMASKED_DECIMAL_FIELD__VALUE);
		createEAttribute(yMaskedDecimalFieldEClass, YMASKED_DECIMAL_FIELD__MASK);
		createEAttribute(yMaskedDecimalFieldEClass, YMASKED_DECIMAL_FIELD__DECIMAL_SEPARATOR);
		createEAttribute(yMaskedDecimalFieldEClass, YMASKED_DECIMAL_FIELD__GROUPING_SEPARATOR);

		yPairComboBoxEClass = createEClass(YPAIR_COMBO_BOX);
		createEReference(yPairComboBoxEClass, YPAIR_COMBO_BOX__DATADESCRIPTION);
		createEReference(yPairComboBoxEClass, YPAIR_COMBO_BOX__DATATYPE);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__SELECTION);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__COLLECTION);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__TYPE);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__EMF_NS_URI);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__TYPE_QUALIFIED_NAME);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__CAPTION_PROPERTY);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__IMAGE_PROPERTY);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__DESCRIPTION_PROPERTY);
		createEAttribute(yPairComboBoxEClass, YPAIR_COMBO_BOX__DESCRIPTION);

		// Create data types
		numberEDataType = createEDataType(NUMBER);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		YConverterPackage theYConverterPackage = (YConverterPackage)EPackage.Registry.INSTANCE.getEPackage(YConverterPackage.eNS_URI);
		YVisibilityPackage theYVisibilityPackage = (YVisibilityPackage)EPackage.Registry.INSTANCE.getEPackage(YVisibilityPackage.eNS_URI);
		CoreModelPackage theCoreModelPackage = (CoreModelPackage)EPackage.Registry.INSTANCE.getEPackage(CoreModelPackage.eNS_URI);
		BindingPackage theBindingPackage = (BindingPackage)EPackage.Registry.INSTANCE.getEPackage(BindingPackage.eNS_URI);
		VisibilityPackage theVisibilityPackage = (VisibilityPackage)EPackage.Registry.INSTANCE.getEPackage(VisibilityPackage.eNS_URI);
		ExtensionModelPackage theExtensionModelPackage = (ExtensionModelPackage)EPackage.Registry.INSTANCE.getEPackage(ExtensionModelPackage.eNS_URI);
		ExtDatatypesPackage theExtDatatypesPackage = (ExtDatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(ExtDatatypesPackage.eNS_URI);
		DatatypesPackage theDatatypesPackage = (DatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theYConverterPackage);
		getESubpackages().add(theYVisibilityPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		yStrategyLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYEmbeddable());
		yLayoutingStrategyEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yDefaultLayoutingStrategyEClass.getESuperTypes().add(this.getYLayoutingStrategy());
		yFocusingStrategyEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yDelegatingLayoutingStrategyEClass.getESuperTypes().add(this.getYLayoutingStrategy());
		yDelegatingFocusingStrategyEClass.getESuperTypes().add(this.getYFocusingStrategy());
		ySuspectEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		ySuspectEClass.getESuperTypes().add(theCoreModelPackage.getYAuthorizationable());
		yTypedSuspectEClass.getESuperTypes().add(this.getYSuspect());
		yTypedCompoundSuspectEClass.getESuperTypes().add(this.getYTypedSuspect());
		ySubTypeBaseSuspectEClass.getESuperTypes().add(this.getYTypedCompoundSuspect());
		ySubTypeSuspectEClass.getESuperTypes().add(this.getYTypedCompoundSuspect());
		yLayoutingInfoEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		ySuspectInfoEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yBlobUploadComponentEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yBlobUploadComponentEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yCustomDecimalFieldEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yCustomDecimalFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yi18nComboBoxEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yi18nComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yi18nComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yIconComboBoxEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yIconComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yIconComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yIconComboBoxEClass.getESuperTypes().add(theExtensionModelPackage.getYBeanServiceConsumer());
		yQuantityTextFieldEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yQuantityTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yCollectionSuspectEClass.getESuperTypes().add(this.getYTypedSuspect());
		yContentSensitiveLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		yContentSensitiveLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		yContentSensitiveLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yRichTextAreaEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yRichTextAreaEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yMaskedTextFieldEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yMaskedTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yPrefixedMaskedTextFieldEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yPrefixedMaskedTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yMaskedNumericFieldEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yMaskedNumericFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yMaskedDecimalFieldEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yMaskedDecimalFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yPairComboBoxEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		yPairComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yPairComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yPairComboBoxEClass.getESuperTypes().add(theExtensionModelPackage.getYBeanServiceConsumer());

		// Initialize classes and features; add operations and parameters
		initEClass(yStrategyLayoutEClass, YStrategyLayout.class, "YStrategyLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYStrategyLayout_LayoutingStrategy(), this.getYLayoutingStrategy(), null, "layoutingStrategy", null, 0, 1, YStrategyLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYStrategyLayout_FocusingStrategies(), this.getYFocusingStrategy(), null, "focusingStrategies", null, 0, -1, YStrategyLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYStrategyLayout_Suspects(), this.getYSuspect(), null, "suspects", null, 0, -1, YStrategyLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYStrategyLayout_LayoutingInfo(), this.getYLayoutingInfo(), null, "layoutingInfo", null, 0, 1, YStrategyLayout.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYStrategyLayout_DefaultFocusingEnhancerId(), ecorePackage.getEString(), "defaultFocusingEnhancerId", null, 0, 1, YStrategyLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYStrategyLayout_NumberColumns(), ecorePackage.getEInt(), "numberColumns", null, 0, 1, YStrategyLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYStrategyLayout_SaveAndNew(), ecorePackage.getEBoolean(), "saveAndNew", null, 0, 1, YStrategyLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yLayoutingStrategyEClass, YLayoutingStrategy.class, "YLayoutingStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYLayoutingStrategy_Trigger(), ecorePackage.getEJavaObject(), "trigger", null, 0, 1, YLayoutingStrategy.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(yLayoutingStrategyEClass, theCoreModelPackage.getYView(), "getView", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(yDefaultLayoutingStrategyEClass, YDefaultLayoutingStrategy.class, "YDefaultLayoutingStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yFocusingStrategyEClass, YFocusingStrategy.class, "YFocusingStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYFocusingStrategy_KeyStrokeDefinition(), theCoreModelPackage.getYKeyStrokeDefinition(), null, "keyStrokeDefinition", null, 0, 1, YFocusingStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYFocusingStrategy_TempStrokeDefinition(), theCoreModelPackage.getYKeyStrokeDefinition(), null, "tempStrokeDefinition", null, 0, 1, YFocusingStrategy.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(yFocusingStrategyEClass, theCoreModelPackage.getYView(), "getView", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(yDelegatingLayoutingStrategyEClass, YDelegatingLayoutingStrategy.class, "YDelegatingLayoutingStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYDelegatingLayoutingStrategy_DelegateStrategyId(), ecorePackage.getEString(), "delegateStrategyId", null, 1, 1, YDelegatingLayoutingStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yDelegatingFocusingStrategyEClass, YDelegatingFocusingStrategy.class, "YDelegatingFocusingStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYDelegatingFocusingStrategy_DelegateStrategyId(), ecorePackage.getEString(), "delegateStrategyId", null, 1, 1, YDelegatingFocusingStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySuspectEClass, YSuspect.class, "YSuspect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYSuspect_LabelI18nKey(), ecorePackage.getEString(), "labelI18nKey", null, 0, 1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuspect_ImageI18nKey(), ecorePackage.getEString(), "imageI18nKey", null, 0, 1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspect_ValueBindingEndpoints(), theBindingPackage.getYBindingEndpoint(), null, "valueBindingEndpoints", null, 0, -1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspect_VisibilityProcessors(), theVisibilityPackage.getYVisibilityProcessor(), null, "visibilityProcessors", null, 0, -1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspect_Commands(), theCoreModelPackage.getYCommand(), null, "commands", null, 0, -1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspect_AssocNewiatedElements(), theCoreModelPackage.getYEmbeddable(), null, "assocNewiatedElements", null, 0, -1, YSuspect.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspect_AssociatedBindings(), theBindingPackage.getYBinding(), null, "associatedBindings", null, 0, -1, YSuspect.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuspect_Label(), ecorePackage.getEString(), "label", null, 0, 1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspect_ContainerValueBindingEndpoint(), theBindingPackage.getYValueBindingEndpoint(), null, "containerValueBindingEndpoint", null, 0, 1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuspect_GroupName(), ecorePackage.getEString(), "groupName", null, 0, 1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuspect_StyleName(), ecorePackage.getEString(), "styleName", null, 0, 1, YSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTypedSuspectEClass, YTypedSuspect.class, "YTypedSuspect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYTypedSuspect_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YTypedSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYTypedSuspect_Type(), g1, "type", null, 0, 1, YTypedSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTypedCompoundSuspectEClass, YTypedCompoundSuspect.class, "YTypedCompoundSuspect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTypedCompoundSuspect_Children(), this.getYSuspect(), null, "children", null, 0, -1, YTypedCompoundSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySubTypeBaseSuspectEClass, YSubTypeBaseSuspect.class, "YSubTypeBaseSuspect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ySubTypeSuspectEClass, YSubTypeSuspect.class, "YSubTypeSuspect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYSubTypeSuspect_BeanSlot(), theCoreModelPackage.getYBeanSlot(), null, "beanSlot", null, 1, 1, YSubTypeSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yLayoutingInfoEClass, YLayoutingInfo.class, "YLayoutingInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYLayoutingInfo_Layout(), this.getYStrategyLayout(), null, "layout", null, 0, 1, YLayoutingInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYLayoutingInfo_Content(), theCoreModelPackage.getYEmbeddable(), null, "content", null, 0, 1, YLayoutingInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYLayoutingInfo_ActiveSuspectInfos(), this.getYSuspectInfo(), null, "activeSuspectInfos", null, 0, -1, YLayoutingInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYLayoutingInfo_FirstFocus(), this.getYSuspectInfo(), null, "firstFocus", null, 0, 1, YLayoutingInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySuspectInfoEClass, YSuspectInfo.class, "YSuspectInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYSuspectInfo_Suspect(), this.getYSuspect(), null, "suspect", null, 0, 1, YSuspectInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspectInfo_Bindings(), theBindingPackage.getYBinding(), null, "bindings", null, 0, -1, YSuspectInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspectInfo_NextFocus(), this.getYSuspectInfo(), this.getYSuspectInfo_PreviousFocus(), "nextFocus", null, 0, 1, YSuspectInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspectInfo_PreviousFocus(), this.getYSuspectInfo(), this.getYSuspectInfo_NextFocus(), "previousFocus", null, 0, 1, YSuspectInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspectInfo_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 0, 1, YSuspectInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuspectInfo_VisibilityProcessors(), theVisibilityPackage.getYVisibilityProcessor(), null, "visibilityProcessors", null, 0, -1, YSuspectInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yBlobUploadComponentEClass, YBlobUploadComponent.class, "YBlobUploadComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYBlobUploadComponent_Value(), ecorePackage.getEString(), "value", null, 0, 1, YBlobUploadComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBlobUploadComponent_DisplayResolutionId(), ecorePackage.getEInt(), "displayResolutionId", null, 0, 1, YBlobUploadComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBlobUploadComponent_FirmlyLinked(), ecorePackage.getEBoolean(), "firmlyLinked", null, 0, 1, YBlobUploadComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBlobUploadComponent_UniqueNameEnabled(), ecorePackage.getEBoolean(), "uniqueNameEnabled", null, 0, 1, YBlobUploadComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yCustomDecimalFieldEClass, YCustomDecimalField.class, "YCustomDecimalField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYCustomDecimalField_Datatype(), theExtDatatypesPackage.getYDecimalDatatype(), null, "datatype", null, 0, 1, YCustomDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYCustomDecimalField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YCustomDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYCustomDecimalField_Value(), ecorePackage.getEDouble(), "value", null, 0, 1, YCustomDecimalField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yi18nComboBoxEClass, YI18nComboBox.class, "YI18nComboBox", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYI18nComboBox_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YI18nComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYI18nComboBox_Datatype(), theExtDatatypesPackage.getYComboBoxDatatype(), null, "datatype", null, 0, 1, YI18nComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYI18nComboBox_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YI18nComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYI18nComboBox_Type(), g1, "type", null, 0, 1, YI18nComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYI18nComboBox_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YI18nComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYI18nComboBox_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YI18nComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yIconComboBoxEClass, YIconComboBox.class, "YIconComboBox", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYIconComboBox_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYIconComboBox_Datatype(), theExtDatatypesPackage.getYComboBoxDatatype(), null, "datatype", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYIconComboBox_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YIconComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYIconComboBox_Type(), g1, "type", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYIconComboBox_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYIconComboBox_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYIconComboBox_CaptionProperty(), ecorePackage.getEString(), "captionProperty", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYIconComboBox_ImageProperty(), ecorePackage.getEString(), "imageProperty", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYIconComboBox_DescriptionProperty(), ecorePackage.getEString(), "descriptionProperty", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYIconComboBox_Description(), ecorePackage.getEString(), "description", null, 0, 1, YIconComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yQuantityTextFieldEClass, YQuantityTextField.class, "YQuantityTextField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYQuantityTextField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YQuantityTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYQuantityTextField_Value(), ecorePackage.getEJavaObject(), "value", null, 0, 1, YQuantityTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yCollectionSuspectEClass, YCollectionSuspect.class, "YCollectionSuspect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYCollectionSuspect_Columns(), this.getYColumnInfo(), null, "columns", null, 0, -1, YCollectionSuspect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yColumnInfoEClass, YColumnInfo.class, "YColumnInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYColumnInfo_Name(), ecorePackage.getEString(), "name", null, 0, 1, YColumnInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYColumnInfo_Type(), g1, "type", null, 0, 1, YColumnInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumnInfo_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YColumnInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYColumnInfo_Properties(), theCoreModelPackage.getYStringToStringMap(), null, "properties", null, 0, -1, YColumnInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumnInfo_LabelI18nKey(), ecorePackage.getEString(), "labelI18nKey", null, 0, 1, YColumnInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYColumnInfo_SourceType(), g1, "sourceType", null, 0, 1, YColumnInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yContentSensitiveLayoutEClass, YContentSensitiveLayout.class, "YContentSensitiveLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yRichTextAreaEClass, YRichTextArea.class, "YRichTextArea", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYRichTextArea_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YRichTextArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYRichTextArea_BlobValue(), ecorePackage.getEByteArray(), "blobValue", null, 0, 1, YRichTextArea.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYRichTextArea_Value(), ecorePackage.getEString(), "value", null, 0, 1, YRichTextArea.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYRichTextArea_UseBlob(), ecorePackage.getEBoolean(), "useBlob", "false", 0, 1, YRichTextArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yMaskedTextFieldEClass, YMaskedTextField.class, "YMaskedTextField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYMaskedTextField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YMaskedTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedTextField_Value(), ecorePackage.getEString(), "value", null, 0, 1, YMaskedTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedTextField_Mask(), ecorePackage.getEString(), "mask", "", 0, 1, YMaskedTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yPrefixedMaskedTextFieldEClass, YPrefixedMaskedTextField.class, "YPrefixedMaskedTextField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYPrefixedMaskedTextField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YPrefixedMaskedTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPrefixedMaskedTextField_Value(), ecorePackage.getEString(), "value", null, 0, 1, YPrefixedMaskedTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPrefixedMaskedTextField_Mask(), ecorePackage.getEString(), "mask", "", 0, 1, YPrefixedMaskedTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYPrefixedMaskedTextField_Prefixes(), theCoreModelPackage.getYStringToStringMap(), null, "prefixes", null, 0, -1, YPrefixedMaskedTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yMaskedNumericFieldEClass, YMaskedNumericField.class, "YMaskedNumericField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYMaskedNumericField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YMaskedNumericField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedNumericField_Value(), this.getNumber(), "value", null, 0, 1, YMaskedNumericField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedNumericField_Mask(), ecorePackage.getEString(), "mask", "", 0, 1, YMaskedNumericField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yMaskedDecimalFieldEClass, YMaskedDecimalField.class, "YMaskedDecimalField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYMaskedDecimalField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YMaskedDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedDecimalField_Value(), this.getNumber(), "value", null, 0, 1, YMaskedDecimalField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedDecimalField_Mask(), ecorePackage.getEString(), "mask", "", 0, 1, YMaskedDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedDecimalField_DecimalSeparator(), ecorePackage.getECharacterObject(), "decimalSeparator", "null", 0, 1, YMaskedDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMaskedDecimalField_GroupingSeparator(), ecorePackage.getECharacterObject(), "groupingSeparator", "null", 0, 1, YMaskedDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yPairComboBoxEClass, YPairComboBox.class, "YPairComboBox", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYPairComboBox_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYPairComboBox_Datatype(), theExtDatatypesPackage.getYComboBoxDatatype(), null, "datatype", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YPairComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, YPairComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYPairComboBox_Type(), g1, "type", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_CaptionProperty(), ecorePackage.getEString(), "captionProperty", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_ImageProperty(), ecorePackage.getEString(), "imageProperty", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_DescriptionProperty(), ecorePackage.getEString(), "descriptionProperty", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPairComboBox_Description(), ecorePackage.getEString(), "description", null, 0, 1, YPairComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(numberEDataType, Number.class, "Number", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

}

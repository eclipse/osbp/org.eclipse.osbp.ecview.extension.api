/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Quantity Renderer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getValuePropertyPath <em>Value Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getUomPropertyPath <em>Uom Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNullRepresentation <em>Null Representation</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getHtmlPattern <em>Html Pattern</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNumberFormat <em>Number Format</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridQuantityRenderer()
 * @model
 * @generated
 */
public interface CxGridQuantityRenderer extends CxGridRenderer {
	
	/**
	 * Returns the value of the '<em><b>Value Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Property Path</em>' attribute.
	 * @see #setValuePropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridQuantityRenderer_ValuePropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getValuePropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getValuePropertyPath <em>Value Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Property Path</em>' attribute.
	 * @see #getValuePropertyPath()
	 * @generated
	 */
	void setValuePropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Uom Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uom Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uom Property Path</em>' attribute.
	 * @see #setUomPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridQuantityRenderer_UomPropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getUomPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getUomPropertyPath <em>Uom Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uom Property Path</em>' attribute.
	 * @see #getUomPropertyPath()
	 * @generated
	 */
	void setUomPropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Null Representation</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Representation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Representation</em>' attribute.
	 * @see #setNullRepresentation(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridQuantityRenderer_NullRepresentation()
	 * @model default=""
	 * @generated
	 */
	String getNullRepresentation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNullRepresentation <em>Null Representation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Representation</em>' attribute.
	 * @see #getNullRepresentation()
	 * @generated
	 */
	void setNullRepresentation(String value);

	/**
	 * Returns the value of the '<em><b>Html Pattern</b></em>' attribute.
	 * The default value is <code>"<b>{@value}</b> <i>{@currency}</i>"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Html Pattern</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Html Pattern</em>' attribute.
	 * @see #setHtmlPattern(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridQuantityRenderer_HtmlPattern()
	 * @model default="<b>{@value}</b> <i>{@currency}</i>" required="true"
	 * @generated
	 */
	String getHtmlPattern();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getHtmlPattern <em>Html Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Html Pattern</em>' attribute.
	 * @see #getHtmlPattern()
	 * @generated
	 */
	void setHtmlPattern(String value);

	/**
	 * Returns the value of the '<em><b>Number Format</b></em>' attribute.
	 * The default value is <code>"#,##0.00"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Format</em>' attribute.
	 * @see #setNumberFormat(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridQuantityRenderer_NumberFormat()
	 * @model default="#,##0.00" required="true"
	 * @generated
	 */
	String getNumberFormat();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNumberFormat <em>Number Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Format</em>' attribute.
	 * @see #getNumberFormat()
	 * @generated
	 */
	void setNumberFormat(String value);

}
/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grouped Cell</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getGroupables <em>Groupables</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#isUseHTML <em>Use HTML</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridGroupedCell()
 * @model
 * @generated
 */
public interface CxGridGroupedCell extends CxGridGroupable {
	
	/**
	 * Returns the value of the '<em><b>Groupables</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Groupables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groupables</em>' reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridGroupedCell_Groupables()
	 * @model
	 * @generated
	 */
	EList<CxGridGroupable> getGroupables();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridGroupedCell_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #setLabelI18nKey(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridGroupedCell_LabelI18nKey()
	 * @model
	 * @generated
	 */
	String getLabelI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabelI18nKey <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #getLabelI18nKey()
	 * @generated
	 */
	void setLabelI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Use HTML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use HTML</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use HTML</em>' attribute.
	 * @see #setUseHTML(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridGroupedCell_UseHTML()
	 * @model
	 * @generated
	 */
	boolean isUseHTML();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#isUseHTML <em>Use HTML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use HTML</em>' attribute.
	 * @see #isUseHTML()
	 * @generated
	 */
	void setUseHTML(boolean value);

}
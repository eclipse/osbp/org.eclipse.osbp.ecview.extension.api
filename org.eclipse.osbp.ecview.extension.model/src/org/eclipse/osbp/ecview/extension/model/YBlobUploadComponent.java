/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.osbp.ecview.core.common.model.core.YValueBindable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YBlob Upload Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getDisplayResolutionId <em>Display Resolution Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isFirmlyLinked <em>Firmly Linked</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isUniqueNameEnabled <em>Unique Name Enabled</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYBlobUploadComponent()
 * @model
 * @generated
 */
public interface YBlobUploadComponent extends YInput, YValueBindable {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYBlobUploadComponent_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Display Resolution Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Resolution Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Resolution Id</em>' attribute.
	 * @see #setDisplayResolutionId(int)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYBlobUploadComponent_DisplayResolutionId()
	 * @model
	 * @generated
	 */
	int getDisplayResolutionId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getDisplayResolutionId <em>Display Resolution Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Resolution Id</em>' attribute.
	 * @see #getDisplayResolutionId()
	 * @generated
	 */
	void setDisplayResolutionId(int value);

	/**
	 * Returns the value of the '<em><b>Firmly Linked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Firmly Linked</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Firmly Linked</em>' attribute.
	 * @see #setFirmlyLinked(boolean)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYBlobUploadComponent_FirmlyLinked()
	 * @model
	 * @generated
	 */
	boolean isFirmlyLinked();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isFirmlyLinked <em>Firmly Linked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Firmly Linked</em>' attribute.
	 * @see #isFirmlyLinked()
	 * @generated
	 */
	void setFirmlyLinked(boolean value);

	/**
	 * Returns the value of the '<em><b>Unique Name Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unique Name Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unique Name Enabled</em>' attribute.
	 * @see #setUniqueNameEnabled(boolean)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYBlobUploadComponent_UniqueNameEnabled()
	 * @model
	 * @generated
	 */
	boolean isUniqueNameEnabled();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isUniqueNameEnabled <em>Unique Name Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unique Name Enabled</em>' attribute.
	 * @see #isUniqueNameEnabled()
	 * @generated
	 */
	void setUniqueNameEnabled(boolean value);

}

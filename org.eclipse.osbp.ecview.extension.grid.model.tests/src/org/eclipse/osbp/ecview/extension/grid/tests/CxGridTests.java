/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>grid</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new CxGridTests("grid Tests");
		suite.addTestSuite(CxGridTest.class);
		suite.addTestSuite(CxGridColumnTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridTests(String name) {
		super(name);
	}

} //CxGridTests

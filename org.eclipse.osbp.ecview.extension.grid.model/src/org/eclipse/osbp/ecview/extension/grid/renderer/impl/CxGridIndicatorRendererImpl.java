/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer.impl;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cx Grid Indicator Renderer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridIndicatorRendererImpl#getRedEnds <em>Red Ends</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridIndicatorRendererImpl#getGreenStarts <em>Green Starts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridIndicatorRendererImpl extends CxGridRendererImpl implements CxGridIndicatorRenderer {
	
	/**
	 * The default value of the '{@link #getRedEnds() <em>Red Ends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRedEnds()
	 * @generated
	 * @ordered
	 */
	protected static final double RED_ENDS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getRedEnds() <em>Red Ends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRedEnds()
	 * @generated
	 * @ordered
	 */
	protected double redEnds = RED_ENDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getGreenStarts() <em>Green Starts</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGreenStarts()
	 * @generated
	 * @ordered
	 */
	protected static final double GREEN_STARTS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getGreenStarts() <em>Green Starts</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGreenStarts()
	 * @generated
	 * @ordered
	 */
	protected double greenStarts = GREEN_STARTS_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected CxGridIndicatorRendererImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridRendererPackage.Literals.CX_GRID_INDICATOR_RENDERER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getRedEnds() <em>Red Ends</em>}'
	 *         attribute
	 * @generated
	 */
	public double getRedEnds() {
		return redEnds;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newRedEnds
	 *            the new cached value of the '{@link #getRedEnds()
	 *            <em>Red Ends</em>}' attribute
	 * @generated
	 */
	public void setRedEnds(double newRedEnds) {
		double oldRedEnds = redEnds;
		redEnds = newRedEnds;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__RED_ENDS, oldRedEnds, redEnds));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getGreenStarts()
	 *         <em>Green Starts</em>}' attribute
	 * @generated
	 */
	public double getGreenStarts() {
		return greenStarts;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newGreenStarts
	 *            the new cached value of the '{@link #getGreenStarts()
	 *            <em>Green Starts</em>}' attribute
	 * @generated
	 */
	public void setGreenStarts(double newGreenStarts) {
		double oldGreenStarts = greenStarts;
		greenStarts = newGreenStarts;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__GREEN_STARTS, oldGreenStarts, greenStarts));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__RED_ENDS:
				return getRedEnds();
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__GREEN_STARTS:
				return getGreenStarts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__RED_ENDS:
				setRedEnds((Double)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__GREEN_STARTS:
				setGreenStarts((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__RED_ENDS:
				setRedEnds(RED_ENDS_EDEFAULT);
				return;
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__GREEN_STARTS:
				setGreenStarts(GREEN_STARTS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__RED_ENDS:
				return redEnds != RED_ENDS_EDEFAULT;
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER__GREEN_STARTS:
				return greenStarts != GREEN_STARTS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (redEnds: ");
		result.append(redEnds);
		result.append(", greenStarts: ");
		result.append(greenStarts);
		result.append(')');
		return result.toString();
	}

}
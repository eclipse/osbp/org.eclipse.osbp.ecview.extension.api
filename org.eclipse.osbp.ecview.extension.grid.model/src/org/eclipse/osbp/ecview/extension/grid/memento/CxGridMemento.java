/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YMemento;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Memento</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getGridId <em>Grid Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isHeaderVisible <em>Header Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFilterVisible <em>Filter Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFooterVisible <em>Footer Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isEditorEnabled <em>Editor Enabled</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getColumns <em>Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getSortOrders <em>Sort Orders</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento()
 * @model
 * @generated
 */
public interface CxGridMemento extends YMemento {
	
	/**
	 * Returns the value of the '<em><b>Grid Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grid Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grid Id</em>' attribute.
	 * @see #setGridId(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento_GridId()
	 * @model required="true"
	 * @generated
	 */
	String getGridId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getGridId <em>Grid Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grid Id</em>' attribute.
	 * @see #getGridId()
	 * @generated
	 */
	void setGridId(String value);

	/**
	 * Returns the value of the '<em><b>Header Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Visible</em>' attribute.
	 * @see #setHeaderVisible(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento_HeaderVisible()
	 * @model
	 * @generated
	 */
	boolean isHeaderVisible();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isHeaderVisible <em>Header Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header Visible</em>' attribute.
	 * @see #isHeaderVisible()
	 * @generated
	 */
	void setHeaderVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Filter Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Visible</em>' attribute.
	 * @see #setFilterVisible(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento_FilterVisible()
	 * @model
	 * @generated
	 */
	boolean isFilterVisible();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFilterVisible <em>Filter Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Visible</em>' attribute.
	 * @see #isFilterVisible()
	 * @generated
	 */
	void setFilterVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Footer Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Footer Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Footer Visible</em>' attribute.
	 * @see #setFooterVisible(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento_FooterVisible()
	 * @model
	 * @generated
	 */
	boolean isFooterVisible();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFooterVisible <em>Footer Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Footer Visible</em>' attribute.
	 * @see #isFooterVisible()
	 * @generated
	 */
	void setFooterVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Editor Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Enabled</em>' attribute.
	 * @see #setEditorEnabled(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento_EditorEnabled()
	 * @model
	 * @generated
	 */
	boolean isEditorEnabled();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isEditorEnabled <em>Editor Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Enabled</em>' attribute.
	 * @see #isEditorEnabled()
	 * @generated
	 */
	void setEditorEnabled(boolean value);

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Columns</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento_Columns()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridMementoColumn> getColumns();

	/**
	 * Returns the value of the '<em><b>Sort Orders</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sort Orders</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sort Orders</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMemento_SortOrders()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridMementoSortable> getSortOrders();

}
/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YKeyStrokeDefinition;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YFocusing Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getKeyStrokeDefinition <em>Key Stroke Definition</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getTempStrokeDefinition <em>Temp Stroke Definition</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYFocusingStrategy()
 * @model
 * @generated
 */
public interface YFocusingStrategy extends YElement {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Key Stroke Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Stroke Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Stroke Definition</em>' containment reference.
	 * @see #setKeyStrokeDefinition(YKeyStrokeDefinition)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYFocusingStrategy_KeyStrokeDefinition()
	 * @model containment="true"
	 * @generated
	 */
	YKeyStrokeDefinition getKeyStrokeDefinition();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getKeyStrokeDefinition <em>Key Stroke Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key Stroke Definition</em>' containment reference.
	 * @see #getKeyStrokeDefinition()
	 * @generated
	 */
	void setKeyStrokeDefinition(YKeyStrokeDefinition value);

	/**
	 * Returns the value of the '<em><b>Temp Stroke Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temp Stroke Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temp Stroke Definition</em>' containment reference.
	 * @see #setTempStrokeDefinition(YKeyStrokeDefinition)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYFocusingStrategy_TempStrokeDefinition()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	YKeyStrokeDefinition getTempStrokeDefinition();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getTempStrokeDefinition <em>Temp Stroke Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temp Stroke Definition</em>' containment reference.
	 * @see #getTempStrokeDefinition()
	 * @generated
	 */
	void setTempStrokeDefinition(YKeyStrokeDefinition value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the view
	 * @model kind="operation"
	 * @generated
	 */
	YView getView();

}

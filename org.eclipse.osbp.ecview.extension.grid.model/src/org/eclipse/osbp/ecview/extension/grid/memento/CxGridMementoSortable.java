/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sortable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#isDescending <em>Descending</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#getColumn <em>Column</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoSortable()
 * @model
 * @generated
 */
public interface CxGridMementoSortable extends EObject {
	
	/**
	 * Returns the value of the '<em><b>Descending</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Descending</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Descending</em>' attribute.
	 * @see #setDescending(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoSortable_Descending()
	 * @model default="false"
	 * @generated
	 */
	boolean isDescending();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#isDescending <em>Descending</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Descending</em>' attribute.
	 * @see #isDescending()
	 * @generated
	 */
	void setDescending(boolean value);

	/**
	 * Returns the value of the '<em><b>Column</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column</em>' reference.
	 * @see #setColumn(CxGridMementoColumn)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoSortable_Column()
	 * @model required="true"
	 * @generated
	 */
	CxGridMementoColumn getColumn();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#getColumn <em>Column</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Column</em>' reference.
	 * @see #getColumn()
	 * @generated
	 */
	void setColumn(CxGridMementoColumn value);

}
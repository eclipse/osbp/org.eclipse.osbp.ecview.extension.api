/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;


import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.provider.YEmbeddableItemProvider;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YStrategyLayoutItemProvider extends YEmbeddableItemProvider {
	
	/**
<<<<<<< HEAD
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
=======
>>>>>>> branch 'development' of ssh://lunifera@80.156.28.28/osbpgit/org.eclipse.osbp.ecview.extension.git
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public YStrategyLayoutItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDefaultFocusingEnhancerIdPropertyDescriptor(object);
			addNumberColumnsPropertyDescriptor(object);
			addSaveAndNewPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Default Focusing Enhancer Id feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultFocusingEnhancerIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YStrategyLayout_defaultFocusingEnhancerId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YStrategyLayout_defaultFocusingEnhancerId_feature", "_UI_YStrategyLayout_type"),
				 YECviewPackage.Literals.YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Columns feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberColumnsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YStrategyLayout_numberColumns_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YStrategyLayout_numberColumns_feature", "_UI_YStrategyLayout_type"),
				 YECviewPackage.Literals.YSTRATEGY_LAYOUT__NUMBER_COLUMNS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Save And New feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSaveAndNewPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YStrategyLayout_saveAndNew_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YStrategyLayout_saveAndNew_feature", "_UI_YStrategyLayout_type"),
				 YECviewPackage.Literals.YSTRATEGY_LAYOUT__SAVE_AND_NEW,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(YECviewPackage.Literals.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY);
			childrenFeatures.add(YECviewPackage.Literals.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES);
			childrenFeatures.add(YECviewPackage.Literals.YSTRATEGY_LAYOUT__SUSPECTS);
			childrenFeatures.add(YECviewPackage.Literals.YSTRATEGY_LAYOUT__LAYOUTING_INFO);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param object
	 *            the object
	 * @param child
	 *            the child
	 * @return the child feature
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YStrategyLayout.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YStrategyLayout"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YStrategyLayout)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YStrategyLayout_type") :
			getString("_UI_YStrategyLayout_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YStrategyLayout.class)) {
			case YECviewPackage.YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID:
			case YECviewPackage.YSTRATEGY_LAYOUT__NUMBER_COLUMNS:
			case YECviewPackage.YSTRATEGY_LAYOUT__SAVE_AND_NEW:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY:
			case YECviewPackage.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES:
			case YECviewPackage.YSTRATEGY_LAYOUT__SUSPECTS:
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTextDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTextAreaDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYNumericDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYDecimalDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTableDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYCheckBoxDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYComboBoxDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYListDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYOptionsGroupDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYBrowserDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYDateTimeDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTreeDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYProgressBarDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTabSheetDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYMasterDetailDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY,
				 YECviewFactory.eINSTANCE.createYLayoutingStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY,
				 YECviewFactory.eINSTANCE.createYDefaultLayoutingStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY,
				 YECviewFactory.eINSTANCE.createYDelegatingLayoutingStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES,
				 YECviewFactory.eINSTANCE.createYFocusingStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES,
				 YECviewFactory.eINSTANCE.createYDelegatingFocusingStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__SUSPECTS,
				 YECviewFactory.eINSTANCE.createYSuspect()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__SUSPECTS,
				 YECviewFactory.eINSTANCE.createYTypedSuspect()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__SUSPECTS,
				 YECviewFactory.eINSTANCE.createYTypedCompoundSuspect()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__SUSPECTS,
				 YECviewFactory.eINSTANCE.createYSubTypeBaseSuspect()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__SUSPECTS,
				 YECviewFactory.eINSTANCE.createYSubTypeSuspect()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__SUSPECTS,
				 YECviewFactory.eINSTANCE.createYCollectionSuspect()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSTRATEGY_LAYOUT__LAYOUTING_INFO,
				 YECviewFactory.eINSTANCE.createYLayoutingInfo()));
	}

}

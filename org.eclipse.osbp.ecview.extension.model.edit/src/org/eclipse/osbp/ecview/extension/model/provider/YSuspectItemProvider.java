/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;


import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YSuspect;

import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityFactory;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.model.YSuspect} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YSuspectItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	
	/**
<<<<<<< HEAD
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
=======
>>>>>>> branch 'development' of ssh://lunifera@80.156.28.28/osbpgit/org.eclipse.osbp.ecview.extension.git
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public YSuspectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addAuthorizationGroupPropertyDescriptor(object);
			addAuthorizationIdPropertyDescriptor(object);
			addLabelI18nKeyPropertyDescriptor(object);
			addImageI18nKeyPropertyDescriptor(object);
			addAssocNewiatedElementsPropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
			addGroupNamePropertyDescriptor(object);
			addStyleNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_id_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_name_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YTaggable_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YTaggable_tags_feature", "_UI_YTaggable_type"),
				 CoreModelPackage.Literals.YTAGGABLE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Authorization Group feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addAuthorizationGroupPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YAuthorizationable_authorizationGroup_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YAuthorizationable_authorizationGroup_feature", "_UI_YAuthorizationable_type"),
				 CoreModelPackage.Literals.YAUTHORIZATIONABLE__AUTHORIZATION_GROUP,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Authorization Id feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addAuthorizationIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YAuthorizationable_authorizationId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YAuthorizationable_authorizationId_feature", "_UI_YAuthorizationable_type"),
				 CoreModelPackage.Literals.YAUTHORIZATIONABLE__AUTHORIZATION_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label I1 8n Key feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addLabelI18nKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspect_labelI18nKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspect_labelI18nKey_feature", "_UI_YSuspect_type"),
				 YECviewPackage.Literals.YSUSPECT__LABEL_I1_8N_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Image I1 8n Key feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addImageI18nKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspect_imageI18nKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspect_imageI18nKey_feature", "_UI_YSuspect_type"),
				 YECviewPackage.Literals.YSUSPECT__IMAGE_I1_8N_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Assoc Newiated Elements feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAssocNewiatedElementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspect_assocNewiatedElements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspect_assocNewiatedElements_feature", "_UI_YSuspect_type"),
				 YECviewPackage.Literals.YSUSPECT__ASSOC_NEWIATED_ELEMENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspect_label_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspect_label_feature", "_UI_YSuspect_type"),
				 YECviewPackage.Literals.YSUSPECT__LABEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Group Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGroupNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspect_groupName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspect_groupName_feature", "_UI_YSuspect_type"),
				 YECviewPackage.Literals.YSUSPECT__GROUP_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Style Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStyleNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspect_styleName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspect_styleName_feature", "_UI_YSuspect_type"),
				 YECviewPackage.Literals.YSUSPECT__STYLE_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YELEMENT__PROPERTIES);
			childrenFeatures.add(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS);
			childrenFeatures.add(YECviewPackage.Literals.YSUSPECT__VISIBILITY_PROCESSORS);
			childrenFeatures.add(YECviewPackage.Literals.YSUSPECT__COMMANDS);
			childrenFeatures.add(YECviewPackage.Literals.YSUSPECT__ASSOCIATED_BINDINGS);
			childrenFeatures.add(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param object
	 *            the object
	 * @param child
	 *            the child
	 * @return the child feature
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YSuspect.gif.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YSuspect"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YSuspect)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YSuspect_type") :
			getString("_UI_YSuspect_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YSuspect.class)) {
			case YECviewPackage.YSUSPECT__TAGS:
			case YECviewPackage.YSUSPECT__ID:
			case YECviewPackage.YSUSPECT__NAME:
			case YECviewPackage.YSUSPECT__AUTHORIZATION_GROUP:
			case YECviewPackage.YSUSPECT__AUTHORIZATION_ID:
			case YECviewPackage.YSUSPECT__LABEL_I1_8N_KEY:
			case YECviewPackage.YSUSPECT__IMAGE_I1_8N_KEY:
			case YECviewPackage.YSUSPECT__LABEL:
			case YECviewPackage.YSUSPECT__GROUP_NAME:
			case YECviewPackage.YSUSPECT__STYLE_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case YECviewPackage.YSUSPECT__PROPERTIES:
			case YECviewPackage.YSUSPECT__VALUE_BINDING_ENDPOINTS:
			case YECviewPackage.YSUSPECT__VISIBILITY_PROCESSORS:
			case YECviewPackage.YSUSPECT__COMMANDS:
			case YECviewPackage.YSUSPECT__ASSOCIATED_BINDINGS:
			case YECviewPackage.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YELEMENT__PROPERTIES,
				 CoreModelFactory.eINSTANCE.create(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYBeanValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYDetailValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYEnumListBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYECViewModelValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYECViewModelListBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYVisibilityProcessorValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYNoOpValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 BindingFactory.eINSTANCE.createYNoOpListBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYContextValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYBeanSlotValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYBeanSlotListBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYEmbeddableValueEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYEmbeddableSelectionEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYEmbeddableMultiSelectionEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYEmbeddableCollectionEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS,
				 CoreModelFactory.eINSTANCE.createYActivatedEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VISIBILITY_PROCESSORS,
				 YVisibilityFactory.eINSTANCE.createYAuthorizationVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VISIBILITY_PROCESSORS,
				 YVisibilityFactory.eINSTANCE.createYSubTypeVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VISIBILITY_PROCESSORS,
				 VisibilityFactory.eINSTANCE.createYVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__VISIBILITY_PROCESSORS,
				 ExtensionModelFactory.eINSTANCE.createYKanbanVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__COMMANDS,
				 CoreModelFactory.eINSTANCE.createYOpenDialogCommand()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__COMMANDS,
				 CoreModelFactory.eINSTANCE.createYSendEventCommand()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__COMMANDS,
				 ExtensionModelFactory.eINSTANCE.createYAddToTableCommand()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__COMMANDS,
				 ExtensionModelFactory.eINSTANCE.createYRemoveFromTableCommand()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__COMMANDS,
				 ExtensionModelFactory.eINSTANCE.createYSetNewBeanInstanceCommand()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__ASSOCIATED_BINDINGS,
				 BindingFactory.eINSTANCE.createYValueBinding()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__ASSOCIATED_BINDINGS,
				 BindingFactory.eINSTANCE.createYListBinding()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYBeanValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYDetailValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYECViewModelValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYVisibilityProcessorValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYNoOpValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYContextValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYBeanSlotValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYEmbeddableValueEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYEmbeddableSelectionEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYActivatedEndpoint()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == YECviewPackage.Literals.YSUSPECT__VALUE_BINDING_ENDPOINTS ||
			childFeature == YECviewPackage.Literals.YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the resource locator
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}

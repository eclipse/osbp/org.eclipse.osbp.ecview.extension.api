/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Delegate Cell Style Generator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridDelegateCellStyleGeneratorTest extends CxGridCellStyleGeneratorTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridDelegateCellStyleGeneratorTest.class);
	}

	/**
	 * Constructs a new Delegate Cell Style Generator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridDelegateCellStyleGeneratorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Delegate Cell Style Generator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CxGridDelegateCellStyleGenerator getFixture() {
		return (CxGridDelegateCellStyleGenerator)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridFactory.eINSTANCE.createCxGridDelegateCellStyleGenerator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridDelegateCellStyleGeneratorTest

/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;

import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Button Renderer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getNullRepresentation <em>Null Representation</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getLastClickEvent <em>Last Click Event</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getEventTopic <em>Event Topic</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridButtonRenderer()
 * @model
 * @generated
 */
public interface CxGridButtonRenderer extends CxGridRenderer {
	
	/**
	 * Returns the value of the '<em><b>Null Representation</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Representation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Representation</em>' attribute.
	 * @see #setNullRepresentation(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridButtonRenderer_NullRepresentation()
	 * @model default=""
	 * @generated
	 */
	String getNullRepresentation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getNullRepresentation <em>Null Representation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Representation</em>' attribute.
	 * @see #getNullRepresentation()
	 * @generated
	 */
	void setNullRepresentation(String value);

	/**
	 * Returns the value of the '<em><b>Last Click Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Click Event</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Click Event</em>' containment reference.
	 * @see #setLastClickEvent(CxGridRendererClickEvent)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridButtonRenderer_LastClickEvent()
	 * @model containment="true"
	 * @generated
	 */
	CxGridRendererClickEvent getLastClickEvent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getLastClickEvent <em>Last Click Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Click Event</em>' containment reference.
	 * @see #getLastClickEvent()
	 * @generated
	 */
	void setLastClickEvent(CxGridRendererClickEvent value);

	/**
	 * Returns the value of the '<em><b>Event Topic</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Topic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Topic</em>' attribute.
	 * @see #setEventTopic(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridButtonRenderer_EventTopic()
	 * @model default=""
	 * @generated
	 */
	String getEventTopic();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getEventTopic <em>Event Topic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Topic</em>' attribute.
	 * @see #getEventTopic()
	 * @generated
	 */
	void setEventTopic(String value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the YEC view model value binding endpoint
	 * @model
	 * @generated
	 */
	YECViewModelValueBindingEndpoint createLastClickEventEndpoint();

}
/**
 * All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.
 * 
 * Contributors:
 *       Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.extension.model.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YI18nComboBox;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YI1 8n Combo Box</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint() <em>Create Collection Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YI18nComboBoxTest extends TestCase {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The fixture for this YI1 8n Combo Box test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YI18nComboBox fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YI18nComboBoxTest.class);
	}

	/**
	 * Constructs a new YI1 8n Combo Box test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YI18nComboBoxTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YI1 8n Combo Box test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YI18nComboBox fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YI1 8n Combo Box test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YI18nComboBox getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(YECviewFactory.eINSTANCE.createYI18nComboBox());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint()
	 * @generated
	 */
	public void testCreateSelectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint() <em>Create Collection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint()
	 * @generated
	 */
	public void testCreateCollectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YI18nComboBoxTest

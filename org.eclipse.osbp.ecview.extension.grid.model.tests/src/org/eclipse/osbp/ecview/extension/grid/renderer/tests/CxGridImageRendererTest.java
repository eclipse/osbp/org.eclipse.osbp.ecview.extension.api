/**
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cx Grid Image Renderer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer#createLastClickEventEndpoint() <em>Create Last Click Event Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class CxGridImageRendererTest extends CxGridRendererTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridImageRendererTest.class);
	}

	/**
	 * Constructs a new Cx Grid Image Renderer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridImageRendererTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Cx Grid Image Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CxGridImageRenderer getFixture() {
		return (CxGridImageRenderer)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridRendererFactory.eINSTANCE.createCxGridImageRenderer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer#createLastClickEventEndpoint() <em>Create Last Click Event Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer#createLastClickEventEndpoint()
	 * @generated
	 */
	public void testCreateLastClickEventEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //CxGridImageRendererTest

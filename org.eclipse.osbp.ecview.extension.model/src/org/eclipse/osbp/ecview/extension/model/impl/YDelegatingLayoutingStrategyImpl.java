/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YDelegating Layouting Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YDelegatingLayoutingStrategyImpl#getDelegateStrategyId <em>Delegate Strategy Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YDelegatingLayoutingStrategyImpl extends YLayoutingStrategyImpl implements YDelegatingLayoutingStrategy {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The default value of the '{@link #getDelegateStrategyId() <em>Delegate Strategy Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelegateStrategyId()
	 * @generated
	 * @ordered
	 */
	protected static final String DELEGATE_STRATEGY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDelegateStrategyId() <em>Delegate Strategy Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelegateStrategyId()
	 * @generated
	 * @ordered
	 */
	protected String delegateStrategyId = DELEGATE_STRATEGY_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YDelegatingLayoutingStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YECviewPackage.Literals.YDELEGATING_LAYOUTING_STRATEGY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getDelegateStrategyId()
	 *         <em>Delegate Strategy Id</em>}' attribute
	 * @generated
	 */
	public String getDelegateStrategyId() {
		return delegateStrategyId;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newDelegateStrategyId
	 *            the new cached value of the '{@link #getDelegateStrategyId()
	 *            <em>Delegate Strategy Id</em>}' attribute
	 * @generated
	 */
	public void setDelegateStrategyId(String newDelegateStrategyId) {
		String oldDelegateStrategyId = delegateStrategyId;
		delegateStrategyId = newDelegateStrategyId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID, oldDelegateStrategyId, delegateStrategyId));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID:
				return getDelegateStrategyId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID:
				setDelegateStrategyId((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID:
				setDelegateStrategyId(DELEGATE_STRATEGY_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID:
				return DELEGATE_STRATEGY_ID_EDEFAULT == null ? delegateStrategyId != null : !DELEGATE_STRATEGY_ID_EDEFAULT.equals(delegateStrategyId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (delegateStrategyId: ");
		result.append(delegateStrategyId);
		result.append(')');
		return result.toString();
	}

}

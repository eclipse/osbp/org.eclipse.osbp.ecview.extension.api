/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.util;

import java.lang.reflect.Field;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.runtime.common.annotations.DtoUtils;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization.Action;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization.Group;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;

/**
 * The Class CxGridUtil.
 */
public class CxGridUtil {

	/**
	 * Returns the grid.
	 *
	 * @param eObject
	 *            the e object
	 * @return the grid
	 */
	public static CxGrid getGrid(EObject eObject) {
		if (eObject == null) {
			return null;
		}

		if (eObject instanceof CxGrid) {
			return (CxGrid) eObject;
		} else {
			return getGrid(eObject.eContainer());
		}
	}
	
	
	/**
	 * Modifies the columns of the grid due to the existing user permissions
	 *  
	 * @param userAccessService
	 * @param grid
	 * @return
	 */
	public static CxGrid setPermissions(IUserAccessService userAccessService, CxGrid grid) {
		if (userAccessService == null) {
			grid.setVisible(true);
		} else {
			Class<?> dtoObjectClass = grid.getType();
			for (CxGridColumn column : grid.getColumns()) {
				String dtoProperty = column.getPropertyId();
				if (column.getSourceType() != null) {
					dtoObjectClass = column.getSourceType();
				}
				columnPermission(userAccessService, dtoObjectClass, column, dtoProperty);
			}
		}
		return grid;
	}


	private static void columnPermission(IUserAccessService userAccessService, Class<?> dtoObjectClass, CxGridColumn column, String dtoProperty) {
		if (dtoProperty.split("\\.").length > 1) {
			String refName = dtoProperty.split("\\.")[0];
			String newDtoProperty = dtoProperty.substring(dtoProperty.indexOf(".") + 1);
			columnPermission(userAccessService, dtoObjectClass, column, newDtoProperty);
			for (Field ref : DtoUtils.getOwnerDomainReferences(dtoObjectClass)){
				if (refName.equals(ref.getName())){
					Class<?> newDtoObjectClass = ref.getType();
					columnPermission(userAccessService, newDtoObjectClass, column, newDtoProperty);
					break;
				}
			}
		} else {
			String dtoName = dtoObjectClass.getCanonicalName();
			if (userAccessService.isGranted(Group.DTO, Action.READABLE, dtoName)) {
				boolean columnHidden = userAccessService.isVetoed(Group.DTO, Action.INVISIBLE ,dtoName, dtoProperty);
				if (!columnHidden) {
					boolean columnEditable = !userAccessService.isVetoed(Group.DTO, Action.NONEDITABLE ,dtoName, dtoProperty);
					boolean columnEnabled = !userAccessService.isVetoed(Group.DTO, Action.DISABLED ,dtoName, dtoProperty);
					if(column.isEditable()) {
						// do not set editable true, if editable false
						column.setEditable(columnEditable && columnEnabled);
					}
				} else {
					if(!column.isHidden()){
						// do not unhide hidden columns
						column.setHidden(columnHidden);
					}
				}
			} else {
				column.setHidden(true);
			}
		}
	}

}

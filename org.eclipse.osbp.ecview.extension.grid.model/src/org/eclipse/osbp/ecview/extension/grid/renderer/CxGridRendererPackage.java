/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory
 * @model kind="package"
 * @generated
 */
public interface CxGridRendererPackage extends EPackage {
	
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "renderer";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.de/ecview/v1/extension/grid/renderer";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "renderer";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CxGridRendererPackage eINSTANCE = org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererImpl <em>Cx Grid Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridRenderer()
	 * @generated
	 */
	int CX_GRID_RENDERER = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The number of structural features of the '<em>Cx Grid Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDelegateRendererImpl <em>Cx Grid Delegate Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDelegateRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridDelegateRenderer()
	 * @generated
	 */
	int CX_GRID_DELEGATE_RENDERER = 1;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Delegate Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_RENDERER__DELEGATE_ID = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cx Grid Delegate Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDateRendererImpl <em>Cx Grid Date Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDateRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridDateRenderer()
	 * @generated
	 */
	int CX_GRID_DATE_RENDERER = 2;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DATE_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DATE_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DATE_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DATE_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Date Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DATE_RENDERER__DATE_FORMAT = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cx Grid Date Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DATE_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridHtmlRendererImpl <em>Cx Grid Html Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridHtmlRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridHtmlRenderer()
	 * @generated
	 */
	int CX_GRID_HTML_RENDERER = 3;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HTML_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HTML_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HTML_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HTML_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Null Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HTML_RENDERER__NULL_REPRESENTATION = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cx Grid Html Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HTML_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNumberRendererImpl <em>Cx Grid Number Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNumberRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridNumberRenderer()
	 * @generated
	 */
	int CX_GRID_NUMBER_RENDERER = 4;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NUMBER_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NUMBER_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NUMBER_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NUMBER_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Number Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NUMBER_RENDERER__NUMBER_FORMAT = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Null Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NUMBER_RENDERER__NULL_REPRESENTATION = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cx Grid Number Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NUMBER_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridProgressBarRendererImpl <em>Cx Grid Progress Bar Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridProgressBarRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridProgressBarRenderer()
	 * @generated
	 */
	int CX_GRID_PROGRESS_BAR_RENDERER = 5;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PROGRESS_BAR_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PROGRESS_BAR_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PROGRESS_BAR_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PROGRESS_BAR_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Max Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PROGRESS_BAR_RENDERER__MAX_VALUE = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cx Grid Progress Bar Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PROGRESS_BAR_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridTextRendererImpl <em>Cx Grid Text Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridTextRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridTextRenderer()
	 * @generated
	 */
	int CX_GRID_TEXT_RENDERER = 6;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_TEXT_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_TEXT_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_TEXT_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_TEXT_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Null Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_TEXT_RENDERER__NULL_REPRESENTATION = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cx Grid Text Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_TEXT_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridButtonRendererImpl <em>Cx Grid Button Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridButtonRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridButtonRenderer()
	 * @generated
	 */
	int CX_GRID_BUTTON_RENDERER = 7;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Null Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER__NULL_REPRESENTATION = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Last Click Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER__LAST_CLICK_EVENT = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event Topic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER__EVENT_TOPIC = CX_GRID_RENDERER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cx Grid Button Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BUTTON_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBlobImageRendererImpl <em>Cx Grid Blob Image Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBlobImageRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridBlobImageRenderer()
	 * @generated
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER = 8;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Last Click Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER__LAST_CLICK_EVENT = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Event Topic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER__EVENT_TOPIC = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cx Grid Blob Image Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BLOB_IMAGE_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridImageRendererImpl <em>Cx Grid Image Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridImageRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridImageRenderer()
	 * @generated
	 */
	int CX_GRID_IMAGE_RENDERER = 9;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_IMAGE_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_IMAGE_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_IMAGE_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_IMAGE_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Last Click Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_IMAGE_RENDERER__LAST_CLICK_EVENT = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Event Topic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_IMAGE_RENDERER__EVENT_TOPIC = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cx Grid Image Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_IMAGE_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererClickEventImpl <em>Click Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererClickEventImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridRendererClickEvent()
	 * @generated
	 */
	int CX_GRID_RENDERER_CLICK_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Renderer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER_CLICK_EVENT__RENDERER = 0;

	/**
	 * The feature id for the '<em><b>Last Click Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER_CLICK_EVENT__LAST_CLICK_TIME = 1;

	/**
	 * The number of structural features of the '<em>Click Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_RENDERER_CLICK_EVENT_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBooleanRendererImpl <em>Cx Grid Boolean Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBooleanRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridBooleanRenderer()
	 * @generated
	 */
	int CX_GRID_BOOLEAN_RENDERER = 11;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BOOLEAN_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BOOLEAN_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BOOLEAN_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BOOLEAN_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The number of structural features of the '<em>Cx Grid Boolean Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_BOOLEAN_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridQuantityRendererImpl <em>Cx Grid Quantity Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridQuantityRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridQuantityRenderer()
	 * @generated
	 */
	int CX_GRID_QUANTITY_RENDERER = 12;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Value Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__VALUE_PROPERTY_PATH = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uom Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__UOM_PROPERTY_PATH = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Null Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__NULL_REPRESENTATION = CX_GRID_RENDERER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Html Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__HTML_PATTERN = CX_GRID_RENDERER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Number Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER__NUMBER_FORMAT = CX_GRID_RENDERER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Cx Grid Quantity Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_QUANTITY_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridPriceRendererImpl <em>Cx Grid Price Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridPriceRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridPriceRenderer()
	 * @generated
	 */
	int CX_GRID_PRICE_RENDERER = 13;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Value Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__VALUE_PROPERTY_PATH = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Currency Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__CURRENCY_PROPERTY_PATH = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Null Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__NULL_REPRESENTATION = CX_GRID_RENDERER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Html Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__HTML_PATTERN = CX_GRID_RENDERER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Number Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER__NUMBER_FORMAT = CX_GRID_RENDERER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Cx Grid Price Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PRICE_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 5;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridIndicatorRendererImpl <em>Cx Grid Indicator Renderer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridIndicatorRendererImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridIndicatorRenderer()
	 * @generated
	 */
	int CX_GRID_INDICATOR_RENDERER = 14;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_INDICATOR_RENDERER__TAGS = CX_GRID_RENDERER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_INDICATOR_RENDERER__ID = CX_GRID_RENDERER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_INDICATOR_RENDERER__NAME = CX_GRID_RENDERER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_INDICATOR_RENDERER__PROPERTIES = CX_GRID_RENDERER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Red Ends</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_INDICATOR_RENDERER__RED_ENDS = CX_GRID_RENDERER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Green Starts</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_INDICATOR_RENDERER__GREEN_STARTS = CX_GRID_RENDERER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cx Grid Indicator Renderer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_INDICATOR_RENDERER_FEATURE_COUNT = CX_GRID_RENDERER_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl <em>Cx Grid Nested Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridNestedConverter()
	 * @generated
	 */
	int CX_GRID_NESTED_CONVERTER = 15;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Nested Dot Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__BASE_TYPE = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Nested Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__NESTED_TYPE = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Nested Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Nested Type Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Cx Grid Nested Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_NESTED_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 6;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer <em>Cx Grid Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer
	 * @generated
	 */
	EClass getCxGridRenderer();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer <em>Cx Grid Delegate Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Delegate Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer
	 * @generated
	 */
	EClass getCxGridDelegateRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer#getDelegateId <em>Delegate Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delegate Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer#getDelegateId()
	 * @see #getCxGridDelegateRenderer()
	 * @generated
	 */
	EAttribute getCxGridDelegateRenderer_DelegateId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer <em>Cx Grid Date Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Date Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer
	 * @generated
	 */
	EClass getCxGridDateRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer#getDateFormat <em>Date Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date Format</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer#getDateFormat()
	 * @see #getCxGridDateRenderer()
	 * @generated
	 */
	EAttribute getCxGridDateRenderer_DateFormat();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer <em>Cx Grid Html Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Html Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer
	 * @generated
	 */
	EClass getCxGridHtmlRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer#getNullRepresentation <em>Null Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Null Representation</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer#getNullRepresentation()
	 * @see #getCxGridHtmlRenderer()
	 * @generated
	 */
	EAttribute getCxGridHtmlRenderer_NullRepresentation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer <em>Cx Grid Number Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Number Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer
	 * @generated
	 */
	EClass getCxGridNumberRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNumberFormat <em>Number Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Format</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNumberFormat()
	 * @see #getCxGridNumberRenderer()
	 * @generated
	 */
	EAttribute getCxGridNumberRenderer_NumberFormat();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNullRepresentation <em>Null Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Null Representation</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer#getNullRepresentation()
	 * @see #getCxGridNumberRenderer()
	 * @generated
	 */
	EAttribute getCxGridNumberRenderer_NullRepresentation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer <em>Cx Grid Progress Bar Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Progress Bar Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer
	 * @generated
	 */
	EClass getCxGridProgressBarRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer#getMaxValue <em>Max Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer#getMaxValue()
	 * @see #getCxGridProgressBarRenderer()
	 * @generated
	 */
	EAttribute getCxGridProgressBarRenderer_MaxValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer <em>Cx Grid Text Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Text Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer
	 * @generated
	 */
	EClass getCxGridTextRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer#getNullRepresentation <em>Null Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Null Representation</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer#getNullRepresentation()
	 * @see #getCxGridTextRenderer()
	 * @generated
	 */
	EAttribute getCxGridTextRenderer_NullRepresentation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer <em>Cx Grid Button Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Button Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer
	 * @generated
	 */
	EClass getCxGridButtonRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getNullRepresentation <em>Null Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Null Representation</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getNullRepresentation()
	 * @see #getCxGridButtonRenderer()
	 * @generated
	 */
	EAttribute getCxGridButtonRenderer_NullRepresentation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getLastClickEvent <em>Last Click Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Last Click Event</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getLastClickEvent()
	 * @see #getCxGridButtonRenderer()
	 * @generated
	 */
	EReference getCxGridButtonRenderer_LastClickEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getEventTopic <em>Event Topic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Topic</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer#getEventTopic()
	 * @see #getCxGridButtonRenderer()
	 * @generated
	 */
	EAttribute getCxGridButtonRenderer_EventTopic();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer <em>Cx Grid Blob Image Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Blob Image Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer
	 * @generated
	 */
	EClass getCxGridBlobImageRenderer();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer#getLastClickEvent <em>Last Click Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Last Click Event</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer#getLastClickEvent()
	 * @see #getCxGridBlobImageRenderer()
	 * @generated
	 */
	EReference getCxGridBlobImageRenderer_LastClickEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer#getEventTopic <em>Event Topic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Topic</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer#getEventTopic()
	 * @see #getCxGridBlobImageRenderer()
	 * @generated
	 */
	EAttribute getCxGridBlobImageRenderer_EventTopic();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer <em>Cx Grid Image Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Image Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer
	 * @generated
	 */
	EClass getCxGridImageRenderer();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer#getLastClickEvent <em>Last Click Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Last Click Event</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer#getLastClickEvent()
	 * @see #getCxGridImageRenderer()
	 * @generated
	 */
	EReference getCxGridImageRenderer_LastClickEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer#getEventTopic <em>Event Topic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Topic</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer#getEventTopic()
	 * @see #getCxGridImageRenderer()
	 * @generated
	 */
	EAttribute getCxGridImageRenderer_EventTopic();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent <em>Click Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Click Event</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent
	 * @generated
	 */
	EClass getCxGridRendererClickEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getRenderer <em>Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getRenderer()
	 * @see #getCxGridRendererClickEvent()
	 * @generated
	 */
	EReference getCxGridRendererClickEvent_Renderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getLastClickTime <em>Last Click Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Click Time</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getLastClickTime()
	 * @see #getCxGridRendererClickEvent()
	 * @generated
	 */
	EAttribute getCxGridRendererClickEvent_LastClickTime();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBooleanRenderer <em>Cx Grid Boolean Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Boolean Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBooleanRenderer
	 * @generated
	 */
	EClass getCxGridBooleanRenderer();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer <em>Cx Grid Quantity Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Quantity Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer
	 * @generated
	 */
	EClass getCxGridQuantityRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getValuePropertyPath <em>Value Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getValuePropertyPath()
	 * @see #getCxGridQuantityRenderer()
	 * @generated
	 */
	EAttribute getCxGridQuantityRenderer_ValuePropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getUomPropertyPath <em>Uom Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uom Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getUomPropertyPath()
	 * @see #getCxGridQuantityRenderer()
	 * @generated
	 */
	EAttribute getCxGridQuantityRenderer_UomPropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNullRepresentation <em>Null Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Null Representation</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNullRepresentation()
	 * @see #getCxGridQuantityRenderer()
	 * @generated
	 */
	EAttribute getCxGridQuantityRenderer_NullRepresentation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getHtmlPattern <em>Html Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Html Pattern</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getHtmlPattern()
	 * @see #getCxGridQuantityRenderer()
	 * @generated
	 */
	EAttribute getCxGridQuantityRenderer_HtmlPattern();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNumberFormat <em>Number Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Format</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer#getNumberFormat()
	 * @see #getCxGridQuantityRenderer()
	 * @generated
	 */
	EAttribute getCxGridQuantityRenderer_NumberFormat();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer <em>Cx Grid Price Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Price Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer
	 * @generated
	 */
	EClass getCxGridPriceRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getValuePropertyPath <em>Value Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getValuePropertyPath()
	 * @see #getCxGridPriceRenderer()
	 * @generated
	 */
	EAttribute getCxGridPriceRenderer_ValuePropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getCurrencyPropertyPath <em>Currency Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Currency Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getCurrencyPropertyPath()
	 * @see #getCxGridPriceRenderer()
	 * @generated
	 */
	EAttribute getCxGridPriceRenderer_CurrencyPropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getNullRepresentation <em>Null Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Null Representation</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getNullRepresentation()
	 * @see #getCxGridPriceRenderer()
	 * @generated
	 */
	EAttribute getCxGridPriceRenderer_NullRepresentation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getHtmlPattern <em>Html Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Html Pattern</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getHtmlPattern()
	 * @see #getCxGridPriceRenderer()
	 * @generated
	 */
	EAttribute getCxGridPriceRenderer_HtmlPattern();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getNumberFormat <em>Number Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Format</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer#getNumberFormat()
	 * @see #getCxGridPriceRenderer()
	 * @generated
	 */
	EAttribute getCxGridPriceRenderer_NumberFormat();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer <em>Cx Grid Indicator Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Indicator Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer
	 * @generated
	 */
	EClass getCxGridIndicatorRenderer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getRedEnds <em>Red Ends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Red Ends</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getRedEnds()
	 * @see #getCxGridIndicatorRenderer()
	 * @generated
	 */
	EAttribute getCxGridIndicatorRenderer_RedEnds();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getGreenStarts <em>Green Starts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Green Starts</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getGreenStarts()
	 * @see #getCxGridIndicatorRenderer()
	 * @generated
	 */
	EAttribute getCxGridIndicatorRenderer_GreenStarts();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter <em>Cx Grid Nested Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Nested Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter
	 * @generated
	 */
	EClass getCxGridNestedConverter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedDotPath <em>Nested Dot Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nested Dot Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedDotPath()
	 * @see #getCxGridNestedConverter()
	 * @generated
	 */
	EAttribute getCxGridNestedConverter_NestedDotPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseType()
	 * @see #getCxGridNestedConverter()
	 * @generated
	 */
	EAttribute getCxGridNestedConverter_BaseType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseTypeQualifiedName <em>Base Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseTypeQualifiedName()
	 * @see #getCxGridNestedConverter()
	 * @generated
	 */
	EAttribute getCxGridNestedConverter_BaseTypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedType <em>Nested Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nested Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedType()
	 * @see #getCxGridNestedConverter()
	 * @generated
	 */
	EAttribute getCxGridNestedConverter_NestedType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeQualifiedName <em>Nested Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nested Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeQualifiedName()
	 * @see #getCxGridNestedConverter()
	 * @generated
	 */
	EAttribute getCxGridNestedConverter_NestedTypeQualifiedName();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeConverter <em>Nested Type Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nested Type Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeConverter()
	 * @see #getCxGridNestedConverter()
	 * @generated
	 */
	EReference getCxGridNestedConverter_NestedTypeConverter();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CxGridRendererFactory getCxGridRendererFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererImpl <em>Cx Grid Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridRenderer()
		 * @generated
		 */
		EClass CX_GRID_RENDERER = eINSTANCE.getCxGridRenderer();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDelegateRendererImpl <em>Cx Grid Delegate Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDelegateRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridDelegateRenderer()
		 * @generated
		 */
		EClass CX_GRID_DELEGATE_RENDERER = eINSTANCE.getCxGridDelegateRenderer();

		/**
		 * The meta object literal for the '<em><b>Delegate Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_DELEGATE_RENDERER__DELEGATE_ID = eINSTANCE.getCxGridDelegateRenderer_DelegateId();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDateRendererImpl <em>Cx Grid Date Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDateRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridDateRenderer()
		 * @generated
		 */
		EClass CX_GRID_DATE_RENDERER = eINSTANCE.getCxGridDateRenderer();

		/**
		 * The meta object literal for the '<em><b>Date Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_DATE_RENDERER__DATE_FORMAT = eINSTANCE.getCxGridDateRenderer_DateFormat();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridHtmlRendererImpl <em>Cx Grid Html Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridHtmlRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridHtmlRenderer()
		 * @generated
		 */
		EClass CX_GRID_HTML_RENDERER = eINSTANCE.getCxGridHtmlRenderer();

		/**
		 * The meta object literal for the '<em><b>Null Representation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_HTML_RENDERER__NULL_REPRESENTATION = eINSTANCE.getCxGridHtmlRenderer_NullRepresentation();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNumberRendererImpl <em>Cx Grid Number Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNumberRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridNumberRenderer()
		 * @generated
		 */
		EClass CX_GRID_NUMBER_RENDERER = eINSTANCE.getCxGridNumberRenderer();

		/**
		 * The meta object literal for the '<em><b>Number Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_NUMBER_RENDERER__NUMBER_FORMAT = eINSTANCE.getCxGridNumberRenderer_NumberFormat();

		/**
		 * The meta object literal for the '<em><b>Null Representation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_NUMBER_RENDERER__NULL_REPRESENTATION = eINSTANCE.getCxGridNumberRenderer_NullRepresentation();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridProgressBarRendererImpl <em>Cx Grid Progress Bar Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridProgressBarRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridProgressBarRenderer()
		 * @generated
		 */
		EClass CX_GRID_PROGRESS_BAR_RENDERER = eINSTANCE.getCxGridProgressBarRenderer();

		/**
		 * The meta object literal for the '<em><b>Max Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_PROGRESS_BAR_RENDERER__MAX_VALUE = eINSTANCE.getCxGridProgressBarRenderer_MaxValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridTextRendererImpl <em>Cx Grid Text Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridTextRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridTextRenderer()
		 * @generated
		 */
		EClass CX_GRID_TEXT_RENDERER = eINSTANCE.getCxGridTextRenderer();

		/**
		 * The meta object literal for the '<em><b>Null Representation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_TEXT_RENDERER__NULL_REPRESENTATION = eINSTANCE.getCxGridTextRenderer_NullRepresentation();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridButtonRendererImpl <em>Cx Grid Button Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridButtonRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridButtonRenderer()
		 * @generated
		 */
		EClass CX_GRID_BUTTON_RENDERER = eINSTANCE.getCxGridButtonRenderer();

		/**
		 * The meta object literal for the '<em><b>Null Representation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_BUTTON_RENDERER__NULL_REPRESENTATION = eINSTANCE.getCxGridButtonRenderer_NullRepresentation();

		/**
		 * The meta object literal for the '<em><b>Last Click Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_BUTTON_RENDERER__LAST_CLICK_EVENT = eINSTANCE.getCxGridButtonRenderer_LastClickEvent();

		/**
		 * The meta object literal for the '<em><b>Event Topic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_BUTTON_RENDERER__EVENT_TOPIC = eINSTANCE.getCxGridButtonRenderer_EventTopic();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBlobImageRendererImpl <em>Cx Grid Blob Image Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBlobImageRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridBlobImageRenderer()
		 * @generated
		 */
		EClass CX_GRID_BLOB_IMAGE_RENDERER = eINSTANCE.getCxGridBlobImageRenderer();

		/**
		 * The meta object literal for the '<em><b>Last Click Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_BLOB_IMAGE_RENDERER__LAST_CLICK_EVENT = eINSTANCE.getCxGridBlobImageRenderer_LastClickEvent();

		/**
		 * The meta object literal for the '<em><b>Event Topic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_BLOB_IMAGE_RENDERER__EVENT_TOPIC = eINSTANCE.getCxGridBlobImageRenderer_EventTopic();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridImageRendererImpl <em>Cx Grid Image Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridImageRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridImageRenderer()
		 * @generated
		 */
		EClass CX_GRID_IMAGE_RENDERER = eINSTANCE.getCxGridImageRenderer();

		/**
		 * The meta object literal for the '<em><b>Last Click Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_IMAGE_RENDERER__LAST_CLICK_EVENT = eINSTANCE.getCxGridImageRenderer_LastClickEvent();

		/**
		 * The meta object literal for the '<em><b>Event Topic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_IMAGE_RENDERER__EVENT_TOPIC = eINSTANCE.getCxGridImageRenderer_EventTopic();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererClickEventImpl <em>Click Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererClickEventImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridRendererClickEvent()
		 * @generated
		 */
		EClass CX_GRID_RENDERER_CLICK_EVENT = eINSTANCE.getCxGridRendererClickEvent();

		/**
		 * The meta object literal for the '<em><b>Renderer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_RENDERER_CLICK_EVENT__RENDERER = eINSTANCE.getCxGridRendererClickEvent_Renderer();

		/**
		 * The meta object literal for the '<em><b>Last Click Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_RENDERER_CLICK_EVENT__LAST_CLICK_TIME = eINSTANCE.getCxGridRendererClickEvent_LastClickTime();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBooleanRendererImpl <em>Cx Grid Boolean Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridBooleanRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridBooleanRenderer()
		 * @generated
		 */
		EClass CX_GRID_BOOLEAN_RENDERER = eINSTANCE.getCxGridBooleanRenderer();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridQuantityRendererImpl <em>Cx Grid Quantity Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridQuantityRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridQuantityRenderer()
		 * @generated
		 */
		EClass CX_GRID_QUANTITY_RENDERER = eINSTANCE.getCxGridQuantityRenderer();

		/**
		 * The meta object literal for the '<em><b>Value Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_QUANTITY_RENDERER__VALUE_PROPERTY_PATH = eINSTANCE.getCxGridQuantityRenderer_ValuePropertyPath();

		/**
		 * The meta object literal for the '<em><b>Uom Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_QUANTITY_RENDERER__UOM_PROPERTY_PATH = eINSTANCE.getCxGridQuantityRenderer_UomPropertyPath();

		/**
		 * The meta object literal for the '<em><b>Null Representation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_QUANTITY_RENDERER__NULL_REPRESENTATION = eINSTANCE.getCxGridQuantityRenderer_NullRepresentation();

		/**
		 * The meta object literal for the '<em><b>Html Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_QUANTITY_RENDERER__HTML_PATTERN = eINSTANCE.getCxGridQuantityRenderer_HtmlPattern();

		/**
		 * The meta object literal for the '<em><b>Number Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_QUANTITY_RENDERER__NUMBER_FORMAT = eINSTANCE.getCxGridQuantityRenderer_NumberFormat();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridPriceRendererImpl <em>Cx Grid Price Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridPriceRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridPriceRenderer()
		 * @generated
		 */
		EClass CX_GRID_PRICE_RENDERER = eINSTANCE.getCxGridPriceRenderer();

		/**
		 * The meta object literal for the '<em><b>Value Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_PRICE_RENDERER__VALUE_PROPERTY_PATH = eINSTANCE.getCxGridPriceRenderer_ValuePropertyPath();

		/**
		 * The meta object literal for the '<em><b>Currency Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_PRICE_RENDERER__CURRENCY_PROPERTY_PATH = eINSTANCE.getCxGridPriceRenderer_CurrencyPropertyPath();

		/**
		 * The meta object literal for the '<em><b>Null Representation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_PRICE_RENDERER__NULL_REPRESENTATION = eINSTANCE.getCxGridPriceRenderer_NullRepresentation();

		/**
		 * The meta object literal for the '<em><b>Html Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_PRICE_RENDERER__HTML_PATTERN = eINSTANCE.getCxGridPriceRenderer_HtmlPattern();

		/**
		 * The meta object literal for the '<em><b>Number Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_PRICE_RENDERER__NUMBER_FORMAT = eINSTANCE.getCxGridPriceRenderer_NumberFormat();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridIndicatorRendererImpl <em>Cx Grid Indicator Renderer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridIndicatorRendererImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridIndicatorRenderer()
		 * @generated
		 */
		EClass CX_GRID_INDICATOR_RENDERER = eINSTANCE.getCxGridIndicatorRenderer();

		/**
		 * The meta object literal for the '<em><b>Red Ends</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_INDICATOR_RENDERER__RED_ENDS = eINSTANCE.getCxGridIndicatorRenderer_RedEnds();

		/**
		 * The meta object literal for the '<em><b>Green Starts</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_INDICATOR_RENDERER__GREEN_STARTS = eINSTANCE.getCxGridIndicatorRenderer_GreenStarts();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl <em>Cx Grid Nested Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl#getCxGridNestedConverter()
		 * @generated
		 */
		EClass CX_GRID_NESTED_CONVERTER = eINSTANCE.getCxGridNestedConverter();

		/**
		 * The meta object literal for the '<em><b>Nested Dot Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH = eINSTANCE.getCxGridNestedConverter_NestedDotPath();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_NESTED_CONVERTER__BASE_TYPE = eINSTANCE.getCxGridNestedConverter_BaseType();

		/**
		 * The meta object literal for the '<em><b>Base Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME = eINSTANCE.getCxGridNestedConverter_BaseTypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Nested Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_NESTED_CONVERTER__NESTED_TYPE = eINSTANCE.getCxGridNestedConverter_NestedType();

		/**
		 * The meta object literal for the '<em><b>Nested Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME = eINSTANCE.getCxGridNestedConverter_NestedTypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Nested Type Converter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER = eINSTANCE.getCxGridNestedConverter_NestedTypeConverter();

	}

}
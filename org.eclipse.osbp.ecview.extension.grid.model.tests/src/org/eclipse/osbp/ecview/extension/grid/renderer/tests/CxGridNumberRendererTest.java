/**
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cx Grid Number Renderer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridNumberRendererTest extends CxGridRendererTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridNumberRendererTest.class);
	}

	/**
	 * Constructs a new Cx Grid Number Renderer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridNumberRendererTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Cx Grid Number Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CxGridNumberRenderer getFixture() {
		return (CxGridNumberRenderer)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridRendererFactory.eINSTANCE.createCxGridNumberRenderer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridNumberRendererTest

/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.model.core.YCompare;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YNumeric To Resource Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getCompare <em>Compare</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getResourceThemePath <em>Resource Theme Path</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYNumericToResourceConfig()
 * @model
 * @generated
 */
public interface YNumericToResourceConfig extends EObject {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(double)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYNumericToResourceConfig_Value()
	 * @model
	 * @generated
	 */
	double getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(double value);

	/**
	 * Returns the value of the '<em><b>Compare</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.core.common.model.core.YCompare}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compare</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compare</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCompare
	 * @see #setCompare(YCompare)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYNumericToResourceConfig_Compare()
	 * @model
	 * @generated
	 */
	YCompare getCompare();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getCompare <em>Compare</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compare</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCompare
	 * @see #getCompare()
	 * @generated
	 */
	void setCompare(YCompare value);

	/**
	 * Returns the value of the '<em><b>Resource Theme Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Theme Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Theme Path</em>' attribute.
	 * @see #setResourceThemePath(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYNumericToResourceConfig_ResourceThemePath()
	 * @model
	 * @generated
	 */
	String getResourceThemePath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getResourceThemePath <em>Resource Theme Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Theme Path</em>' attribute.
	 * @see #getResourceThemePath()
	 * @generated
	 */
	void setResourceThemePath(String value);

}

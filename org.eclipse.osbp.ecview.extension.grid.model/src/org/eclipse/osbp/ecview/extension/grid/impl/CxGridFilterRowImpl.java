/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.impl;

import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridFilterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.util.CxGridUtil;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YStringToStringMapImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Filter Row</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl#getGroupings <em>Groupings</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl#getCustomCells <em>Custom Cells</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridFilterRowImpl extends MinimalEObjectImpl.Container implements CxGridFilterRow {
	
	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * The cached value of the '{@link #getGroupings() <em>Groupings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupings()
	 * @generated
	 * @ordered
	 */
	protected EList<CxGridGroupedCell> groupings;

	/**
	 * The cached value of the '{@link #getCustomCells() <em>Custom Cells</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomCells()
	 * @generated
	 * @ordered
	 */
	protected EList<CxGridMetaCell> customCells;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected CxGridFilterRowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridPackage.Literals.CX_GRID_FILTER_ROW;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getId() <em>Id</em>}' attribute
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newId
	 *            the new cached value of the '{@link #getId() <em>Id</em>}'
	 *            attribute
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_FILTER_ROW__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getName() <em>Name</em>}'
	 *         attribute
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newName
	 *            the new cached value of the '{@link #getName() <em>Name</em>}'
	 *            attribute
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_FILTER_ROW__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTags() <em>Tags</em>}'
	 *         attribute list
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, CxGridPackage.CX_GRID_FILTER_ROW__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getProperties()
	 *         <em>Properties</em>}' map
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP, YStringToStringMapImpl.class, this, CxGridPackage.CX_GRID_FILTER_ROW__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getGroupings()
	 *         <em>Groupings</em>}' containment reference list
	 * @generated
	 */
	public EList<CxGridGroupedCell> getGroupings() {
		if (groupings == null) {
			groupings = new EObjectContainmentEList<CxGridGroupedCell>(CxGridGroupedCell.class, this, CxGridPackage.CX_GRID_FILTER_ROW__GROUPINGS);
		}
		return groupings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getCustomCells()
	 *         <em>Custom Cells</em>}' containment reference list
	 * @generated
	 */
	public EList<CxGridMetaCell> getCustomCells() {
		if (customCells == null) {
			customCells = new EObjectContainmentEList<CxGridMetaCell>(CxGridMetaCell.class, this, CxGridPackage.CX_GRID_FILTER_ROW__CUSTOM_CELLS);
		}
		return customCells;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_FILTER_ROW__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case CxGridPackage.CX_GRID_FILTER_ROW__GROUPINGS:
				return ((InternalEList<?>)getGroupings()).basicRemove(otherEnd, msgs);
			case CxGridPackage.CX_GRID_FILTER_ROW__CUSTOM_CELLS:
				return ((InternalEList<?>)getCustomCells()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_FILTER_ROW__TAGS:
				return getTags();
			case CxGridPackage.CX_GRID_FILTER_ROW__ID:
				return getId();
			case CxGridPackage.CX_GRID_FILTER_ROW__NAME:
				return getName();
			case CxGridPackage.CX_GRID_FILTER_ROW__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
			case CxGridPackage.CX_GRID_FILTER_ROW__GROUPINGS:
				return getGroupings();
			case CxGridPackage.CX_GRID_FILTER_ROW__CUSTOM_CELLS:
				return getCustomCells();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_FILTER_ROW__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__ID:
				setId((String)newValue);
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__NAME:
				setName((String)newValue);
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__GROUPINGS:
				getGroupings().clear();
				getGroupings().addAll((Collection<? extends CxGridGroupedCell>)newValue);
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__CUSTOM_CELLS:
				getCustomCells().clear();
				getCustomCells().addAll((Collection<? extends CxGridMetaCell>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_FILTER_ROW__TAGS:
				getTags().clear();
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__ID:
				setId(ID_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__PROPERTIES:
				getProperties().clear();
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__GROUPINGS:
				getGroupings().clear();
				return;
			case CxGridPackage.CX_GRID_FILTER_ROW__CUSTOM_CELLS:
				getCustomCells().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_FILTER_ROW__TAGS:
				return tags != null && !tags.isEmpty();
			case CxGridPackage.CX_GRID_FILTER_ROW__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case CxGridPackage.CX_GRID_FILTER_ROW__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CxGridPackage.CX_GRID_FILTER_ROW__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case CxGridPackage.CX_GRID_FILTER_ROW__GROUPINGS:
				return groupings != null && !groupings.isEmpty();
			case CxGridPackage.CX_GRID_FILTER_ROW__CUSTOM_CELLS:
				return customCells != null && !customCells.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridProvider#getGrid()
	 */
	@Override
	public CxGrid getGrid() {
		return CxGridUtil.getGrid(this);
	}

}
/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoFactory
 * @model kind="package"
 * @generated
 */
public interface CxGridMementoPackage extends EPackage {
	
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "memento";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.de/ecview/v1/extension/grid/memento";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "memento";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CxGridMementoPackage eINSTANCE = org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoImpl <em>Cx Grid Memento</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl#getCxGridMemento()
	 * @generated
	 */
	int CX_GRID_MEMENTO = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__TAGS = CoreModelPackage.YMEMENTO__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__ID = CoreModelPackage.YMEMENTO__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__NAME = CoreModelPackage.YMEMENTO__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__PROPERTIES = CoreModelPackage.YMEMENTO__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Grid Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__GRID_ID = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Header Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__HEADER_VISIBLE = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Filter Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__FILTER_VISIBLE = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Footer Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__FOOTER_VISIBLE = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Editor Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__EDITOR_ENABLED = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__COLUMNS = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Sort Orders</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO__SORT_ORDERS = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Cx Grid Memento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_FEATURE_COUNT = CoreModelPackage.YMEMENTO_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl <em>Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl#getCxGridMementoColumn()
	 * @generated
	 */
	int CX_GRID_MEMENTO_COLUMN = 1;

	/**
	 * The feature id for the '<em><b>Property Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN__PROPERTY_ID = 0;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN__EDITABLE = 1;

	/**
	 * The feature id for the '<em><b>Expand Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO = 2;

	/**
	 * The feature id for the '<em><b>Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN__HIDDEN = 3;

	/**
	 * The feature id for the '<em><b>Hideable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN__HIDEABLE = 4;

	/**
	 * The feature id for the '<em><b>Sortable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN__SORTABLE = 5;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN__WIDTH = 6;

	/**
	 * The number of structural features of the '<em>Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_COLUMN_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoSortableImpl <em>Sortable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoSortableImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl#getCxGridMementoSortable()
	 * @generated
	 */
	int CX_GRID_MEMENTO_SORTABLE = 2;

	/**
	 * The feature id for the '<em><b>Descending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_SORTABLE__DESCENDING = 0;

	/**
	 * The feature id for the '<em><b>Column</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_SORTABLE__COLUMN = 1;

	/**
	 * The number of structural features of the '<em>Sortable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_MEMENTO_SORTABLE_FEATURE_COUNT = 2;

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento <em>Cx Grid Memento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid Memento</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento
	 * @generated
	 */
	EClass getCxGridMemento();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getGridId <em>Grid Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grid Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getGridId()
	 * @see #getCxGridMemento()
	 * @generated
	 */
	EAttribute getCxGridMemento_GridId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isHeaderVisible <em>Header Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header Visible</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isHeaderVisible()
	 * @see #getCxGridMemento()
	 * @generated
	 */
	EAttribute getCxGridMemento_HeaderVisible();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFilterVisible <em>Filter Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Visible</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFilterVisible()
	 * @see #getCxGridMemento()
	 * @generated
	 */
	EAttribute getCxGridMemento_FilterVisible();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFooterVisible <em>Footer Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Footer Visible</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isFooterVisible()
	 * @see #getCxGridMemento()
	 * @generated
	 */
	EAttribute getCxGridMemento_FooterVisible();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isEditorEnabled <em>Editor Enabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Enabled</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#isEditorEnabled()
	 * @see #getCxGridMemento()
	 * @generated
	 */
	EAttribute getCxGridMemento_EditorEnabled();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Columns</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getColumns()
	 * @see #getCxGridMemento()
	 * @generated
	 */
	EReference getCxGridMemento_Columns();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getSortOrders <em>Sort Orders</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sort Orders</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento#getSortOrders()
	 * @see #getCxGridMemento()
	 * @generated
	 */
	EReference getCxGridMemento_SortOrders();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Column</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn
	 * @generated
	 */
	EClass getCxGridMementoColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getPropertyId <em>Property Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getPropertyId()
	 * @see #getCxGridMementoColumn()
	 * @generated
	 */
	EAttribute getCxGridMementoColumn_PropertyId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isEditable <em>Editable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isEditable()
	 * @see #getCxGridMementoColumn()
	 * @generated
	 */
	EAttribute getCxGridMementoColumn_Editable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getExpandRatio <em>Expand Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expand Ratio</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getExpandRatio()
	 * @see #getCxGridMementoColumn()
	 * @generated
	 */
	EAttribute getCxGridMementoColumn_ExpandRatio();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHidden <em>Hidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hidden</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHidden()
	 * @see #getCxGridMementoColumn()
	 * @generated
	 */
	EAttribute getCxGridMementoColumn_Hidden();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHideable <em>Hideable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hideable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHideable()
	 * @see #getCxGridMementoColumn()
	 * @generated
	 */
	EAttribute getCxGridMementoColumn_Hideable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isSortable <em>Sortable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sortable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isSortable()
	 * @see #getCxGridMementoColumn()
	 * @generated
	 */
	EAttribute getCxGridMementoColumn_Sortable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getWidth()
	 * @see #getCxGridMementoColumn()
	 * @generated
	 */
	EAttribute getCxGridMementoColumn_Width();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable <em>Sortable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sortable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable
	 * @generated
	 */
	EClass getCxGridMementoSortable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#isDescending <em>Descending</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Descending</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#isDescending()
	 * @see #getCxGridMementoSortable()
	 * @generated
	 */
	EAttribute getCxGridMementoSortable_Descending();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#getColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable#getColumn()
	 * @see #getCxGridMementoSortable()
	 * @generated
	 */
	EReference getCxGridMementoSortable_Column();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CxGridMementoFactory getCxGridMementoFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoImpl <em>Cx Grid Memento</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl#getCxGridMemento()
		 * @generated
		 */
		EClass CX_GRID_MEMENTO = eINSTANCE.getCxGridMemento();

		/**
		 * The meta object literal for the '<em><b>Grid Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO__GRID_ID = eINSTANCE.getCxGridMemento_GridId();

		/**
		 * The meta object literal for the '<em><b>Header Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO__HEADER_VISIBLE = eINSTANCE.getCxGridMemento_HeaderVisible();

		/**
		 * The meta object literal for the '<em><b>Filter Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO__FILTER_VISIBLE = eINSTANCE.getCxGridMemento_FilterVisible();

		/**
		 * The meta object literal for the '<em><b>Footer Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO__FOOTER_VISIBLE = eINSTANCE.getCxGridMemento_FooterVisible();

		/**
		 * The meta object literal for the '<em><b>Editor Enabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO__EDITOR_ENABLED = eINSTANCE.getCxGridMemento_EditorEnabled();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_MEMENTO__COLUMNS = eINSTANCE.getCxGridMemento_Columns();

		/**
		 * The meta object literal for the '<em><b>Sort Orders</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_MEMENTO__SORT_ORDERS = eINSTANCE.getCxGridMemento_SortOrders();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl <em>Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl#getCxGridMementoColumn()
		 * @generated
		 */
		EClass CX_GRID_MEMENTO_COLUMN = eINSTANCE.getCxGridMementoColumn();

		/**
		 * The meta object literal for the '<em><b>Property Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_COLUMN__PROPERTY_ID = eINSTANCE.getCxGridMementoColumn_PropertyId();

		/**
		 * The meta object literal for the '<em><b>Editable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_COLUMN__EDITABLE = eINSTANCE.getCxGridMementoColumn_Editable();

		/**
		 * The meta object literal for the '<em><b>Expand Ratio</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO = eINSTANCE.getCxGridMementoColumn_ExpandRatio();

		/**
		 * The meta object literal for the '<em><b>Hidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_COLUMN__HIDDEN = eINSTANCE.getCxGridMementoColumn_Hidden();

		/**
		 * The meta object literal for the '<em><b>Hideable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_COLUMN__HIDEABLE = eINSTANCE.getCxGridMementoColumn_Hideable();

		/**
		 * The meta object literal for the '<em><b>Sortable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_COLUMN__SORTABLE = eINSTANCE.getCxGridMementoColumn_Sortable();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_COLUMN__WIDTH = eINSTANCE.getCxGridMementoColumn_Width();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoSortableImpl <em>Sortable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoSortableImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl#getCxGridMementoSortable()
		 * @generated
		 */
		EClass CX_GRID_MEMENTO_SORTABLE = eINSTANCE.getCxGridMementoSortable();

		/**
		 * The meta object literal for the '<em><b>Descending</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_MEMENTO_SORTABLE__DESCENDING = eINSTANCE.getCxGridMementoSortable_Descending();

		/**
		 * The meta object literal for the '<em><b>Column</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_MEMENTO_SORTABLE__COLUMN = eINSTANCE.getCxGridMementoSortable_Column();

	}

}
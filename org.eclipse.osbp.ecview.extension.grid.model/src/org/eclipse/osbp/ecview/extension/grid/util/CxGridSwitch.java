/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.util;

import org.eclipse.osbp.ecview.extension.grid.*;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable;
import org.eclipse.osbp.ecview.core.common.model.core.YBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YCssAble;
import org.eclipse.osbp.ecview.core.common.model.core.YEditable;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YEnable;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;
import org.eclipse.osbp.ecview.core.common.model.core.YHelperLayoutProvider;
import org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;
import org.eclipse.osbp.ecview.core.common.model.core.YVisibleable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanServiceConsumer;
import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupable;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage
 * @generated
 */
public class CxGridSwitch<T> extends Switch<T> {
	
	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected static CxGridPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridSwitch() {
		if (modelPackage == null) {
			modelPackage = CxGridPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param ePackage
	 *            the e package
	 * @return whether this is a switch for the given package.
	 * @parameter ePackage the package in question.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CxGridPackage.CX_GRID: {
				CxGrid cxGrid = (CxGrid)theEObject;
				T result = caseCxGrid(cxGrid);
				if (result == null) result = caseYInput(cxGrid);
				if (result == null) result = caseYCollectionBindable(cxGrid);
				if (result == null) result = caseYSelectionBindable(cxGrid);
				if (result == null) result = caseYMultiSelectionBindable(cxGrid);
				if (result == null) result = caseYBeanServiceConsumer(cxGrid);
				if (result == null) result = caseYField(cxGrid);
				if (result == null) result = caseYBindable(cxGrid);
				if (result == null) result = caseYEmbeddable(cxGrid);
				if (result == null) result = caseYEditable(cxGrid);
				if (result == null) result = caseYEnable(cxGrid);
				if (result == null) result = caseYFocusable(cxGrid);
				if (result == null) result = caseYElement(cxGrid);
				if (result == null) result = caseYCssAble(cxGrid);
				if (result == null) result = caseYVisibleable(cxGrid);
				if (result == null) result = caseYAuthorizationable(cxGrid);
				if (result == null) result = caseYTaggable(cxGrid);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_PROVIDER: {
				CxGridProvider cxGridProvider = (CxGridProvider)theEObject;
				T result = caseCxGridProvider(cxGridProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_META_ROW: {
				CxGridMetaRow cxGridMetaRow = (CxGridMetaRow)theEObject;
				T result = caseCxGridMetaRow(cxGridMetaRow);
				if (result == null) result = caseYElement(cxGridMetaRow);
				if (result == null) result = caseCxGridProvider(cxGridMetaRow);
				if (result == null) result = caseYTaggable(cxGridMetaRow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_HEADER_ROW: {
				CxGridHeaderRow cxGridHeaderRow = (CxGridHeaderRow)theEObject;
				T result = caseCxGridHeaderRow(cxGridHeaderRow);
				if (result == null) result = caseCxGridMetaRow(cxGridHeaderRow);
				if (result == null) result = caseYElement(cxGridHeaderRow);
				if (result == null) result = caseCxGridProvider(cxGridHeaderRow);
				if (result == null) result = caseYTaggable(cxGridHeaderRow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_FOOTER_ROW: {
				CxGridFooterRow cxGridFooterRow = (CxGridFooterRow)theEObject;
				T result = caseCxGridFooterRow(cxGridFooterRow);
				if (result == null) result = caseCxGridMetaRow(cxGridFooterRow);
				if (result == null) result = caseYElement(cxGridFooterRow);
				if (result == null) result = caseCxGridProvider(cxGridFooterRow);
				if (result == null) result = caseYTaggable(cxGridFooterRow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_FILTER_ROW: {
				CxGridFilterRow cxGridFilterRow = (CxGridFilterRow)theEObject;
				T result = caseCxGridFilterRow(cxGridFilterRow);
				if (result == null) result = caseCxGridMetaRow(cxGridFilterRow);
				if (result == null) result = caseYElement(cxGridFilterRow);
				if (result == null) result = caseCxGridProvider(cxGridFilterRow);
				if (result == null) result = caseYTaggable(cxGridFilterRow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_GROUPABLE: {
				CxGridGroupable cxGridGroupable = (CxGridGroupable)theEObject;
				T result = caseCxGridGroupable(cxGridGroupable);
				if (result == null) result = caseYElement(cxGridGroupable);
				if (result == null) result = caseCxGridProvider(cxGridGroupable);
				if (result == null) result = caseYTaggable(cxGridGroupable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_META_CELL: {
				CxGridMetaCell cxGridMetaCell = (CxGridMetaCell)theEObject;
				T result = caseCxGridMetaCell(cxGridMetaCell);
				if (result == null) result = caseYElement(cxGridMetaCell);
				if (result == null) result = caseCxGridProvider(cxGridMetaCell);
				if (result == null) result = caseYTaggable(cxGridMetaCell);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_GROUPED_CELL: {
				CxGridGroupedCell cxGridGroupedCell = (CxGridGroupedCell)theEObject;
				T result = caseCxGridGroupedCell(cxGridGroupedCell);
				if (result == null) result = caseCxGridGroupable(cxGridGroupedCell);
				if (result == null) result = caseYElement(cxGridGroupedCell);
				if (result == null) result = caseCxGridProvider(cxGridGroupedCell);
				if (result == null) result = caseYTaggable(cxGridGroupedCell);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_COLUMN: {
				CxGridColumn cxGridColumn = (CxGridColumn)theEObject;
				T result = caseCxGridColumn(cxGridColumn);
				if (result == null) result = caseCxGridGroupable(cxGridColumn);
				if (result == null) result = caseYHelperLayoutProvider(cxGridColumn);
				if (result == null) result = caseYElement(cxGridColumn);
				if (result == null) result = caseCxGridProvider(cxGridColumn);
				if (result == null) result = caseYTaggable(cxGridColumn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_CELL_STYLE_GENERATOR: {
				CxGridCellStyleGenerator cxGridCellStyleGenerator = (CxGridCellStyleGenerator)theEObject;
				T result = caseCxGridCellStyleGenerator(cxGridCellStyleGenerator);
				if (result == null) result = caseYElement(cxGridCellStyleGenerator);
				if (result == null) result = caseCxGridProvider(cxGridCellStyleGenerator);
				if (result == null) result = caseYTaggable(cxGridCellStyleGenerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_DELEGATE_CELL_STYLE_GENERATOR: {
				CxGridDelegateCellStyleGenerator cxGridDelegateCellStyleGenerator = (CxGridDelegateCellStyleGenerator)theEObject;
				T result = caseCxGridDelegateCellStyleGenerator(cxGridDelegateCellStyleGenerator);
				if (result == null) result = caseCxGridCellStyleGenerator(cxGridDelegateCellStyleGenerator);
				if (result == null) result = caseYElement(cxGridDelegateCellStyleGenerator);
				if (result == null) result = caseCxGridProvider(cxGridDelegateCellStyleGenerator);
				if (result == null) result = caseYTaggable(cxGridDelegateCellStyleGenerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridPackage.CX_GRID_SORTABLE: {
				CxGridSortable cxGridSortable = (CxGridSortable)theEObject;
				T result = caseCxGridSortable(cxGridSortable);
				if (result == null) result = caseYElement(cxGridSortable);
				if (result == null) result = caseYTaggable(cxGridSortable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGrid(CxGrid object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridProvider(CxGridProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Meta Row</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Meta Row</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridMetaRow(CxGridMetaRow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Header Row</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Header Row</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridHeaderRow(CxGridHeaderRow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Footer Row</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Footer Row</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridFooterRow(CxGridFooterRow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Filter Row</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Filter Row</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridFilterRow(CxGridFilterRow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Groupable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Groupable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridGroupable(CxGridGroupable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Meta Cell</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Meta Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridMetaCell(CxGridMetaCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Grouped Cell</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Grouped Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridGroupedCell(CxGridGroupedCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Column</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Column</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridColumn(CxGridColumn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cell Style Generator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cell Style Generator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridCellStyleGenerator(CxGridCellStyleGenerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delegate Cell Style Generator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delegate Cell Style Generator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridDelegateCellStyleGenerator(CxGridDelegateCellStyleGenerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sortable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sortable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridSortable(CxGridSortable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYTaggable(YTaggable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYElement(YElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YCss Able</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YCss Able</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYCssAble(YCssAble object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YVisibleable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YVisibleable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYVisibleable(YVisibleable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YAuthorizationable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YAuthorizationable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYAuthorizationable(YAuthorizationable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YEmbeddable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YEmbeddable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYEmbeddable(YEmbeddable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YEditable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YEditable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYEditable(YEditable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YEnable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YEnable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYEnable(YEnable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YFocusable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YFocusable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYFocusable(YFocusable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YField</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YField</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYField(YField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YInput</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YInput</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYInput(YInput object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YBindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YBindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYBindable(YBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YCollection Bindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YCollection Bindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYCollectionBindable(YCollectionBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSelection Bindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSelection Bindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSelectionBindable(YSelectionBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YMulti Selection Bindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YMulti Selection Bindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYMultiSelectionBindable(YMultiSelectionBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YBean Service Consumer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YBean Service Consumer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYBeanServiceConsumer(YBeanServiceConsumer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YHelper Layout Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YHelper Layout Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYHelperLayoutProvider(YHelperLayoutProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

}
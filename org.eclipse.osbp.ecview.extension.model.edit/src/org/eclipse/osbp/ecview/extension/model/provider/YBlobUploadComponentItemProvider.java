/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;


import org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.provider.YInputItemProvider;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YBlobUploadComponentItemProvider extends YInputItemProvider {
	
	/**
<<<<<<< HEAD
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
=======
>>>>>>> branch 'development' of ssh://lunifera@80.156.28.28/osbpgit/org.eclipse.osbp.ecview.extension.git
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public YBlobUploadComponentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValueBindingEndpointPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addDisplayResolutionIdPropertyDescriptor(object);
			addFirmlyLinkedPropertyDescriptor(object);
			addUniqueNameEnabledPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Value Binding Endpoint feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValueBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YValueBindable_valueBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YValueBindable_valueBindingEndpoint_feature", "_UI_YValueBindable_type"),
				 CoreModelPackage.Literals.YVALUE_BINDABLE__VALUE_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Value feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBlobUploadComponent_value_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBlobUploadComponent_value_feature", "_UI_YBlobUploadComponent_type"),
				 YECviewPackage.Literals.YBLOB_UPLOAD_COMPONENT__VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Display Resolution Id feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayResolutionIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBlobUploadComponent_displayResolutionId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBlobUploadComponent_displayResolutionId_feature", "_UI_YBlobUploadComponent_type"),
				 YECviewPackage.Literals.YBLOB_UPLOAD_COMPONENT__DISPLAY_RESOLUTION_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Firmly Linked feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFirmlyLinkedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBlobUploadComponent_firmlyLinked_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBlobUploadComponent_firmlyLinked_feature", "_UI_YBlobUploadComponent_type"),
				 YECviewPackage.Literals.YBLOB_UPLOAD_COMPONENT__FIRMLY_LINKED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Unique Name Enabled feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUniqueNameEnabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBlobUploadComponent_uniqueNameEnabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBlobUploadComponent_uniqueNameEnabled_feature", "_UI_YBlobUploadComponent_type"),
				 YECviewPackage.Literals.YBLOB_UPLOAD_COMPONENT__UNIQUE_NAME_ENABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns YBlobUploadComponent.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YBlobUploadComponent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YBlobUploadComponent)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YBlobUploadComponent_type") :
			getString("_UI_YBlobUploadComponent_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YBlobUploadComponent.class)) {
			case YECviewPackage.YBLOB_UPLOAD_COMPONENT__VALUE:
			case YECviewPackage.YBLOB_UPLOAD_COMPONENT__DISPLAY_RESOLUTION_ID:
			case YECviewPackage.YBLOB_UPLOAD_COMPONENT__FIRMLY_LINKED:
			case YECviewPackage.YBLOB_UPLOAD_COMPONENT__UNIQUE_NAME_ENABLED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYObjectToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYStringToByteArrayConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYCustomDecimalConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYNumericToResourceConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYStringToResourceConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYPriceToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYQuantityToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYNumericToUomoConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYDecimalToUomoConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYSimpleDecimalConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYVaaclipseUiThemeToStringConverter()));
	}

	/**
	 * This returns the label text for
	 * {@link org.eclipse.emf.edit.command.CreateChildCommand}. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param owner
	 *            the owner
	 * @param feature
	 *            the feature
	 * @param child
	 *            the child
	 * @param selection
	 *            the selection
	 * @return the creates the child text
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}

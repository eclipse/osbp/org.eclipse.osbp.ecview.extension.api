/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.renderer.tests.CxGridRendererTests;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>CxGrid</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class cxecviewAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new cxecviewAllTests("CxGrid Tests");
		suite.addTest(CxGridTests.suite());
		suite.addTest(CxGridRendererTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public cxecviewAllTests(String name) {
		super(name);
	}

} //cxecviewAllTests

/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YHelperLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanServiceConsumer;
import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Cx Grid</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionType <em>Selection Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionEventTopic <em>Selection Event Topic</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelection <em>Selection</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getMultiSelection <em>Multi Selection</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getCollection <em>Collection</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEmfNsURI <em>Emf Ns URI</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getColumns <em>Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSortOrder <em>Sort Order</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isColumnReorderingAllowed <em>Column Reordering Allowed</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getCellStyleGenerator <em>Cell Style Generator</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isFilteringVisible <em>Filtering Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isCustomFilters <em>Custom Filters</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getHeaders <em>Headers</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isHeaderVisible <em>Header Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getFooters <em>Footers</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isFooterVisible <em>Footer Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isEditorEnabled <em>Editor Enabled</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorCancelI18nLabelKey <em>Editor Cancel I1 8n Label Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaveI18nLabelKey <em>Editor Save I1 8n Label Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaved <em>Editor Saved</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSetLastRefreshTime <em>Set Last Refresh Time</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid()
 * @model
 * @generated
 */
public interface CxGrid extends YInput, YCollectionBindable,
		YSelectionBindable, YMultiSelectionBindable, YBeanServiceConsumer {
	
	/**
	 * Returns the value of the '<em><b>Selection Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Type</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType
	 * @see #setSelectionType(YSelectionType)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_SelectionType()
	 * @model
	 * @generated
	 */
	YSelectionType getSelectionType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionType <em>Selection Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Selection Type</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType
	 * @see #getSelectionType()
	 * @generated
	 */
	void setSelectionType(YSelectionType value);

	/**
	 * Returns the value of the '<em><b>Selection Event Topic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection Event Topic</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Event Topic</em>' attribute.
	 * @see #setSelectionEventTopic(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_SelectionEventTopic()
	 * @model
	 * @generated
	 */
	String getSelectionEventTopic();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionEventTopic <em>Selection Event Topic</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Selection Event Topic</em>' attribute.
	 * @see #getSelectionEventTopic()
	 * @generated
	 */
	void setSelectionEventTopic(String value);

	/**
	 * Returns the value of the '<em><b>Selection</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Selection</em>' attribute.
	 * @see #setSelection(Object)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_Selection()
	 * @model transient="true"
	 * @generated
	 */
	Object getSelection();

	/**
	 * Sets the value of the '
	 * {@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelection
	 * <em>Selection</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Selection</em>' attribute.
	 * @see #getSelection()
	 * @generated
	 */
	void setSelection(Object value);

	/**
	 * Returns the value of the '<em><b>Multi Selection</b></em>' attribute
	 * list. The list contents are of type {@link java.lang.Object}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multi Selection</em>' attribute list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Multi Selection</em>' attribute list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_MultiSelection()
	 * @model transient="true"
	 * @generated
	 */
	EList<Object> getMultiSelection();

	/**
	 * Returns the value of the '<em><b>Collection</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Object}.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' attribute list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_Collection()
	 * @model transient="true"
	 * @generated
	 */
	EList<Object> getCollection();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(Class)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_Type()
	 * @model
	 * @generated
	 */
	Class<?> getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Emf Ns URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emf Ns URI</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #setEmfNsURI(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_EmfNsURI()
	 * @model
	 * @generated
	 */
	String getEmfNsURI();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEmfNsURI <em>Emf Ns URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #getEmfNsURI()
	 * @generated
	 */
	void setEmfNsURI(String value);

	/**
	 * Returns the value of the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Qualified Name</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #setTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_TypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getTypeQualifiedName <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #getTypeQualifiedName()
	 * @generated
	 */
	void setTypeQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Editor Enabled</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Editor Enabled</em>' attribute.
	 * @see #setEditorEnabled(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_EditorEnabled()
	 * @model
	 * @generated
	 */
	boolean isEditorEnabled();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isEditorEnabled <em>Editor Enabled</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Editor Enabled</em>' attribute.
	 * @see #isEditorEnabled()
	 * @generated
	 */
	void setEditorEnabled(boolean value);

	/**
	 * Returns the value of the '<em><b>Cell Style Generator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Style Generator</em>' containment
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Style Generator</em>' containment reference.
	 * @see #setCellStyleGenerator(CxGridCellStyleGenerator)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_CellStyleGenerator()
	 * @model containment="true"
	 * @generated
	 */
	CxGridCellStyleGenerator getCellStyleGenerator();

	/**
	 * Sets the value of the '
	 * {@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getCellStyleGenerator
	 * <em>Cell Style Generator</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Cell Style Generator</em>'
	 *            containment reference.
	 * @see #getCellStyleGenerator()
	 * @generated
	 */
	void setCellStyleGenerator(CxGridCellStyleGenerator value);

	/**
	 * Returns the value of the '<em><b>Filtering Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filtering Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filtering Visible</em>' attribute.
	 * @see #setFilteringVisible(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_FilteringVisible()
	 * @model transient="true"
	 * @generated
	 */
	boolean isFilteringVisible();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isFilteringVisible <em>Filtering Visible</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Filtering Visible</em>' attribute.
	 * @see #isFilteringVisible()
	 * @generated
	 */
	void setFilteringVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Custom Filters</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Custom Filters</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Filters</em>' attribute.
	 * @see #setCustomFilters(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_CustomFilters()
	 * @model default="false"
	 * @generated
	 */
	boolean isCustomFilters();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isCustomFilters <em>Custom Filters</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Custom Filters</em>' attribute.
	 * @see #isCustomFilters()
	 * @generated
	 */
	void setCustomFilters(boolean value);

	/**
	 * Returns the value of the '<em><b>Headers</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Headers</em>' containment reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Headers</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_Headers()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridHeaderRow> getHeaders();

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn}.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Columns</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_Columns()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridColumn> getColumns();

	/**
	 * Returns the value of the '<em><b>Sort Order</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.extension.grid.CxGridSortable}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sort Order</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Sort Order</em>' containment reference
	 *         list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_SortOrder()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridSortable> getSortOrder();

	/**
	 * Returns the value of the '<em><b>Column Reordering Allowed</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Column Reordering Allowed</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column Reordering Allowed</em>' attribute.
	 * @see #setColumnReorderingAllowed(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_ColumnReorderingAllowed()
	 * @model default="true"
	 * @generated
	 */
	boolean isColumnReorderingAllowed();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isColumnReorderingAllowed <em>Column Reordering Allowed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Column Reordering Allowed</em>' attribute.
	 * @see #isColumnReorderingAllowed()
	 * @generated
	 */
	void setColumnReorderingAllowed(boolean value);

	/**
	 * Returns the value of the '<em><b>Footer Visible</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Footer Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Footer Visible</em>' attribute.
	 * @see #setFooterVisible(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_FooterVisible()
	 * @model
	 * @generated
	 */
	boolean isFooterVisible();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isFooterVisible <em>Footer Visible</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Footer Visible</em>' attribute.
	 * @see #isFooterVisible()
	 * @generated
	 */
	void setFooterVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Header Visible</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Visible</em>' attribute.
	 * @see #setHeaderVisible(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_HeaderVisible()
	 * @model default="true"
	 * @generated
	 */
	boolean isHeaderVisible();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isHeaderVisible <em>Header Visible</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Header Visible</em>' attribute.
	 * @see #isHeaderVisible()
	 * @generated
	 */
	void setHeaderVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Footers</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Footers</em>' containment reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Footers</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_Footers()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridFooterRow> getFooters();

	/**
	 * Returns the value of the '<em><b>Editor Cancel I1 8n Label Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Cancel I1 8n Label Key</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Cancel I1 8n Label Key</em>' attribute.
	 * @see #setEditorCancelI18nLabelKey(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_EditorCancelI18nLabelKey()
	 * @model
	 * @generated
	 */
	String getEditorCancelI18nLabelKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorCancelI18nLabelKey <em>Editor Cancel I1 8n Label Key</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Cancel I1 8n Label Key</em>' attribute.
	 * @see #getEditorCancelI18nLabelKey()
	 * @generated
	 */
	void setEditorCancelI18nLabelKey(String value);

	/**
	 * Returns the value of the '<em><b>Editor Save I1 8n Label Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Save I1 8n Label Key</em>' attribute
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Save I1 8n Label Key</em>' attribute.
	 * @see #setEditorSaveI18nLabelKey(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_EditorSaveI18nLabelKey()
	 * @model
	 * @generated
	 */
	String getEditorSaveI18nLabelKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaveI18nLabelKey <em>Editor Save I1 8n Label Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Save I1 8n Label Key</em>' attribute.
	 * @see #getEditorSaveI18nLabelKey()
	 * @generated
	 */
	void setEditorSaveI18nLabelKey(String value);

	/**
	 * Returns the value of the '<em><b>Editor Saved</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Saved</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Editor Saved</em>' attribute.
	 * @see #setEditorSaved(Object)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_EditorSaved()
	 * @model transient="true"
	 * @generated
	 */
	Object getEditorSaved();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaved <em>Editor Saved</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Editor Saved</em>' attribute.
	 * @see #getEditorSaved()
	 * @generated
	 */
	void setEditorSaved(Object value);

	/**
	 * Returns the value of the '<em><b>Set Last Refresh Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Last Refresh Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Last Refresh Time</em>' attribute.
	 * @see #setSetLastRefreshTime(long)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGrid_SetLastRefreshTime()
	 * @model
	 * @generated
	 */
	long getSetLastRefreshTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSetLastRefreshTime <em>Set Last Refresh Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Set Last Refresh Time</em>' attribute.
	 * @see #getSetLastRefreshTime()
	 * @generated
	 */
	void setSetLastRefreshTime(long value);

	/**
	 * <!-- begin-user-doc --> Returns a new instance of YHelperLayout
	 * referencing <b>(NOT containing)</b> all editor fields. <!-- end-user-doc
	 * -->
	 *
	 * @return the y helper layout
	 * @model
	 * @generated
	 */
	YHelperLayout createEditorFieldHelperLayout();

	/**
	 * Creates a binding endpoint which passes the editor input if "save" was
	 * pressed.
	 * <p>
	 * Operations about save need to be handled outside.
	 *
	 * @return the y value binding endpoint
	 */
	YValueBindingEndpoint createEditorUpdatedEndpoint();

}
/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.visibility.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.osbp.ecview.core.common.model.visibility.impl.YVisibilityProcessorImpl;
import org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YAuthorization Visibility Processor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class YAuthorizationVisibilityProcessorImpl extends YVisibilityProcessorImpl implements YAuthorizationVisibilityProcessor {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YAuthorizationVisibilityProcessorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YVisibilityPackage.Literals.YAUTHORIZATION_VISIBILITY_PROCESSOR;
	}

}

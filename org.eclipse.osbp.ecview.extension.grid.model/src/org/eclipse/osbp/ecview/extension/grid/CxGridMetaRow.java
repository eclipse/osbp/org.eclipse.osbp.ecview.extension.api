/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Row</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow#getGroupings <em>Groupings</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow#getCustomCells <em>Custom Cells</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaRow()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface CxGridMetaRow extends YElement, CxGridProvider {
	
	/**
	 * Returns the value of the '<em><b>Groupings</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Groupings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groupings</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaRow_Groupings()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridGroupedCell> getGroupings();

	/**
	 * Returns the value of the '<em><b>Custom Cells</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Custom Cells</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Cells</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaRow_CustomCells()
	 * @model containment="true"
	 * @generated
	 */
	EList<CxGridMetaCell> getCustomCells();

}
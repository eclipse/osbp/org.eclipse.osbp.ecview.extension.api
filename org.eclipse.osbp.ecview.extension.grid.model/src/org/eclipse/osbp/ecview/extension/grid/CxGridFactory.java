/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage
 * @generated
 */
public interface CxGridFactory extends EFactory {
	
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CxGridFactory eINSTANCE = org.eclipse.osbp.ecview.extension.grid.impl.CxGridFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Cx Grid</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cx Grid</em>'.
	 * @generated
	 */
	CxGrid createCxGrid();

	/**
	 * Returns a new object of class '<em>Header Row</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Header Row</em>'.
	 * @generated
	 */
	CxGridHeaderRow createCxGridHeaderRow();

	/**
	 * Returns a new object of class '<em>Footer Row</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Footer Row</em>'.
	 * @generated
	 */
	CxGridFooterRow createCxGridFooterRow();

	/**
	 * Returns a new object of class '<em>Filter Row</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Filter Row</em>'.
	 * @generated
	 */
	CxGridFilterRow createCxGridFilterRow();

	/**
	 * Returns a new object of class '<em>Meta Cell</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Meta Cell</em>'.
	 * @generated
	 */
	CxGridMetaCell createCxGridMetaCell();

	/**
	 * Returns a new object of class '<em>Grouped Cell</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Grouped Cell</em>'.
	 * @generated
	 */
	CxGridGroupedCell createCxGridGroupedCell();

	/**
	 * Returns a new object of class '<em>Column</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Column</em>'.
	 * @generated
	 */
	CxGridColumn createCxGridColumn();

	/**
	 * Returns a new object of class '<em>Delegate Cell Style Generator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delegate Cell Style Generator</em>'.
	 * @generated
	 */
	CxGridDelegateCellStyleGenerator createCxGridDelegateCellStyleGenerator();

	/**
	 * Returns a new object of class '<em>Sortable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sortable</em>'.
	 * @generated
	 */
	CxGridSortable createCxGridSortable();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CxGridPackage getCxGridPackage();

}
/**
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cx Grid Delegate Renderer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridDelegateRendererTest extends CxGridRendererTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridDelegateRendererTest.class);
	}

	/**
	 * Constructs a new Cx Grid Delegate Renderer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridDelegateRendererTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Cx Grid Delegate Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CxGridDelegateRenderer getFixture() {
		return (CxGridDelegateRenderer)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridRendererFactory.eINSTANCE.createCxGridDelegateRenderer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridDelegateRendererTest

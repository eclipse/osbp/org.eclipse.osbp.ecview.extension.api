/**
 *                                                                            
 *  Copyright (c) 2011, 2015 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>model</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class YECviewTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new YECviewTests("model Tests");
		suite.addTestSuite(YStrategyLayoutTest.class);
		suite.addTestSuite(YLayoutingStrategyTest.class);
		suite.addTestSuite(YDefaultLayoutingStrategyTest.class);
		suite.addTestSuite(YFocusingStrategyTest.class);
		suite.addTestSuite(YDelegatingLayoutingStrategyTest.class);
		suite.addTestSuite(YDelegatingFocusingStrategyTest.class);
		suite.addTestSuite(YBlobUploadComponentTest.class);
		suite.addTestSuite(YCustomDecimalFieldTest.class);
		suite.addTestSuite(YI18nComboBoxTest.class);
		suite.addTestSuite(YIconComboBoxTest.class);
		suite.addTestSuite(YQuantityTextFieldTest.class);
		suite.addTestSuite(YContentSensitiveLayoutTest.class);
		suite.addTestSuite(YRichTextAreaTest.class);
		suite.addTestSuite(YMaskedTextFieldTest.class);
		suite.addTestSuite(YPrefixedMaskedTextFieldTest.class);
		suite.addTestSuite(YMaskedNumericFieldTest.class);
		suite.addTestSuite(YMaskedDecimalFieldTest.class);
		suite.addTestSuite(YPairComboBoxTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YECviewTests(String name) {
		super(name);
	}

} //YECviewTests

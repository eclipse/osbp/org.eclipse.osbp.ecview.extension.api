/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.model.YECviewFactory
 * @model kind="package"
 * @generated
 */
public interface YECviewPackage extends EPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.de/ecview/v1/extension";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "model";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	YECviewPackage eINSTANCE = org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl <em>YStrategy Layout</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYStrategyLayout()
	 * @generated
	 */
	int YSTRATEGY_LAYOUT = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__TAGS = CoreModelPackage.YEMBEDDABLE__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__ID = CoreModelPackage.YEMBEDDABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__NAME = CoreModelPackage.YEMBEDDABLE__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__PROPERTIES = CoreModelPackage.YEMBEDDABLE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__CSS_CLASS = CoreModelPackage.YEMBEDDABLE__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__CSS_ID = CoreModelPackage.YEMBEDDABLE__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__INITIAL_VISIBLE = CoreModelPackage.YEMBEDDABLE__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__VISIBLE = CoreModelPackage.YEMBEDDABLE__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__AUTHORIZATION_GROUP = CoreModelPackage.YEMBEDDABLE__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__AUTHORIZATION_ID = CoreModelPackage.YEMBEDDABLE__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__ORPHAN_DATATYPES = CoreModelPackage.YEMBEDDABLE__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__ORPHAN_DATADESCRIPTIONS = CoreModelPackage.YEMBEDDABLE__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__MEMENTO_ENABLED = CoreModelPackage.YEMBEDDABLE__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__MEMENTO_ID = CoreModelPackage.YEMBEDDABLE__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__LABEL = CoreModelPackage.YEMBEDDABLE__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__LABEL_I1_8N_KEY = CoreModelPackage.YEMBEDDABLE__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__LAST_CONTEXT_CLICK = CoreModelPackage.YEMBEDDABLE__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__READONLY = CoreModelPackage.YEMBEDDABLE__READONLY;

	/**
	 * The feature id for the '<em><b>Layouting Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Focusing Strategies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Suspects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__SUSPECTS = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Layouting Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__LAYOUTING_INFO = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Default Focusing Enhancer Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Number Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__NUMBER_COLUMNS = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Save And New</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT__SAVE_AND_NEW = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>YStrategy Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRATEGY_LAYOUT_FEATURE_COUNT = CoreModelPackage.YEMBEDDABLE_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YLayoutingStrategyImpl <em>YLayouting Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YLayoutingStrategyImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYLayoutingStrategy()
	 * @generated
	 */
	int YLAYOUTING_STRATEGY = 1;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_STRATEGY__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_STRATEGY__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_STRATEGY__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_STRATEGY__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_STRATEGY__TRIGGER = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YLayouting Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_STRATEGY_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YDefaultLayoutingStrategyImpl <em>YDefault Layouting Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YDefaultLayoutingStrategyImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYDefaultLayoutingStrategy()
	 * @generated
	 */
	int YDEFAULT_LAYOUTING_STRATEGY = 2;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDEFAULT_LAYOUTING_STRATEGY__TAGS = YLAYOUTING_STRATEGY__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDEFAULT_LAYOUTING_STRATEGY__ID = YLAYOUTING_STRATEGY__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDEFAULT_LAYOUTING_STRATEGY__NAME = YLAYOUTING_STRATEGY__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDEFAULT_LAYOUTING_STRATEGY__PROPERTIES = YLAYOUTING_STRATEGY__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDEFAULT_LAYOUTING_STRATEGY__TRIGGER = YLAYOUTING_STRATEGY__TRIGGER;

	/**
	 * The number of structural features of the '<em>YDefault Layouting Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDEFAULT_LAYOUTING_STRATEGY_FEATURE_COUNT = YLAYOUTING_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl <em>YFocusing Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYFocusingStrategy()
	 * @generated
	 */
	int YFOCUSING_STRATEGY = 3;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFOCUSING_STRATEGY__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFOCUSING_STRATEGY__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFOCUSING_STRATEGY__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFOCUSING_STRATEGY__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Key Stroke Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Temp Stroke Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>YFocusing Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFOCUSING_STRATEGY_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YDelegatingLayoutingStrategyImpl <em>YDelegating Layouting Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YDelegatingLayoutingStrategyImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYDelegatingLayoutingStrategy()
	 * @generated
	 */
	int YDELEGATING_LAYOUTING_STRATEGY = 4;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_LAYOUTING_STRATEGY__TAGS = YLAYOUTING_STRATEGY__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_LAYOUTING_STRATEGY__ID = YLAYOUTING_STRATEGY__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_LAYOUTING_STRATEGY__NAME = YLAYOUTING_STRATEGY__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_LAYOUTING_STRATEGY__PROPERTIES = YLAYOUTING_STRATEGY__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_LAYOUTING_STRATEGY__TRIGGER = YLAYOUTING_STRATEGY__TRIGGER;

	/**
	 * The feature id for the '<em><b>Delegate Strategy Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID = YLAYOUTING_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YDelegating Layouting Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_LAYOUTING_STRATEGY_FEATURE_COUNT = YLAYOUTING_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YDelegatingFocusingStrategyImpl <em>YDelegating Focusing Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YDelegatingFocusingStrategyImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYDelegatingFocusingStrategy()
	 * @generated
	 */
	int YDELEGATING_FOCUSING_STRATEGY = 5;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY__TAGS = YFOCUSING_STRATEGY__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY__ID = YFOCUSING_STRATEGY__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY__NAME = YFOCUSING_STRATEGY__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY__PROPERTIES = YFOCUSING_STRATEGY__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Key Stroke Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY__KEY_STROKE_DEFINITION = YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Temp Stroke Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY__TEMP_STROKE_DEFINITION = YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Delegate Strategy Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY__DELEGATE_STRATEGY_ID = YFOCUSING_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YDelegating Focusing Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDELEGATING_FOCUSING_STRATEGY_FEATURE_COUNT = YFOCUSING_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectImpl <em>YSuspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YSuspectImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSuspect()
	 * @generated
	 */
	int YSUSPECT = 6;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__AUTHORIZATION_GROUP = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__AUTHORIZATION_ID = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__LABEL_I1_8N_KEY = CoreModelPackage.YELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Image I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__IMAGE_I1_8N_KEY = CoreModelPackage.YELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__VALUE_BINDING_ENDPOINTS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__VISIBILITY_PROCESSORS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__COMMANDS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Assoc Newiated Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__ASSOC_NEWIATED_ELEMENTS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Associated Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__ASSOCIATED_BINDINGS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__LABEL = CoreModelPackage.YELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__GROUP_NAME = CoreModelPackage.YELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Style Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT__STYLE_NAME = CoreModelPackage.YELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>YSuspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 13;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YLayoutingInfoImpl <em>YLayouting Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YLayoutingInfoImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYLayoutingInfo()
	 * @generated
	 */
	int YLAYOUTING_INFO = 11;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl <em>YSuspect Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSuspectInfo()
	 * @generated
	 */
	int YSUSPECT_INFO = 12;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YBlobUploadComponentImpl <em>YBlob Upload Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YBlobUploadComponentImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYBlobUploadComponent()
	 * @generated
	 */
	int YBLOB_UPLOAD_COMPONENT = 13;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YTypedSuspectImpl <em>YTyped Suspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YTypedSuspectImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYTypedSuspect()
	 * @generated
	 */
	int YTYPED_SUSPECT = 7;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__TAGS = YSUSPECT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__ID = YSUSPECT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__NAME = YSUSPECT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__PROPERTIES = YSUSPECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__AUTHORIZATION_GROUP = YSUSPECT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__AUTHORIZATION_ID = YSUSPECT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__LABEL_I1_8N_KEY = YSUSPECT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Image I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__IMAGE_I1_8N_KEY = YSUSPECT__IMAGE_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__VALUE_BINDING_ENDPOINTS = YSUSPECT__VALUE_BINDING_ENDPOINTS;

	/**
	 * The feature id for the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__VISIBILITY_PROCESSORS = YSUSPECT__VISIBILITY_PROCESSORS;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__COMMANDS = YSUSPECT__COMMANDS;

	/**
	 * The feature id for the '<em><b>Assoc Newiated Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__ASSOC_NEWIATED_ELEMENTS = YSUSPECT__ASSOC_NEWIATED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Associated Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__ASSOCIATED_BINDINGS = YSUSPECT__ASSOCIATED_BINDINGS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__LABEL = YSUSPECT__LABEL;

	/**
	 * The feature id for the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT = YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__GROUP_NAME = YSUSPECT__GROUP_NAME;

	/**
	 * The feature id for the '<em><b>Style Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__STYLE_NAME = YSUSPECT__STYLE_NAME;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__TYPE_QUALIFIED_NAME = YSUSPECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT__TYPE = YSUSPECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>YTyped Suspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_SUSPECT_FEATURE_COUNT = YSUSPECT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YTypedCompoundSuspectImpl <em>YTyped Compound Suspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YTypedCompoundSuspectImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYTypedCompoundSuspect()
	 * @generated
	 */
	int YTYPED_COMPOUND_SUSPECT = 8;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__TAGS = YTYPED_SUSPECT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__ID = YTYPED_SUSPECT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__NAME = YTYPED_SUSPECT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__PROPERTIES = YTYPED_SUSPECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__AUTHORIZATION_GROUP = YTYPED_SUSPECT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__AUTHORIZATION_ID = YTYPED_SUSPECT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__LABEL_I1_8N_KEY = YTYPED_SUSPECT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Image I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__IMAGE_I1_8N_KEY = YTYPED_SUSPECT__IMAGE_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__VALUE_BINDING_ENDPOINTS = YTYPED_SUSPECT__VALUE_BINDING_ENDPOINTS;

	/**
	 * The feature id for the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__VISIBILITY_PROCESSORS = YTYPED_SUSPECT__VISIBILITY_PROCESSORS;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__COMMANDS = YTYPED_SUSPECT__COMMANDS;

	/**
	 * The feature id for the '<em><b>Assoc Newiated Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__ASSOC_NEWIATED_ELEMENTS = YTYPED_SUSPECT__ASSOC_NEWIATED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Associated Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__ASSOCIATED_BINDINGS = YTYPED_SUSPECT__ASSOCIATED_BINDINGS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__LABEL = YTYPED_SUSPECT__LABEL;

	/**
	 * The feature id for the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT = YTYPED_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__GROUP_NAME = YTYPED_SUSPECT__GROUP_NAME;

	/**
	 * The feature id for the '<em><b>Style Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__STYLE_NAME = YTYPED_SUSPECT__STYLE_NAME;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__TYPE_QUALIFIED_NAME = YTYPED_SUSPECT__TYPE_QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__TYPE = YTYPED_SUSPECT__TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT__CHILDREN = YTYPED_SUSPECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YTyped Compound Suspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YTYPED_COMPOUND_SUSPECT_FEATURE_COUNT = YTYPED_SUSPECT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSubTypeBaseSuspectImpl <em>YSub Type Base Suspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YSubTypeBaseSuspectImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSubTypeBaseSuspect()
	 * @generated
	 */
	int YSUB_TYPE_BASE_SUSPECT = 9;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__TAGS = YTYPED_COMPOUND_SUSPECT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__ID = YTYPED_COMPOUND_SUSPECT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__NAME = YTYPED_COMPOUND_SUSPECT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__PROPERTIES = YTYPED_COMPOUND_SUSPECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__AUTHORIZATION_GROUP = YTYPED_COMPOUND_SUSPECT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__AUTHORIZATION_ID = YTYPED_COMPOUND_SUSPECT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__LABEL_I1_8N_KEY = YTYPED_COMPOUND_SUSPECT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Image I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__IMAGE_I1_8N_KEY = YTYPED_COMPOUND_SUSPECT__IMAGE_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__VALUE_BINDING_ENDPOINTS = YTYPED_COMPOUND_SUSPECT__VALUE_BINDING_ENDPOINTS;

	/**
	 * The feature id for the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__VISIBILITY_PROCESSORS = YTYPED_COMPOUND_SUSPECT__VISIBILITY_PROCESSORS;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__COMMANDS = YTYPED_COMPOUND_SUSPECT__COMMANDS;

	/**
	 * The feature id for the '<em><b>Assoc Newiated Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__ASSOC_NEWIATED_ELEMENTS = YTYPED_COMPOUND_SUSPECT__ASSOC_NEWIATED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Associated Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__ASSOCIATED_BINDINGS = YTYPED_COMPOUND_SUSPECT__ASSOCIATED_BINDINGS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__LABEL = YTYPED_COMPOUND_SUSPECT__LABEL;

	/**
	 * The feature id for the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT = YTYPED_COMPOUND_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__GROUP_NAME = YTYPED_COMPOUND_SUSPECT__GROUP_NAME;

	/**
	 * The feature id for the '<em><b>Style Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__STYLE_NAME = YTYPED_COMPOUND_SUSPECT__STYLE_NAME;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__TYPE_QUALIFIED_NAME = YTYPED_COMPOUND_SUSPECT__TYPE_QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__TYPE = YTYPED_COMPOUND_SUSPECT__TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT__CHILDREN = YTYPED_COMPOUND_SUSPECT__CHILDREN;

	/**
	 * The number of structural features of the '<em>YSub Type Base Suspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_BASE_SUSPECT_FEATURE_COUNT = YTYPED_COMPOUND_SUSPECT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSubTypeSuspectImpl <em>YSub Type Suspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YSubTypeSuspectImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSubTypeSuspect()
	 * @generated
	 */
	int YSUB_TYPE_SUSPECT = 10;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__TAGS = YTYPED_COMPOUND_SUSPECT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__ID = YTYPED_COMPOUND_SUSPECT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__NAME = YTYPED_COMPOUND_SUSPECT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__PROPERTIES = YTYPED_COMPOUND_SUSPECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__AUTHORIZATION_GROUP = YTYPED_COMPOUND_SUSPECT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__AUTHORIZATION_ID = YTYPED_COMPOUND_SUSPECT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__LABEL_I1_8N_KEY = YTYPED_COMPOUND_SUSPECT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Image I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__IMAGE_I1_8N_KEY = YTYPED_COMPOUND_SUSPECT__IMAGE_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__VALUE_BINDING_ENDPOINTS = YTYPED_COMPOUND_SUSPECT__VALUE_BINDING_ENDPOINTS;

	/**
	 * The feature id for the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__VISIBILITY_PROCESSORS = YTYPED_COMPOUND_SUSPECT__VISIBILITY_PROCESSORS;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__COMMANDS = YTYPED_COMPOUND_SUSPECT__COMMANDS;

	/**
	 * The feature id for the '<em><b>Assoc Newiated Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__ASSOC_NEWIATED_ELEMENTS = YTYPED_COMPOUND_SUSPECT__ASSOC_NEWIATED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Associated Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__ASSOCIATED_BINDINGS = YTYPED_COMPOUND_SUSPECT__ASSOCIATED_BINDINGS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__LABEL = YTYPED_COMPOUND_SUSPECT__LABEL;

	/**
	 * The feature id for the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT = YTYPED_COMPOUND_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__GROUP_NAME = YTYPED_COMPOUND_SUSPECT__GROUP_NAME;

	/**
	 * The feature id for the '<em><b>Style Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__STYLE_NAME = YTYPED_COMPOUND_SUSPECT__STYLE_NAME;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__TYPE_QUALIFIED_NAME = YTYPED_COMPOUND_SUSPECT__TYPE_QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__TYPE = YTYPED_COMPOUND_SUSPECT__TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__CHILDREN = YTYPED_COMPOUND_SUSPECT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Bean Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT__BEAN_SLOT = YTYPED_COMPOUND_SUSPECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YSub Type Suspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_SUSPECT_FEATURE_COUNT = YTYPED_COMPOUND_SUSPECT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Layout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__LAYOUT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__CONTENT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Active Suspect Infos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>First Focus</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO__FIRST_FOCUS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>YLayouting Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YLAYOUTING_INFO_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Suspect</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__SUSPECT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__BINDINGS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Next Focus</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__NEXT_FOCUS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Previous Focus</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__PREVIOUS_FOCUS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__TARGET = CoreModelPackage.YELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO__VISIBILITY_PROCESSORS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>YSuspect Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUSPECT_INFO_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Display Resolution Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__DISPLAY_RESOLUTION_ID = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Firmly Linked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__FIRMLY_LINKED = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Unique Name Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT__UNIQUE_NAME_ENABLED = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>YBlob Upload Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YBLOB_UPLOAD_COMPONENT_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YCustomDecimalFieldImpl <em>YCustom Decimal Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YCustomDecimalFieldImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYCustomDecimalField()
	 * @generated
	 */
	int YCUSTOM_DECIMAL_FIELD = 14;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__DATATYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>YCustom Decimal Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_FIELD_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YI18nComboBoxImpl <em>YI1 8n Combo Box</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YI18nComboBoxImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYI18nComboBox()
	 * @generated
	 */
	int YI1_8N_COMBO_BOX = 15;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Collection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__COLLECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__SELECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__DATATYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__SELECTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__TYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Emf Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__EMF_NS_URI = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX__TYPE_QUALIFIED_NAME = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>YI1 8n Combo Box</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YI1_8N_COMBO_BOX_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YIconComboBoxImpl <em>YIcon Combo Box</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YIconComboBoxImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYIconComboBox()
	 * @generated
	 */
	int YICON_COMBO_BOX = 16;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Collection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__COLLECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__SELECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Use Bean Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__USE_BEAN_SERVICE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__DATATYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__SELECTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__TYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Emf Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__EMF_NS_URI = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__TYPE_QUALIFIED_NAME = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Caption Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__CAPTION_PROPERTY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Image Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__IMAGE_PROPERTY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Description Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__DESCRIPTION_PROPERTY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX__DESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>YIcon Combo Box</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YICON_COMBO_BOX_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 13;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YQuantityTextFieldImpl <em>YQuantity Text Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YQuantityTextFieldImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYQuantityTextField()
	 * @generated
	 */
	int YQUANTITY_TEXT_FIELD = 17;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>YQuantity Text Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TEXT_FIELD_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YCollectionSuspectImpl <em>YCollection Suspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YCollectionSuspectImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYCollectionSuspect()
	 * @generated
	 */
	int YCOLLECTION_SUSPECT = 18;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__TAGS = YTYPED_SUSPECT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__ID = YTYPED_SUSPECT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__NAME = YTYPED_SUSPECT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__PROPERTIES = YTYPED_SUSPECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__AUTHORIZATION_GROUP = YTYPED_SUSPECT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__AUTHORIZATION_ID = YTYPED_SUSPECT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__LABEL_I1_8N_KEY = YTYPED_SUSPECT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Image I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__IMAGE_I1_8N_KEY = YTYPED_SUSPECT__IMAGE_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__VALUE_BINDING_ENDPOINTS = YTYPED_SUSPECT__VALUE_BINDING_ENDPOINTS;

	/**
	 * The feature id for the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__VISIBILITY_PROCESSORS = YTYPED_SUSPECT__VISIBILITY_PROCESSORS;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__COMMANDS = YTYPED_SUSPECT__COMMANDS;

	/**
	 * The feature id for the '<em><b>Assoc Newiated Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__ASSOC_NEWIATED_ELEMENTS = YTYPED_SUSPECT__ASSOC_NEWIATED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Associated Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__ASSOCIATED_BINDINGS = YTYPED_SUSPECT__ASSOCIATED_BINDINGS;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__LABEL = YTYPED_SUSPECT__LABEL;

	/**
	 * The feature id for the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT = YTYPED_SUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__GROUP_NAME = YTYPED_SUSPECT__GROUP_NAME;

	/**
	 * The feature id for the '<em><b>Style Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__STYLE_NAME = YTYPED_SUSPECT__STYLE_NAME;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__TYPE_QUALIFIED_NAME = YTYPED_SUSPECT__TYPE_QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__TYPE = YTYPED_SUSPECT__TYPE;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT__COLUMNS = YTYPED_SUSPECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YCollection Suspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLLECTION_SUSPECT_FEATURE_COUNT = YTYPED_SUSPECT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YColumnInfoImpl <em>YColumn Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YColumnInfoImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYColumnInfo()
	 * @generated
	 */
	int YCOLUMN_INFO = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLUMN_INFO__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLUMN_INFO__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLUMN_INFO__TYPE_QUALIFIED_NAME = 2;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLUMN_INFO__PROPERTIES = 3;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLUMN_INFO__LABEL_I1_8N_KEY = 4;

	/**
	 * The feature id for the '<em><b>Source Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLUMN_INFO__SOURCE_TYPE = 5;

	/**
	 * The number of structural features of the '<em>YColumn Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCOLUMN_INFO_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YContentSensitiveLayoutImpl <em>YContent Sensitive Layout</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YContentSensitiveLayoutImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYContentSensitiveLayout()
	 * @generated
	 */
	int YCONTENT_SENSITIVE_LAYOUT = 20;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__TAGS = CoreModelPackage.YLAYOUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__ID = CoreModelPackage.YLAYOUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__NAME = CoreModelPackage.YLAYOUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__PROPERTIES = CoreModelPackage.YLAYOUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__CSS_CLASS = CoreModelPackage.YLAYOUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__CSS_ID = CoreModelPackage.YLAYOUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__INITIAL_VISIBLE = CoreModelPackage.YLAYOUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__VISIBLE = CoreModelPackage.YLAYOUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__AUTHORIZATION_GROUP = CoreModelPackage.YLAYOUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__AUTHORIZATION_ID = CoreModelPackage.YLAYOUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__ORPHAN_DATATYPES = CoreModelPackage.YLAYOUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__ORPHAN_DATADESCRIPTIONS = CoreModelPackage.YLAYOUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__MEMENTO_ENABLED = CoreModelPackage.YLAYOUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__MEMENTO_ID = CoreModelPackage.YLAYOUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__LABEL = CoreModelPackage.YLAYOUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__LABEL_I1_8N_KEY = CoreModelPackage.YLAYOUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__LAST_CONTEXT_CLICK = CoreModelPackage.YLAYOUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__READONLY = CoreModelPackage.YLAYOUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__INITIAL_EDITABLE = CoreModelPackage.YLAYOUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__EDITABLE = CoreModelPackage.YLAYOUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__INITIAL_ENABLED = CoreModelPackage.YLAYOUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__ENABLED = CoreModelPackage.YLAYOUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__ELEMENTS = CoreModelPackage.YLAYOUT__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Last Component Attach</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__LAST_COMPONENT_ATTACH = CoreModelPackage.YLAYOUT__LAST_COMPONENT_ATTACH;

	/**
	 * The feature id for the '<em><b>Last Component Detach</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__LAST_COMPONENT_DETACH = CoreModelPackage.YLAYOUT__LAST_COMPONENT_DETACH;

	/**
	 * The feature id for the '<em><b>Number Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__NUMBER_COLUMNS = CoreModelPackage.YLAYOUT__NUMBER_COLUMNS;

	/**
	 * The feature id for the '<em><b>Save And New</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__SAVE_AND_NEW = CoreModelPackage.YLAYOUT__SAVE_AND_NEW;

	/**
	 * The feature id for the '<em><b>Spacing</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__SPACING = CoreModelPackage.YLAYOUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Margin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT__MARGIN = CoreModelPackage.YLAYOUT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>YContent Sensitive Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCONTENT_SENSITIVE_LAYOUT_FEATURE_COUNT = CoreModelPackage.YLAYOUT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YRichTextAreaImpl <em>YRich Text Area</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YRichTextAreaImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYRichTextArea()
	 * @generated
	 */
	int YRICH_TEXT_AREA = 21;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Blob Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__BLOB_VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Use Blob</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA__USE_BLOB = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>YRich Text Area</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YRICH_TEXT_AREA_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YPairComboBoxImpl <em>YPair Combo Box</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YPairComboBoxImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYPairComboBox()
	 * @generated
	 */
	int YPAIR_COMBO_BOX = 26;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YMaskedTextFieldImpl <em>YMasked Text Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YMaskedTextFieldImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYMaskedTextField()
	 * @generated
	 */
	int YMASKED_TEXT_FIELD = 22;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mask</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD__MASK = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>YMasked Text Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_TEXT_FIELD_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YPrefixedMaskedTextFieldImpl <em>YPrefixed Masked Text Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YPrefixedMaskedTextFieldImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYPrefixedMaskedTextField()
	 * @generated
	 */
	int YPREFIXED_MASKED_TEXT_FIELD = 23;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mask</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__MASK = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Prefixes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD__PREFIXES = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>YPrefixed Masked Text Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPREFIXED_MASKED_TEXT_FIELD_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YMaskedNumericFieldImpl <em>YMasked Numeric Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YMaskedNumericFieldImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYMaskedNumericField()
	 * @generated
	 */
	int YMASKED_NUMERIC_FIELD = 24;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mask</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD__MASK = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>YMasked Numeric Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_NUMERIC_FIELD_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YMaskedDecimalFieldImpl <em>YMasked Decimal Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YMaskedDecimalFieldImpl
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYMaskedDecimalField()
	 * @generated
	 */
	int YMASKED_DECIMAL_FIELD = 25;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Value Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__VALUE_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__VALUE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mask</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__MASK = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Decimal Separator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__DECIMAL_SEPARATOR = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Grouping Separator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD__GROUPING_SEPARATOR = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>YMasked Decimal Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YMASKED_DECIMAL_FIELD_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Collection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__COLLECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__SELECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Use Bean Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__USE_BEAN_SERVICE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__DATADESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__DATATYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__SELECTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__COLLECTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__TYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Emf Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__EMF_NS_URI = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__TYPE_QUALIFIED_NAME = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Caption Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__CAPTION_PROPERTY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Image Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__IMAGE_PROPERTY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Description Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__DESCRIPTION_PROPERTY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX__DESCRIPTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>YPair Combo Box</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPAIR_COMBO_BOX_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 14;

	/**
	 * The meta object id for the '<em>Number</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Number
	 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getNumber()
	 * @generated
	 */
	int NUMBER = 27;

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout <em>YStrategy Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YStrategy Layout</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout
	 * @generated
	 */
	EClass getYStrategyLayout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingStrategy <em>Layouting Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Layouting Strategy</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingStrategy()
	 * @see #getYStrategyLayout()
	 * @generated
	 */
	EReference getYStrategyLayout_LayoutingStrategy();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getFocusingStrategies <em>Focusing Strategies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Focusing Strategies</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getFocusingStrategies()
	 * @see #getYStrategyLayout()
	 * @generated
	 */
	EReference getYStrategyLayout_FocusingStrategies();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getSuspects <em>Suspects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Suspects</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getSuspects()
	 * @see #getYStrategyLayout()
	 * @generated
	 */
	EReference getYStrategyLayout_Suspects();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingInfo <em>Layouting Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Layouting Info</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingInfo()
	 * @see #getYStrategyLayout()
	 * @generated
	 */
	EReference getYStrategyLayout_LayoutingInfo();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getDefaultFocusingEnhancerId <em>Default Focusing Enhancer Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Focusing Enhancer Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getDefaultFocusingEnhancerId()
	 * @see #getYStrategyLayout()
	 * @generated
	 */
	EAttribute getYStrategyLayout_DefaultFocusingEnhancerId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getNumberColumns <em>Number Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Columns</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getNumberColumns()
	 * @see #getYStrategyLayout()
	 * @generated
	 */
	EAttribute getYStrategyLayout_NumberColumns();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#isSaveAndNew <em>Save And New</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Save And New</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YStrategyLayout#isSaveAndNew()
	 * @see #getYStrategyLayout()
	 * @generated
	 */
	EAttribute getYStrategyLayout_SaveAndNew();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy <em>YLayouting Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YLayouting Strategy</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy
	 * @generated
	 */
	EClass getYLayoutingStrategy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy#getTrigger()
	 * @see #getYLayoutingStrategy()
	 * @generated
	 */
	EAttribute getYLayoutingStrategy_Trigger();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YDefaultLayoutingStrategy <em>YDefault Layouting Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YDefault Layouting Strategy</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YDefaultLayoutingStrategy
	 * @generated
	 */
	EClass getYDefaultLayoutingStrategy();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy <em>YFocusing Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YFocusing Strategy</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YFocusingStrategy
	 * @generated
	 */
	EClass getYFocusingStrategy();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getKeyStrokeDefinition <em>Key Stroke Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Key Stroke Definition</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getKeyStrokeDefinition()
	 * @see #getYFocusingStrategy()
	 * @generated
	 */
	EReference getYFocusingStrategy_KeyStrokeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getTempStrokeDefinition <em>Temp Stroke Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Temp Stroke Definition</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YFocusingStrategy#getTempStrokeDefinition()
	 * @see #getYFocusingStrategy()
	 * @generated
	 */
	EReference getYFocusingStrategy_TempStrokeDefinition();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy <em>YDelegating Layouting Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YDelegating Layouting Strategy</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy
	 * @generated
	 */
	EClass getYDelegatingLayoutingStrategy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy#getDelegateStrategyId <em>Delegate Strategy Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delegate Strategy Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy#getDelegateStrategyId()
	 * @see #getYDelegatingLayoutingStrategy()
	 * @generated
	 */
	EAttribute getYDelegatingLayoutingStrategy_DelegateStrategyId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy <em>YDelegating Focusing Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YDelegating Focusing Strategy</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy
	 * @generated
	 */
	EClass getYDelegatingFocusingStrategy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy#getDelegateStrategyId <em>Delegate Strategy Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delegate Strategy Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy#getDelegateStrategyId()
	 * @see #getYDelegatingFocusingStrategy()
	 * @generated
	 */
	EAttribute getYDelegatingFocusingStrategy_DelegateStrategyId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YSuspect <em>YSuspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YSuspect</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect
	 * @generated
	 */
	EClass getYSuspect();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getLabelI18nKey <em>Label I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label I1 8n Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getLabelI18nKey()
	 * @see #getYSuspect()
	 * @generated
	 */
	EAttribute getYSuspect_LabelI18nKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getImageI18nKey <em>Image I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image I1 8n Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getImageI18nKey()
	 * @see #getYSuspect()
	 * @generated
	 */
	EAttribute getYSuspect_ImageI18nKey();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getValueBindingEndpoints <em>Value Binding Endpoints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value Binding Endpoints</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getValueBindingEndpoints()
	 * @see #getYSuspect()
	 * @generated
	 */
	EReference getYSuspect_ValueBindingEndpoints();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getVisibilityProcessors <em>Visibility Processors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Visibility Processors</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getVisibilityProcessors()
	 * @see #getYSuspect()
	 * @generated
	 */
	EReference getYSuspect_VisibilityProcessors();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getCommands <em>Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Commands</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getCommands()
	 * @see #getYSuspect()
	 * @generated
	 */
	EReference getYSuspect_Commands();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getAssocNewiatedElements <em>Assoc Newiated Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assoc Newiated Elements</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getAssocNewiatedElements()
	 * @see #getYSuspect()
	 * @generated
	 */
	EReference getYSuspect_AssocNewiatedElements();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getAssociatedBindings <em>Associated Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Associated Bindings</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getAssociatedBindings()
	 * @see #getYSuspect()
	 * @generated
	 */
	EReference getYSuspect_AssociatedBindings();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getLabel()
	 * @see #getYSuspect()
	 * @generated
	 */
	EAttribute getYSuspect_Label();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getContainerValueBindingEndpoint <em>Container Value Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Container Value Binding Endpoint</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getContainerValueBindingEndpoint()
	 * @see #getYSuspect()
	 * @generated
	 */
	EReference getYSuspect_ContainerValueBindingEndpoint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getGroupName <em>Group Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getGroupName()
	 * @see #getYSuspect()
	 * @generated
	 */
	EAttribute getYSuspect_GroupName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getStyleName <em>Style Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Style Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspect#getStyleName()
	 * @see #getYSuspect()
	 * @generated
	 */
	EAttribute getYSuspect_StyleName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo <em>YLayouting Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YLayouting Info</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YLayoutingInfo
	 * @generated
	 */
	EClass getYLayoutingInfo();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getLayout <em>Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Layout</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getLayout()
	 * @see #getYLayoutingInfo()
	 * @generated
	 */
	EReference getYLayoutingInfo_Layout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getContent()
	 * @see #getYLayoutingInfo()
	 * @generated
	 */
	EReference getYLayoutingInfo_Content();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getActiveSuspectInfos <em>Active Suspect Infos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Suspect Infos</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getActiveSuspectInfos()
	 * @see #getYLayoutingInfo()
	 * @generated
	 */
	EReference getYLayoutingInfo_ActiveSuspectInfos();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getFirstFocus <em>First Focus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First Focus</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getFirstFocus()
	 * @see #getYLayoutingInfo()
	 * @generated
	 */
	EReference getYLayoutingInfo_FirstFocus();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo <em>YSuspect Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YSuspect Info</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo
	 * @generated
	 */
	EClass getYSuspectInfo();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getSuspect <em>Suspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Suspect</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getSuspect()
	 * @see #getYSuspectInfo()
	 * @generated
	 */
	EReference getYSuspectInfo_Suspect();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bindings</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getBindings()
	 * @see #getYSuspectInfo()
	 * @generated
	 */
	EReference getYSuspectInfo_Bindings();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getNextFocus <em>Next Focus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next Focus</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getNextFocus()
	 * @see #getYSuspectInfo()
	 * @generated
	 */
	EReference getYSuspectInfo_NextFocus();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getPreviousFocus <em>Previous Focus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous Focus</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getPreviousFocus()
	 * @see #getYSuspectInfo()
	 * @generated
	 */
	EReference getYSuspectInfo_PreviousFocus();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getTarget()
	 * @see #getYSuspectInfo()
	 * @generated
	 */
	EReference getYSuspectInfo_Target();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getVisibilityProcessors <em>Visibility Processors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Visibility Processors</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getVisibilityProcessors()
	 * @see #getYSuspectInfo()
	 * @generated
	 */
	EReference getYSuspectInfo_VisibilityProcessors();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent <em>YBlob Upload Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YBlob Upload Component</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent
	 * @generated
	 */
	EClass getYBlobUploadComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getValue()
	 * @see #getYBlobUploadComponent()
	 * @generated
	 */
	EAttribute getYBlobUploadComponent_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getDisplayResolutionId <em>Display Resolution Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Resolution Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#getDisplayResolutionId()
	 * @see #getYBlobUploadComponent()
	 * @generated
	 */
	EAttribute getYBlobUploadComponent_DisplayResolutionId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isFirmlyLinked <em>Firmly Linked</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Firmly Linked</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isFirmlyLinked()
	 * @see #getYBlobUploadComponent()
	 * @generated
	 */
	EAttribute getYBlobUploadComponent_FirmlyLinked();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isUniqueNameEnabled <em>Unique Name Enabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique Name Enabled</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent#isUniqueNameEnabled()
	 * @see #getYBlobUploadComponent()
	 * @generated
	 */
	EAttribute getYBlobUploadComponent_UniqueNameEnabled();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YTypedSuspect <em>YTyped Suspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YTyped Suspect</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YTypedSuspect
	 * @generated
	 */
	EClass getYTypedSuspect();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YTypedSuspect#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YTypedSuspect#getTypeQualifiedName()
	 * @see #getYTypedSuspect()
	 * @generated
	 */
	EAttribute getYTypedSuspect_TypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YTypedSuspect#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YTypedSuspect#getType()
	 * @see #getYTypedSuspect()
	 * @generated
	 */
	EAttribute getYTypedSuspect_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YTypedCompoundSuspect <em>YTyped Compound Suspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YTyped Compound Suspect</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YTypedCompoundSuspect
	 * @generated
	 */
	EClass getYTypedCompoundSuspect();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YTypedCompoundSuspect#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YTypedCompoundSuspect#getChildren()
	 * @see #getYTypedCompoundSuspect()
	 * @generated
	 */
	EReference getYTypedCompoundSuspect_Children();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YSubTypeBaseSuspect <em>YSub Type Base Suspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YSub Type Base Suspect</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSubTypeBaseSuspect
	 * @generated
	 */
	EClass getYSubTypeBaseSuspect();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect <em>YSub Type Suspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YSub Type Suspect</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect
	 * @generated
	 */
	EClass getYSubTypeSuspect();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect#getBeanSlot <em>Bean Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bean Slot</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect#getBeanSlot()
	 * @see #getYSubTypeSuspect()
	 * @generated
	 */
	EReference getYSubTypeSuspect_BeanSlot();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YCustomDecimalField <em>YCustom Decimal Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YCustom Decimal Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YCustomDecimalField
	 * @generated
	 */
	EClass getYCustomDecimalField();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YCustomDecimalField#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datatype</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YCustomDecimalField#getDatatype()
	 * @see #getYCustomDecimalField()
	 * @generated
	 */
	EReference getYCustomDecimalField_Datatype();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YCustomDecimalField#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YCustomDecimalField#getDatadescription()
	 * @see #getYCustomDecimalField()
	 * @generated
	 */
	EReference getYCustomDecimalField_Datadescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YCustomDecimalField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YCustomDecimalField#getValue()
	 * @see #getYCustomDecimalField()
	 * @generated
	 */
	EAttribute getYCustomDecimalField_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox <em>YI1 8n Combo Box</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YI1 8n Combo Box</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YI18nComboBox
	 * @generated
	 */
	EClass getYI18nComboBox();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getDatadescription()
	 * @see #getYI18nComboBox()
	 * @generated
	 */
	EReference getYI18nComboBox_Datadescription();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datatype</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getDatatype()
	 * @see #getYI18nComboBox()
	 * @generated
	 */
	EReference getYI18nComboBox_Datatype();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getSelection <em>Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getSelection()
	 * @see #getYI18nComboBox()
	 * @generated
	 */
	EAttribute getYI18nComboBox_Selection();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getType()
	 * @see #getYI18nComboBox()
	 * @generated
	 */
	EAttribute getYI18nComboBox_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getEmfNsURI <em>Emf Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Emf Ns URI</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getEmfNsURI()
	 * @see #getYI18nComboBox()
	 * @generated
	 */
	EAttribute getYI18nComboBox_EmfNsURI();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YI18nComboBox#getTypeQualifiedName()
	 * @see #getYI18nComboBox()
	 * @generated
	 */
	EAttribute getYI18nComboBox_TypeQualifiedName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox <em>YIcon Combo Box</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YIcon Combo Box</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox
	 * @generated
	 */
	EClass getYIconComboBox();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatadescription()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EReference getYIconComboBox_Datadescription();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datatype</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatatype()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EReference getYIconComboBox_Datatype();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getSelection <em>Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getSelection()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_Selection();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getType()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getEmfNsURI <em>Emf Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Emf Ns URI</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getEmfNsURI()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_EmfNsURI();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getTypeQualifiedName()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_TypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getCaptionProperty <em>Caption Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Caption Property</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getCaptionProperty()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_CaptionProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getImageProperty <em>Image Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image Property</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getImageProperty()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_ImageProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescriptionProperty <em>Description Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Property</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescriptionProperty()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_DescriptionProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescription()
	 * @see #getYIconComboBox()
	 * @generated
	 */
	EAttribute getYIconComboBox_Description();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YQuantityTextField <em>YQuantity Text Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YQuantity Text Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YQuantityTextField
	 * @generated
	 */
	EClass getYQuantityTextField();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YQuantityTextField#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YQuantityTextField#getDatadescription()
	 * @see #getYQuantityTextField()
	 * @generated
	 */
	EReference getYQuantityTextField_Datadescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YQuantityTextField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YQuantityTextField#getValue()
	 * @see #getYQuantityTextField()
	 * @generated
	 */
	EAttribute getYQuantityTextField_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YCollectionSuspect <em>YCollection Suspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YCollection Suspect</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YCollectionSuspect
	 * @generated
	 */
	EClass getYCollectionSuspect();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.YCollectionSuspect#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Columns</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YCollectionSuspect#getColumns()
	 * @see #getYCollectionSuspect()
	 * @generated
	 */
	EReference getYCollectionSuspect_Columns();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YColumnInfo <em>YColumn Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YColumn Info</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YColumnInfo
	 * @generated
	 */
	EClass getYColumnInfo();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YColumnInfo#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YColumnInfo#getName()
	 * @see #getYColumnInfo()
	 * @generated
	 */
	EAttribute getYColumnInfo_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YColumnInfo#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YColumnInfo#getType()
	 * @see #getYColumnInfo()
	 * @generated
	 */
	EAttribute getYColumnInfo_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YColumnInfo#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YColumnInfo#getTypeQualifiedName()
	 * @see #getYColumnInfo()
	 * @generated
	 */
	EAttribute getYColumnInfo_TypeQualifiedName();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.osbp.ecview.extension.model.YColumnInfo#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Properties</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YColumnInfo#getProperties()
	 * @see #getYColumnInfo()
	 * @generated
	 */
	EReference getYColumnInfo_Properties();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YColumnInfo#getLabelI18nKey <em>Label I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label I1 8n Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YColumnInfo#getLabelI18nKey()
	 * @see #getYColumnInfo()
	 * @generated
	 */
	EAttribute getYColumnInfo_LabelI18nKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YColumnInfo#getSourceType <em>Source Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YColumnInfo#getSourceType()
	 * @see #getYColumnInfo()
	 * @generated
	 */
	EAttribute getYColumnInfo_SourceType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout <em>YContent Sensitive Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YContent Sensitive Layout</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout
	 * @generated
	 */
	EClass getYContentSensitiveLayout();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YRichTextArea <em>YRich Text Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YRich Text Area</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YRichTextArea
	 * @generated
	 */
	EClass getYRichTextArea();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YRichTextArea#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YRichTextArea#getDatadescription()
	 * @see #getYRichTextArea()
	 * @generated
	 */
	EReference getYRichTextArea_Datadescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YRichTextArea#getBlobValue <em>Blob Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Blob Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YRichTextArea#getBlobValue()
	 * @see #getYRichTextArea()
	 * @generated
	 */
	EAttribute getYRichTextArea_BlobValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YRichTextArea#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YRichTextArea#getValue()
	 * @see #getYRichTextArea()
	 * @generated
	 */
	EAttribute getYRichTextArea_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YRichTextArea#isUseBlob <em>Use Blob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Blob</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YRichTextArea#isUseBlob()
	 * @see #getYRichTextArea()
	 * @generated
	 */
	EAttribute getYRichTextArea_UseBlob();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YMaskedTextField <em>YMasked Text Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YMasked Text Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedTextField
	 * @generated
	 */
	EClass getYMaskedTextField();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YMaskedTextField#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedTextField#getDatadescription()
	 * @see #getYMaskedTextField()
	 * @generated
	 */
	EReference getYMaskedTextField_Datadescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedTextField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedTextField#getValue()
	 * @see #getYMaskedTextField()
	 * @generated
	 */
	EAttribute getYMaskedTextField_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedTextField#getMask <em>Mask</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mask</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedTextField#getMask()
	 * @see #getYMaskedTextField()
	 * @generated
	 */
	EAttribute getYMaskedTextField_Mask();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField <em>YPrefixed Masked Text Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YPrefixed Masked Text Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField
	 * @generated
	 */
	EClass getYPrefixedMaskedTextField();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getDatadescription()
	 * @see #getYPrefixedMaskedTextField()
	 * @generated
	 */
	EReference getYPrefixedMaskedTextField_Datadescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getValue()
	 * @see #getYPrefixedMaskedTextField()
	 * @generated
	 */
	EAttribute getYPrefixedMaskedTextField_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getMask <em>Mask</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mask</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getMask()
	 * @see #getYPrefixedMaskedTextField()
	 * @generated
	 */
	EAttribute getYPrefixedMaskedTextField_Mask();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getPrefixes <em>Prefixes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Prefixes</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField#getPrefixes()
	 * @see #getYPrefixedMaskedTextField()
	 * @generated
	 */
	EReference getYPrefixedMaskedTextField_Prefixes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YMaskedNumericField <em>YMasked Numeric Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YMasked Numeric Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedNumericField
	 * @generated
	 */
	EClass getYMaskedNumericField();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YMaskedNumericField#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedNumericField#getDatadescription()
	 * @see #getYMaskedNumericField()
	 * @generated
	 */
	EReference getYMaskedNumericField_Datadescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedNumericField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedNumericField#getValue()
	 * @see #getYMaskedNumericField()
	 * @generated
	 */
	EAttribute getYMaskedNumericField_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedNumericField#getMask <em>Mask</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mask</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedNumericField#getMask()
	 * @see #getYMaskedNumericField()
	 * @generated
	 */
	EAttribute getYMaskedNumericField_Mask();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField <em>YMasked Decimal Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YMasked Decimal Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField
	 * @generated
	 */
	EClass getYMaskedDecimalField();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDatadescription()
	 * @see #getYMaskedDecimalField()
	 * @generated
	 */
	EReference getYMaskedDecimalField_Datadescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getValue()
	 * @see #getYMaskedDecimalField()
	 * @generated
	 */
	EAttribute getYMaskedDecimalField_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getMask <em>Mask</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mask</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getMask()
	 * @see #getYMaskedDecimalField()
	 * @generated
	 */
	EAttribute getYMaskedDecimalField_Mask();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDecimalSeparator <em>Decimal Separator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Decimal Separator</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDecimalSeparator()
	 * @see #getYMaskedDecimalField()
	 * @generated
	 */
	EAttribute getYMaskedDecimalField_DecimalSeparator();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getGroupingSeparator <em>Grouping Separator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grouping Separator</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getGroupingSeparator()
	 * @see #getYMaskedDecimalField()
	 * @generated
	 */
	EAttribute getYMaskedDecimalField_GroupingSeparator();

	/**
	 * Returns the meta object for data type '{@link java.lang.Number <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Number</em>'.
	 * @see java.lang.Number
	 * @model instanceClass="java.lang.Number"
	 * @generated
	 */
	EDataType getNumber();
	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox <em>YPair Combo Box</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YPair Combo Box</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox
	 * @generated
	 */
	EClass getYPairComboBox();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDatadescription <em>Datadescription</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datadescription</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDatadescription()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EReference getYPairComboBox_Datadescription();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDatatype <em>Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datatype</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDatatype()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EReference getYPairComboBox_Datatype();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getSelection <em>Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getSelection()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_Selection();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Collection</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getCollection()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_Collection();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getType()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getEmfNsURI <em>Emf Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Emf Ns URI</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getEmfNsURI()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_EmfNsURI();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getTypeQualifiedName()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_TypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getCaptionProperty <em>Caption Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Caption Property</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getCaptionProperty()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_CaptionProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getImageProperty <em>Image Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image Property</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getImageProperty()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_ImageProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDescriptionProperty <em>Description Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Property</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDescriptionProperty()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_DescriptionProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.YPairComboBox#getDescription()
	 * @see #getYPairComboBox()
	 * @generated
	 */
	EAttribute getYPairComboBox_Description();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	YECviewFactory getYECviewFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl <em>YStrategy Layout</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYStrategyLayout()
		 * @generated
		 */
		EClass YSTRATEGY_LAYOUT = eINSTANCE.getYStrategyLayout();

		/**
		 * The meta object literal for the '<em><b>Layouting Strategy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY = eINSTANCE.getYStrategyLayout_LayoutingStrategy();

		/**
		 * The meta object literal for the '<em><b>Focusing Strategies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES = eINSTANCE.getYStrategyLayout_FocusingStrategies();

		/**
		 * The meta object literal for the '<em><b>Suspects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSTRATEGY_LAYOUT__SUSPECTS = eINSTANCE.getYStrategyLayout_Suspects();

		/**
		 * The meta object literal for the '<em><b>Layouting Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSTRATEGY_LAYOUT__LAYOUTING_INFO = eINSTANCE.getYStrategyLayout_LayoutingInfo();

		/**
		 * The meta object literal for the '<em><b>Default Focusing Enhancer Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID = eINSTANCE.getYStrategyLayout_DefaultFocusingEnhancerId();

		/**
		 * The meta object literal for the '<em><b>Number Columns</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSTRATEGY_LAYOUT__NUMBER_COLUMNS = eINSTANCE.getYStrategyLayout_NumberColumns();

		/**
		 * The meta object literal for the '<em><b>Save And New</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSTRATEGY_LAYOUT__SAVE_AND_NEW = eINSTANCE.getYStrategyLayout_SaveAndNew();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YLayoutingStrategyImpl <em>YLayouting Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YLayoutingStrategyImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYLayoutingStrategy()
		 * @generated
		 */
		EClass YLAYOUTING_STRATEGY = eINSTANCE.getYLayoutingStrategy();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YLAYOUTING_STRATEGY__TRIGGER = eINSTANCE.getYLayoutingStrategy_Trigger();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YDefaultLayoutingStrategyImpl <em>YDefault Layouting Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YDefaultLayoutingStrategyImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYDefaultLayoutingStrategy()
		 * @generated
		 */
		EClass YDEFAULT_LAYOUTING_STRATEGY = eINSTANCE.getYDefaultLayoutingStrategy();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl <em>YFocusing Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYFocusingStrategy()
		 * @generated
		 */
		EClass YFOCUSING_STRATEGY = eINSTANCE.getYFocusingStrategy();

		/**
		 * The meta object literal for the '<em><b>Key Stroke Definition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION = eINSTANCE.getYFocusingStrategy_KeyStrokeDefinition();

		/**
		 * The meta object literal for the '<em><b>Temp Stroke Definition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION = eINSTANCE.getYFocusingStrategy_TempStrokeDefinition();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YDelegatingLayoutingStrategyImpl <em>YDelegating Layouting Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YDelegatingLayoutingStrategyImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYDelegatingLayoutingStrategy()
		 * @generated
		 */
		EClass YDELEGATING_LAYOUTING_STRATEGY = eINSTANCE.getYDelegatingLayoutingStrategy();

		/**
		 * The meta object literal for the '<em><b>Delegate Strategy Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID = eINSTANCE.getYDelegatingLayoutingStrategy_DelegateStrategyId();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YDelegatingFocusingStrategyImpl <em>YDelegating Focusing Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YDelegatingFocusingStrategyImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYDelegatingFocusingStrategy()
		 * @generated
		 */
		EClass YDELEGATING_FOCUSING_STRATEGY = eINSTANCE.getYDelegatingFocusingStrategy();

		/**
		 * The meta object literal for the '<em><b>Delegate Strategy Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YDELEGATING_FOCUSING_STRATEGY__DELEGATE_STRATEGY_ID = eINSTANCE.getYDelegatingFocusingStrategy_DelegateStrategyId();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectImpl <em>YSuspect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YSuspectImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSuspect()
		 * @generated
		 */
		EClass YSUSPECT = eINSTANCE.getYSuspect();

		/**
		 * The meta object literal for the '<em><b>Label I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSUSPECT__LABEL_I1_8N_KEY = eINSTANCE.getYSuspect_LabelI18nKey();

		/**
		 * The meta object literal for the '<em><b>Image I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSUSPECT__IMAGE_I1_8N_KEY = eINSTANCE.getYSuspect_ImageI18nKey();

		/**
		 * The meta object literal for the '<em><b>Value Binding Endpoints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT__VALUE_BINDING_ENDPOINTS = eINSTANCE.getYSuspect_ValueBindingEndpoints();

		/**
		 * The meta object literal for the '<em><b>Visibility Processors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT__VISIBILITY_PROCESSORS = eINSTANCE.getYSuspect_VisibilityProcessors();

		/**
		 * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT__COMMANDS = eINSTANCE.getYSuspect_Commands();

		/**
		 * The meta object literal for the '<em><b>Assoc Newiated Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT__ASSOC_NEWIATED_ELEMENTS = eINSTANCE.getYSuspect_AssocNewiatedElements();

		/**
		 * The meta object literal for the '<em><b>Associated Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT__ASSOCIATED_BINDINGS = eINSTANCE.getYSuspect_AssociatedBindings();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSUSPECT__LABEL = eINSTANCE.getYSuspect_Label();

		/**
		 * The meta object literal for the '<em><b>Container Value Binding Endpoint</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT__CONTAINER_VALUE_BINDING_ENDPOINT = eINSTANCE.getYSuspect_ContainerValueBindingEndpoint();

		/**
		 * The meta object literal for the '<em><b>Group Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSUSPECT__GROUP_NAME = eINSTANCE.getYSuspect_GroupName();

		/**
		 * The meta object literal for the '<em><b>Style Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSUSPECT__STYLE_NAME = eINSTANCE.getYSuspect_StyleName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YLayoutingInfoImpl <em>YLayouting Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YLayoutingInfoImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYLayoutingInfo()
		 * @generated
		 */
		EClass YLAYOUTING_INFO = eINSTANCE.getYLayoutingInfo();

		/**
		 * The meta object literal for the '<em><b>Layout</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YLAYOUTING_INFO__LAYOUT = eINSTANCE.getYLayoutingInfo_Layout();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YLAYOUTING_INFO__CONTENT = eINSTANCE.getYLayoutingInfo_Content();

		/**
		 * The meta object literal for the '<em><b>Active Suspect Infos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS = eINSTANCE.getYLayoutingInfo_ActiveSuspectInfos();

		/**
		 * The meta object literal for the '<em><b>First Focus</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YLAYOUTING_INFO__FIRST_FOCUS = eINSTANCE.getYLayoutingInfo_FirstFocus();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl <em>YSuspect Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSuspectInfo()
		 * @generated
		 */
		EClass YSUSPECT_INFO = eINSTANCE.getYSuspectInfo();

		/**
		 * The meta object literal for the '<em><b>Suspect</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT_INFO__SUSPECT = eINSTANCE.getYSuspectInfo_Suspect();

		/**
		 * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT_INFO__BINDINGS = eINSTANCE.getYSuspectInfo_Bindings();

		/**
		 * The meta object literal for the '<em><b>Next Focus</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT_INFO__NEXT_FOCUS = eINSTANCE.getYSuspectInfo_NextFocus();

		/**
		 * The meta object literal for the '<em><b>Previous Focus</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT_INFO__PREVIOUS_FOCUS = eINSTANCE.getYSuspectInfo_PreviousFocus();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT_INFO__TARGET = eINSTANCE.getYSuspectInfo_Target();

		/**
		 * The meta object literal for the '<em><b>Visibility Processors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUSPECT_INFO__VISIBILITY_PROCESSORS = eINSTANCE.getYSuspectInfo_VisibilityProcessors();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YBlobUploadComponentImpl <em>YBlob Upload Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YBlobUploadComponentImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYBlobUploadComponent()
		 * @generated
		 */
		EClass YBLOB_UPLOAD_COMPONENT = eINSTANCE.getYBlobUploadComponent();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YBLOB_UPLOAD_COMPONENT__VALUE = eINSTANCE.getYBlobUploadComponent_Value();

		/**
		 * The meta object literal for the '<em><b>Display Resolution Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YBLOB_UPLOAD_COMPONENT__DISPLAY_RESOLUTION_ID = eINSTANCE.getYBlobUploadComponent_DisplayResolutionId();

		/**
		 * The meta object literal for the '<em><b>Firmly Linked</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YBLOB_UPLOAD_COMPONENT__FIRMLY_LINKED = eINSTANCE.getYBlobUploadComponent_FirmlyLinked();

		/**
		 * The meta object literal for the '<em><b>Unique Name Enabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YBLOB_UPLOAD_COMPONENT__UNIQUE_NAME_ENABLED = eINSTANCE.getYBlobUploadComponent_UniqueNameEnabled();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YTypedSuspectImpl <em>YTyped Suspect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YTypedSuspectImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYTypedSuspect()
		 * @generated
		 */
		EClass YTYPED_SUSPECT = eINSTANCE.getYTypedSuspect();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YTYPED_SUSPECT__TYPE_QUALIFIED_NAME = eINSTANCE.getYTypedSuspect_TypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YTYPED_SUSPECT__TYPE = eINSTANCE.getYTypedSuspect_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YTypedCompoundSuspectImpl <em>YTyped Compound Suspect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YTypedCompoundSuspectImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYTypedCompoundSuspect()
		 * @generated
		 */
		EClass YTYPED_COMPOUND_SUSPECT = eINSTANCE.getYTypedCompoundSuspect();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YTYPED_COMPOUND_SUSPECT__CHILDREN = eINSTANCE.getYTypedCompoundSuspect_Children();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSubTypeBaseSuspectImpl <em>YSub Type Base Suspect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YSubTypeBaseSuspectImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSubTypeBaseSuspect()
		 * @generated
		 */
		EClass YSUB_TYPE_BASE_SUSPECT = eINSTANCE.getYSubTypeBaseSuspect();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YSubTypeSuspectImpl <em>YSub Type Suspect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YSubTypeSuspectImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYSubTypeSuspect()
		 * @generated
		 */
		EClass YSUB_TYPE_SUSPECT = eINSTANCE.getYSubTypeSuspect();

		/**
		 * The meta object literal for the '<em><b>Bean Slot</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUB_TYPE_SUSPECT__BEAN_SLOT = eINSTANCE.getYSubTypeSuspect_BeanSlot();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YCustomDecimalFieldImpl <em>YCustom Decimal Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YCustomDecimalFieldImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYCustomDecimalField()
		 * @generated
		 */
		EClass YCUSTOM_DECIMAL_FIELD = eINSTANCE.getYCustomDecimalField();

		/**
		 * The meta object literal for the '<em><b>Datatype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YCUSTOM_DECIMAL_FIELD__DATATYPE = eINSTANCE.getYCustomDecimalField_Datatype();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YCUSTOM_DECIMAL_FIELD__DATADESCRIPTION = eINSTANCE.getYCustomDecimalField_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YCUSTOM_DECIMAL_FIELD__VALUE = eINSTANCE.getYCustomDecimalField_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YI18nComboBoxImpl <em>YI1 8n Combo Box</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YI18nComboBoxImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYI18nComboBox()
		 * @generated
		 */
		EClass YI1_8N_COMBO_BOX = eINSTANCE.getYI18nComboBox();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YI1_8N_COMBO_BOX__DATADESCRIPTION = eINSTANCE.getYI18nComboBox_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Datatype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YI1_8N_COMBO_BOX__DATATYPE = eINSTANCE.getYI18nComboBox_Datatype();

		/**
		 * The meta object literal for the '<em><b>Selection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YI1_8N_COMBO_BOX__SELECTION = eINSTANCE.getYI18nComboBox_Selection();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YI1_8N_COMBO_BOX__TYPE = eINSTANCE.getYI18nComboBox_Type();

		/**
		 * The meta object literal for the '<em><b>Emf Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YI1_8N_COMBO_BOX__EMF_NS_URI = eINSTANCE.getYI18nComboBox_EmfNsURI();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YI1_8N_COMBO_BOX__TYPE_QUALIFIED_NAME = eINSTANCE.getYI18nComboBox_TypeQualifiedName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YIconComboBoxImpl <em>YIcon Combo Box</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YIconComboBoxImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYIconComboBox()
		 * @generated
		 */
		EClass YICON_COMBO_BOX = eINSTANCE.getYIconComboBox();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YICON_COMBO_BOX__DATADESCRIPTION = eINSTANCE.getYIconComboBox_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Datatype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YICON_COMBO_BOX__DATATYPE = eINSTANCE.getYIconComboBox_Datatype();

		/**
		 * The meta object literal for the '<em><b>Selection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__SELECTION = eINSTANCE.getYIconComboBox_Selection();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__TYPE = eINSTANCE.getYIconComboBox_Type();

		/**
		 * The meta object literal for the '<em><b>Emf Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__EMF_NS_URI = eINSTANCE.getYIconComboBox_EmfNsURI();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__TYPE_QUALIFIED_NAME = eINSTANCE.getYIconComboBox_TypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Caption Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__CAPTION_PROPERTY = eINSTANCE.getYIconComboBox_CaptionProperty();

		/**
		 * The meta object literal for the '<em><b>Image Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__IMAGE_PROPERTY = eINSTANCE.getYIconComboBox_ImageProperty();

		/**
		 * The meta object literal for the '<em><b>Description Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__DESCRIPTION_PROPERTY = eINSTANCE.getYIconComboBox_DescriptionProperty();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YICON_COMBO_BOX__DESCRIPTION = eINSTANCE.getYIconComboBox_Description();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YQuantityTextFieldImpl <em>YQuantity Text Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YQuantityTextFieldImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYQuantityTextField()
		 * @generated
		 */
		EClass YQUANTITY_TEXT_FIELD = eINSTANCE.getYQuantityTextField();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YQUANTITY_TEXT_FIELD__DATADESCRIPTION = eINSTANCE.getYQuantityTextField_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YQUANTITY_TEXT_FIELD__VALUE = eINSTANCE.getYQuantityTextField_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YCollectionSuspectImpl <em>YCollection Suspect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YCollectionSuspectImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYCollectionSuspect()
		 * @generated
		 */
		EClass YCOLLECTION_SUSPECT = eINSTANCE.getYCollectionSuspect();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YCOLLECTION_SUSPECT__COLUMNS = eINSTANCE.getYCollectionSuspect_Columns();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YColumnInfoImpl <em>YColumn Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YColumnInfoImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYColumnInfo()
		 * @generated
		 */
		EClass YCOLUMN_INFO = eINSTANCE.getYColumnInfo();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YCOLUMN_INFO__NAME = eINSTANCE.getYColumnInfo_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YCOLUMN_INFO__TYPE = eINSTANCE.getYColumnInfo_Type();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YCOLUMN_INFO__TYPE_QUALIFIED_NAME = eINSTANCE.getYColumnInfo_TypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YCOLUMN_INFO__PROPERTIES = eINSTANCE.getYColumnInfo_Properties();

		/**
		 * The meta object literal for the '<em><b>Label I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YCOLUMN_INFO__LABEL_I1_8N_KEY = eINSTANCE.getYColumnInfo_LabelI18nKey();

		/**
		 * The meta object literal for the '<em><b>Source Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YCOLUMN_INFO__SOURCE_TYPE = eINSTANCE.getYColumnInfo_SourceType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YContentSensitiveLayoutImpl <em>YContent Sensitive Layout</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YContentSensitiveLayoutImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYContentSensitiveLayout()
		 * @generated
		 */
		EClass YCONTENT_SENSITIVE_LAYOUT = eINSTANCE.getYContentSensitiveLayout();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YRichTextAreaImpl <em>YRich Text Area</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YRichTextAreaImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYRichTextArea()
		 * @generated
		 */
		EClass YRICH_TEXT_AREA = eINSTANCE.getYRichTextArea();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YRICH_TEXT_AREA__DATADESCRIPTION = eINSTANCE.getYRichTextArea_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Blob Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YRICH_TEXT_AREA__BLOB_VALUE = eINSTANCE.getYRichTextArea_BlobValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YRICH_TEXT_AREA__VALUE = eINSTANCE.getYRichTextArea_Value();

		/**
		 * The meta object literal for the '<em><b>Use Blob</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YRICH_TEXT_AREA__USE_BLOB = eINSTANCE.getYRichTextArea_UseBlob();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YMaskedTextFieldImpl <em>YMasked Text Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YMaskedTextFieldImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYMaskedTextField()
		 * @generated
		 */
		EClass YMASKED_TEXT_FIELD = eINSTANCE.getYMaskedTextField();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YMASKED_TEXT_FIELD__DATADESCRIPTION = eINSTANCE.getYMaskedTextField_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_TEXT_FIELD__VALUE = eINSTANCE.getYMaskedTextField_Value();

		/**
		 * The meta object literal for the '<em><b>Mask</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_TEXT_FIELD__MASK = eINSTANCE.getYMaskedTextField_Mask();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YPrefixedMaskedTextFieldImpl <em>YPrefixed Masked Text Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YPrefixedMaskedTextFieldImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYPrefixedMaskedTextField()
		 * @generated
		 */
		EClass YPREFIXED_MASKED_TEXT_FIELD = eINSTANCE.getYPrefixedMaskedTextField();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YPREFIXED_MASKED_TEXT_FIELD__DATADESCRIPTION = eINSTANCE.getYPrefixedMaskedTextField_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPREFIXED_MASKED_TEXT_FIELD__VALUE = eINSTANCE.getYPrefixedMaskedTextField_Value();

		/**
		 * The meta object literal for the '<em><b>Mask</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPREFIXED_MASKED_TEXT_FIELD__MASK = eINSTANCE.getYPrefixedMaskedTextField_Mask();

		/**
		 * The meta object literal for the '<em><b>Prefixes</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YPREFIXED_MASKED_TEXT_FIELD__PREFIXES = eINSTANCE.getYPrefixedMaskedTextField_Prefixes();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YMaskedNumericFieldImpl <em>YMasked Numeric Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YMaskedNumericFieldImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYMaskedNumericField()
		 * @generated
		 */
		EClass YMASKED_NUMERIC_FIELD = eINSTANCE.getYMaskedNumericField();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YMASKED_NUMERIC_FIELD__DATADESCRIPTION = eINSTANCE.getYMaskedNumericField_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_NUMERIC_FIELD__VALUE = eINSTANCE.getYMaskedNumericField_Value();

		/**
		 * The meta object literal for the '<em><b>Mask</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_NUMERIC_FIELD__MASK = eINSTANCE.getYMaskedNumericField_Mask();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YMaskedDecimalFieldImpl <em>YMasked Decimal Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YMaskedDecimalFieldImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYMaskedDecimalField()
		 * @generated
		 */
		EClass YMASKED_DECIMAL_FIELD = eINSTANCE.getYMaskedDecimalField();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YMASKED_DECIMAL_FIELD__DATADESCRIPTION = eINSTANCE.getYMaskedDecimalField_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_DECIMAL_FIELD__VALUE = eINSTANCE.getYMaskedDecimalField_Value();

		/**
		 * The meta object literal for the '<em><b>Mask</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_DECIMAL_FIELD__MASK = eINSTANCE.getYMaskedDecimalField_Mask();

		/**
		 * The meta object literal for the '<em><b>Decimal Separator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_DECIMAL_FIELD__DECIMAL_SEPARATOR = eINSTANCE.getYMaskedDecimalField_DecimalSeparator();

		/**
		 * The meta object literal for the '<em><b>Grouping Separator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YMASKED_DECIMAL_FIELD__GROUPING_SEPARATOR = eINSTANCE.getYMaskedDecimalField_GroupingSeparator();

		/**
		 * The meta object literal for the '<em>Number</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Number
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getNumber()
		 * @generated
		 */
		EDataType NUMBER = eINSTANCE.getNumber();
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.impl.YPairComboBoxImpl <em>YPair Combo Box</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YPairComboBoxImpl
		 * @see org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl#getYPairComboBox()
		 * @generated
		 */
		EClass YPAIR_COMBO_BOX = eINSTANCE.getYPairComboBox();

		/**
		 * The meta object literal for the '<em><b>Datadescription</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YPAIR_COMBO_BOX__DATADESCRIPTION = eINSTANCE.getYPairComboBox_Datadescription();

		/**
		 * The meta object literal for the '<em><b>Datatype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YPAIR_COMBO_BOX__DATATYPE = eINSTANCE.getYPairComboBox_Datatype();

		/**
		 * The meta object literal for the '<em><b>Selection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__SELECTION = eINSTANCE.getYPairComboBox_Selection();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__COLLECTION = eINSTANCE.getYPairComboBox_Collection();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__TYPE = eINSTANCE.getYPairComboBox_Type();

		/**
		 * The meta object literal for the '<em><b>Emf Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__EMF_NS_URI = eINSTANCE.getYPairComboBox_EmfNsURI();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__TYPE_QUALIFIED_NAME = eINSTANCE.getYPairComboBox_TypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Caption Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__CAPTION_PROPERTY = eINSTANCE.getYPairComboBox_CaptionProperty();

		/**
		 * The meta object literal for the '<em><b>Image Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__IMAGE_PROPERTY = eINSTANCE.getYPairComboBox_ImageProperty();

		/**
		 * The meta object literal for the '<em><b>Description Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__DESCRIPTION_PROPERTY = eINSTANCE.getYPairComboBox_DescriptionProperty();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPAIR_COMBO_BOX__DESCRIPTION = eINSTANCE.getYPairComboBox_Description();

	}

}

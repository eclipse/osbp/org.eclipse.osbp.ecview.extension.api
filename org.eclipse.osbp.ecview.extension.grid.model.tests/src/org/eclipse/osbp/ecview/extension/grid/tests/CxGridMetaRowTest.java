/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Meta Row</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CxGridMetaRowTest extends TestCase {

	/**
	 * The fixture for this Meta Row test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridMetaRow fixture = null;

	/**
	 * Constructs a new Meta Row test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridMetaRowTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Meta Row test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridMetaRow fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Meta Row test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridMetaRow getFixture() {
		return fixture;
	}

} //CxGridMetaRowTest

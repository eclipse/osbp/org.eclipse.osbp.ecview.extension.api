/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.editparts;

import java.util.Map;

import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;

/**
 * The abstraction for a grid element.
 */
public interface IGridEditpart extends IEmbeddableEditpart {

	/**
	 * Sets the filter properties and their value. Map#key is a nested property
	 * path. Map#value is the filter value.<br>
	 * All entries will be used as Equals-Filter and will be joined by And.
	 *
	 * @param filterProps
	 *            the filter props
	 */
	void setFilter(Map<String, Object> filterProps);

}

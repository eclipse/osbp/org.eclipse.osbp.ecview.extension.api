/**
 *                                                                            
 *  Copyright (c) 2011, 2015 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.extension.model.extension.provider.YInputItemProvider;

import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YRichTextArea;

import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.model.YRichTextArea} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YRichTextAreaItemProvider extends YInputItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YRichTextAreaItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValueBindingEndpointPropertyDescriptor(object);
			addDatadescriptionPropertyDescriptor(object);
			addBlobValuePropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addUseBlobPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Value Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValueBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YValueBindable_valueBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YValueBindable_valueBindingEndpoint_feature", "_UI_YValueBindable_type"),
				 CoreModelPackage.Literals.YVALUE_BINDABLE__VALUE_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datadescription feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatadescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YRichTextArea_datadescription_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YRichTextArea_datadescription_feature", "_UI_YRichTextArea_type"),
				 YECviewPackage.Literals.YRICH_TEXT_AREA__DATADESCRIPTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Blob Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBlobValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YRichTextArea_blobValue_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YRichTextArea_blobValue_feature", "_UI_YRichTextArea_type"),
				 YECviewPackage.Literals.YRICH_TEXT_AREA__BLOB_VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YRichTextArea_value_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YRichTextArea_value_feature", "_UI_YRichTextArea_type"),
				 YECviewPackage.Literals.YRICH_TEXT_AREA__VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Blob feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseBlobPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YRichTextArea_useBlob_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YRichTextArea_useBlob_feature", "_UI_YRichTextArea_type"),
				 YECviewPackage.Literals.YRICH_TEXT_AREA__USE_BLOB,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns YRichTextArea.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YRichTextArea"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YRichTextArea)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YRichTextArea_type") :
			getString("_UI_YRichTextArea_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YRichTextArea.class)) {
			case YECviewPackage.YRICH_TEXT_AREA__BLOB_VALUE:
			case YECviewPackage.YRICH_TEXT_AREA__VALUE:
			case YECviewPackage.YRICH_TEXT_AREA__USE_BLOB:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYObjectToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYStringToByteArrayConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYCustomDecimalConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYNumericToResourceConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYStringToResourceConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYPriceToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYQuantityToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYNumericToUomoConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYDecimalToUomoConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYSimpleDecimalConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYVaaclipseUiThemeToStringConverter()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}

/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.util;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.util.SimpleCoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.YBeanValidationValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YMaxLengthValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidator;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YCheckBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YComboBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YListDataType;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YNumericDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YProgressBarDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTableDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextAreaDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTreeDatatype;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBrowser;
import org.eclipse.osbp.ecview.core.extension.model.extension.YButton;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCheckBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YColumn;
import org.eclipse.osbp.ecview.core.extension.model.extension.YComboBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCssLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCssLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDateTime;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDecimalField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YImage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YLabel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YList;
import org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YOptionsGroup;
import org.eclipse.osbp.ecview.core.extension.model.extension.YPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YProgressBar;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSlider;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSpanInfo;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTab;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTabSheet;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextArea;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTree;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayoutCellStyle;
import org.eclipse.osbp.ecview.extension.model.YCustomDecimalField;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;

/**
 * A factory for creating SimpleECviewExtensionModel objects.
 */
public class SimpleECviewExtensionModelFactory extends SimpleCoreModelFactory {

	/** The extfactory. */
	private ExtensionModelFactory extfactory = ExtensionModelFactory.eINSTANCE;
	
	/** The dt factory. */
	private ExtDatatypesFactory dtFactory = ExtDatatypesFactory.eINSTANCE;
	
	/** The ec factory. */
	private YECviewFactory ecFactory = YECviewFactory.eINSTANCE;

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y text field
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYTextField()
	 */
	public YTextField createTextField() {
		return extfactory.createYTextField();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y image
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYImage()
	 */
	public YImage createImage() {
		return extfactory.createYImage();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y progress bar
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYProgressBar()
	 */
	public YProgressBar createProgressBar() {
		return extfactory.createYProgressBar();
	}
	
	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y slider
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYSlider()
	 */
	public YSlider createSlider() {
		return extfactory.createYSlider();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y grid layout
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYGridLayout()
	 */
	public YGridLayout createGridLayout() {
		return extfactory.createYGridLayout();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y grid layout cell style
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYGridLayoutCellStyle()
	 */
	public YGridLayoutCellStyle createGridLayoutCellStyle() {
		return extfactory.createYGridLayoutCellStyle();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y css layout
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYHorizontalLayout()
	 */
	public YCssLayout createCssLayout() {
		return extfactory.createYCssLayout();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y css layout cell style
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYHorizontalLayoutCellStyle()
	 */
	public YCssLayoutCellStyle createCssLayoutCellStyle() {
		return extfactory.createYCssLayoutCellStyle();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y horizontal layout
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYHorizontalLayout()
	 */
	public YHorizontalLayout createHorizontalLayout() {
		return extfactory.createYHorizontalLayout();
	}
	
	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y horizontal layout cell style
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYHorizontalLayoutCellStyle()
	 */
	public YHorizontalLayoutCellStyle createHorizontalLayoutCellStyle() {
		return extfactory.createYHorizontalLayoutCellStyle();
	}
	
	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y vertical layout
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYVerticalLayout()
	 */
	public YVerticalLayout createVerticalLayout() {
		return extfactory.createYVerticalLayout();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y form layout
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYFormLayout()
	 */
	public YFormLayout createFormLayout() {
		return extfactory.createYFormLayout();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y vertical layout cell style
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYVerticalLayoutCellStyle()
	 */
	public YVerticalLayoutCellStyle createVerticalLayoutCellStyle() {
		return extfactory.createYVerticalLayoutCellStyle();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y span info
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYSpanInfo()
	 */
	public YSpanInfo createSpanInfo() {
		return extfactory.createYSpanInfo();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y table
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYTable()
	 */
	public YTable createTable() {
		return extfactory.createYTable();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y column
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#creatYColumn()
	 */
	public YColumn createColumn() {
		return ExtensionModelFactory.eINSTANCE.createYColumn();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y tab sheet
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYTable()
	 */
	public YTabSheet createTabSheet() {
		return extfactory.createYTabSheet();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y tab
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYTable()
	 */
	public YTab createTab() {
		return extfactory.createYTab();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y tree
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYTree()
	 */
	public YTree createTree() {
		return extfactory.createYTree();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y label
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYLabel()
	 */
	public YLabel createLabel() {
		return extfactory.createYLabel();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y text area
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYTextArea()
	 */
	public YTextArea createTextArea() {
		return extfactory.createYTextArea();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y browser
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYBrowser()
	 */
	public YBrowser createBrowser() {
		return extfactory.createYBrowser();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y date time
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYDateTime()
	 */
	public YDateTime createDateTime() {
		return extfactory.createYDateTime();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y check box
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYCheckBox()
	 */
	public YCheckBox createCheckBox() {
		return extfactory.createYCheckBox();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y decimal field
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYDecimalField()
	 */
	public YDecimalField createDecimalField() {
		return extfactory.createYDecimalField();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y custom decimal field
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYDecimalField()
	 */
	public YCustomDecimalField createCustomDecimalField() {
		return ecFactory.createYCustomDecimalField();
	}
	
	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y numeric field
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYNumericField()
	 */
	public YNumericField createNumericField() {
		return extfactory.createYNumericField();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y combo box
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYComboBox()
	 */
	public YComboBox createComboBox() {
		return extfactory.createYComboBox();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y list
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYList()
	 */
	public YList createList() {
		return extfactory.createYList();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y options group
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYOptionsGroup()
	 */
	public YOptionsGroup createOptionsGroup() {
		return extfactory.createYOptionsGroup();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y button
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory#createYButton()
	 */
	public YButton createButton() {
		return extfactory.createYButton();
	}
	
	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y split panel
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYSplitPanel()
	 */
	public YSplitPanel createSplitPanel() {
		return extfactory.createYSplitPanel();
	}
	
	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y panel
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYPanel()
	 */
	public YPanel createPanel() {
		return extfactory.createYPanel();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y text datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYTextDatatype()
	 */
	public YTextDatatype createTextDatatype() {
		return dtFactory.createYTextDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y progress bar datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYProgressBarDatatype()
	 */
	public YProgressBarDatatype createProgressBarDatatype() {
		return dtFactory.createYProgressBarDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y text area datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYTextAreaDatatype()
	 */
	public YTextAreaDatatype createTextAreaDatatype() {
		return dtFactory.createYTextAreaDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y numeric datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYNumericDatatype()
	 */
	public YNumericDatatype createNumericDatatype() {
		return dtFactory.createYNumericDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y decimal datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYDecimalDatatype()
	 */
	public YDecimalDatatype createDecimalDatatype() {
		return dtFactory.createYDecimalDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y table datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYTableDatatype()
	 */
	public YTableDatatype createTableDatatype() {
		return dtFactory.createYTableDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y tree datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYTreeDatatype()
	 */
	public YTreeDatatype createTreeDatatype() {
		return dtFactory.createYTreeDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y check box datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYCheckBoxDatatype()
	 */
	public YCheckBoxDatatype createCheckBoxDatatype() {
		return dtFactory.createYCheckBoxDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y combo box datatype
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYComboBoxDatatype()
	 */
	public YComboBoxDatatype createComboBoxDatatype() {
		return dtFactory.createYComboBoxDatatype();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y list data type
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory#createYListDataType()
	 */
	public YListDataType createListDataType() {
		return dtFactory.createYListDataType();
	}
	
	/**
	 * Creates an instance of {@link YHorizontalLayoutCellStyle}.
	 *
	 * @param yComponent
	 *            set as target
	 * @param yLayout
	 *            style added to this layout
	 * @return the y horizontal layout cell style
	 */
	public YHorizontalLayoutCellStyle createHorizontalLayoutCellStyle(
			YEmbeddable yComponent, YHorizontalLayout yLayout) {
		YHorizontalLayoutCellStyle yStyle = ExtensionModelFactory.eINSTANCE
				.createYHorizontalLayoutCellStyle();
		yStyle.setTarget(yComponent);
		yLayout.getCellStyles().add(yStyle);
		return yStyle;
	}

	/**
	 * Creates an instance of {@link YVerticalLayoutCellStyle}.
	 *
	 * @param yComponent
	 *            set as target
	 * @param yLayout
	 *            style added to this layout
	 * @return the y vertical layout cell style
	 */
	public YVerticalLayoutCellStyle createVerticalLayoutCellStyle(
			YEmbeddable yComponent, YVerticalLayout yLayout) {
		YVerticalLayoutCellStyle yStyle = ExtensionModelFactory.eINSTANCE
				.createYVerticalLayoutCellStyle();
		yStyle.setTarget(yComponent);
		yLayout.getCellStyles().add(yStyle);
		return yStyle;
	}

	/**
	 * Returns a master detail element.
	 *
	 * @return the y master detail
	 */
	public YMasterDetail createMasterDetail() {
		return ExtensionModelFactory.eINSTANCE.createYMasterDetail();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y max length validator
	 */
	public YMaxLengthValidator createMaxLengthValidator() {
		return ValidationFactory.eINSTANCE.createYMaxLengthValidator();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y min length validator
	 */
	public YMinLengthValidator createMinLengthValidator() {
		return ValidationFactory.eINSTANCE.createYMinLengthValidator();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y regexp validator
	 */
	public YRegexpValidator createRegexpValidator() {
		return ValidationFactory.eINSTANCE.createYRegexpValidator();
	}

	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y class delegate validator
	 */
	public YClassDelegateValidator createClassDelegateValidator() {
		return ValidationFactory.eINSTANCE.createYClassDelegateValidator();
	}
	
	/**
	 * Creates a new SimpleECviewExtensionModel object.
	 *
	 * @return the y bean validation validator
	 */
	public YBeanValidationValidator createBeanValidationValidator() {
		return ValidationFactory.eINSTANCE.createYBeanValidationValidator();
	}

}

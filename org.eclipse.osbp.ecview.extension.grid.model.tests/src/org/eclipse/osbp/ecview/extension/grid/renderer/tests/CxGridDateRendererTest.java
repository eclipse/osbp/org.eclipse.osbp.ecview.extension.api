/**
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cx Grid Date Renderer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridDateRendererTest extends CxGridRendererTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridDateRendererTest.class);
	}

	/**
	 * Constructs a new Cx Grid Date Renderer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridDateRendererTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Cx Grid Date Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CxGridDateRenderer getFixture() {
		return (CxGridDateRenderer)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridRendererFactory.eINSTANCE.createCxGridDateRenderer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridDateRendererTest

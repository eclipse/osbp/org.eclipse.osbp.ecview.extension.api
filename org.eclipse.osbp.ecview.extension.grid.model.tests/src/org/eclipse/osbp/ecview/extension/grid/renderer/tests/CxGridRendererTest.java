/**
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cx Grid Renderer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CxGridRendererTest extends TestCase {

	/**
	 * The fixture for this Cx Grid Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridRenderer fixture = null;

	/**
	 * Constructs a new Cx Grid Renderer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridRendererTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Cx Grid Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridRenderer fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Cx Grid Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridRenderer getFixture() {
		return fixture;
	}

} //CxGridRendererTest

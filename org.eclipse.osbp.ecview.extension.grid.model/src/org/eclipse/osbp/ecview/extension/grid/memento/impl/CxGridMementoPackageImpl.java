/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento.impl;

import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoFactory;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;
import org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridMementoPackageImpl extends EPackageImpl implements CxGridMementoPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridMementoEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridMementoColumnEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridMementoSortableEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CxGridMementoPackageImpl() {
		super(eNS_URI, CxGridMementoFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link CxGridMementoPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the cx grid memento package
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CxGridMementoPackage init() {
		if (isInited) return (CxGridMementoPackage)EPackage.Registry.INSTANCE.getEPackage(CxGridMementoPackage.eNS_URI);

		// Obtain or create and register package
		CxGridMementoPackageImpl theCxGridMementoPackage = (CxGridMementoPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CxGridMementoPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CxGridMementoPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ExtDatatypesPackage.eINSTANCE.eClass();
		ExtensionModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CxGridPackageImpl theCxGridPackage = (CxGridPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CxGridPackage.eNS_URI) instanceof CxGridPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CxGridPackage.eNS_URI) : CxGridPackage.eINSTANCE);
		CxGridRendererPackageImpl theCxGridRendererPackage = (CxGridRendererPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CxGridRendererPackage.eNS_URI) instanceof CxGridRendererPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CxGridRendererPackage.eNS_URI) : CxGridRendererPackage.eINSTANCE);

		// Create package meta-data objects
		theCxGridMementoPackage.createPackageContents();
		theCxGridPackage.createPackageContents();
		theCxGridRendererPackage.createPackageContents();

		// Initialize created meta-data
		theCxGridMementoPackage.initializePackageContents();
		theCxGridPackage.initializePackageContents();
		theCxGridRendererPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCxGridMementoPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CxGridMementoPackage.eNS_URI, theCxGridMementoPackage);
		return theCxGridMementoPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento
	 * @generated
	 */
	public EClass getCxGridMemento() {
		return cxGridMementoEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento_ grid id
	 * @generated
	 */
	public EAttribute getCxGridMemento_GridId() {
		return (EAttribute)cxGridMementoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento_ header visible
	 * @generated
	 */
	public EAttribute getCxGridMemento_HeaderVisible() {
		return (EAttribute)cxGridMementoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento_ filter visible
	 * @generated
	 */
	public EAttribute getCxGridMemento_FilterVisible() {
		return (EAttribute)cxGridMementoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento_ footer visible
	 * @generated
	 */
	public EAttribute getCxGridMemento_FooterVisible() {
		return (EAttribute)cxGridMementoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento_ editor enabled
	 * @generated
	 */
	public EAttribute getCxGridMemento_EditorEnabled() {
		return (EAttribute)cxGridMementoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento_ columns
	 * @generated
	 */
	public EReference getCxGridMemento_Columns() {
		return (EReference)cxGridMementoEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento_ sort orders
	 * @generated
	 */
	public EReference getCxGridMemento_SortOrders() {
		return (EReference)cxGridMementoEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column
	 * @generated
	 */
	public EClass getCxGridMementoColumn() {
		return cxGridMementoColumnEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column_ property id
	 * @generated
	 */
	public EAttribute getCxGridMementoColumn_PropertyId() {
		return (EAttribute)cxGridMementoColumnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column_ editable
	 * @generated
	 */
	public EAttribute getCxGridMementoColumn_Editable() {
		return (EAttribute)cxGridMementoColumnEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column_ expand ratio
	 * @generated
	 */
	public EAttribute getCxGridMementoColumn_ExpandRatio() {
		return (EAttribute)cxGridMementoColumnEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column_ hidden
	 * @generated
	 */
	public EAttribute getCxGridMementoColumn_Hidden() {
		return (EAttribute)cxGridMementoColumnEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column_ hideable
	 * @generated
	 */
	public EAttribute getCxGridMementoColumn_Hideable() {
		return (EAttribute)cxGridMementoColumnEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column_ sortable
	 * @generated
	 */
	public EAttribute getCxGridMementoColumn_Sortable() {
		return (EAttribute)cxGridMementoColumnEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column_ width
	 * @generated
	 */
	public EAttribute getCxGridMementoColumn_Width() {
		return (EAttribute)cxGridMementoColumnEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento sortable
	 * @generated
	 */
	public EClass getCxGridMementoSortable() {
		return cxGridMementoSortableEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento sortable_ descending
	 * @generated
	 */
	public EAttribute getCxGridMementoSortable_Descending() {
		return (EAttribute)cxGridMementoSortableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento sortable_ column
	 * @generated
	 */
	public EReference getCxGridMementoSortable_Column() {
		return (EReference)cxGridMementoSortableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento factory
	 * @generated
	 */
	public CxGridMementoFactory getCxGridMementoFactory() {
		return (CxGridMementoFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		cxGridMementoEClass = createEClass(CX_GRID_MEMENTO);
		createEAttribute(cxGridMementoEClass, CX_GRID_MEMENTO__GRID_ID);
		createEAttribute(cxGridMementoEClass, CX_GRID_MEMENTO__HEADER_VISIBLE);
		createEAttribute(cxGridMementoEClass, CX_GRID_MEMENTO__FILTER_VISIBLE);
		createEAttribute(cxGridMementoEClass, CX_GRID_MEMENTO__FOOTER_VISIBLE);
		createEAttribute(cxGridMementoEClass, CX_GRID_MEMENTO__EDITOR_ENABLED);
		createEReference(cxGridMementoEClass, CX_GRID_MEMENTO__COLUMNS);
		createEReference(cxGridMementoEClass, CX_GRID_MEMENTO__SORT_ORDERS);

		cxGridMementoColumnEClass = createEClass(CX_GRID_MEMENTO_COLUMN);
		createEAttribute(cxGridMementoColumnEClass, CX_GRID_MEMENTO_COLUMN__PROPERTY_ID);
		createEAttribute(cxGridMementoColumnEClass, CX_GRID_MEMENTO_COLUMN__EDITABLE);
		createEAttribute(cxGridMementoColumnEClass, CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO);
		createEAttribute(cxGridMementoColumnEClass, CX_GRID_MEMENTO_COLUMN__HIDDEN);
		createEAttribute(cxGridMementoColumnEClass, CX_GRID_MEMENTO_COLUMN__HIDEABLE);
		createEAttribute(cxGridMementoColumnEClass, CX_GRID_MEMENTO_COLUMN__SORTABLE);
		createEAttribute(cxGridMementoColumnEClass, CX_GRID_MEMENTO_COLUMN__WIDTH);

		cxGridMementoSortableEClass = createEClass(CX_GRID_MEMENTO_SORTABLE);
		createEAttribute(cxGridMementoSortableEClass, CX_GRID_MEMENTO_SORTABLE__DESCENDING);
		createEReference(cxGridMementoSortableEClass, CX_GRID_MEMENTO_SORTABLE__COLUMN);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CoreModelPackage theCoreModelPackage = (CoreModelPackage)EPackage.Registry.INSTANCE.getEPackage(CoreModelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		cxGridMementoEClass.getESuperTypes().add(theCoreModelPackage.getYMemento());

		// Initialize classes and features; add operations and parameters
		initEClass(cxGridMementoEClass, CxGridMemento.class, "CxGridMemento", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridMemento_GridId(), ecorePackage.getEString(), "gridId", null, 1, 1, CxGridMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMemento_HeaderVisible(), ecorePackage.getEBoolean(), "headerVisible", null, 0, 1, CxGridMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMemento_FilterVisible(), ecorePackage.getEBoolean(), "filterVisible", null, 0, 1, CxGridMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMemento_FooterVisible(), ecorePackage.getEBoolean(), "footerVisible", null, 0, 1, CxGridMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMemento_EditorEnabled(), ecorePackage.getEBoolean(), "editorEnabled", null, 0, 1, CxGridMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridMemento_Columns(), this.getCxGridMementoColumn(), null, "columns", null, 0, -1, CxGridMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridMemento_SortOrders(), this.getCxGridMementoSortable(), null, "sortOrders", null, 0, -1, CxGridMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridMementoColumnEClass, CxGridMementoColumn.class, "CxGridMementoColumn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridMementoColumn_PropertyId(), ecorePackage.getEString(), "propertyId", null, 0, 1, CxGridMementoColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMementoColumn_Editable(), ecorePackage.getEBoolean(), "editable", "false", 0, 1, CxGridMementoColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMementoColumn_ExpandRatio(), ecorePackage.getEInt(), "expandRatio", "-1", 0, 1, CxGridMementoColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMementoColumn_Hidden(), ecorePackage.getEBoolean(), "hidden", "false", 0, 1, CxGridMementoColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMementoColumn_Hideable(), ecorePackage.getEBoolean(), "hideable", "true", 0, 1, CxGridMementoColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMementoColumn_Sortable(), ecorePackage.getEBoolean(), "sortable", "true", 0, 1, CxGridMementoColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMementoColumn_Width(), ecorePackage.getEInt(), "width", "-1", 0, 1, CxGridMementoColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridMementoSortableEClass, CxGridMementoSortable.class, "CxGridMementoSortable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridMementoSortable_Descending(), ecorePackage.getEBoolean(), "descending", "false", 0, 1, CxGridMementoSortable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridMementoSortable_Column(), this.getCxGridMementoColumn(), null, "column", null, 1, 1, CxGridMementoSortable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

}
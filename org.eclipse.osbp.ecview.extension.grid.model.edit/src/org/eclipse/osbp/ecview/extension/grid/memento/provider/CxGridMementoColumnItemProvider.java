/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.memento.provider;


import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage;

import org.eclipse.osbp.ecview.extension.grid.provider.CxGridEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridMementoColumnItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public CxGridMementoColumnItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPropertyIdPropertyDescriptor(object);
			addEditablePropertyDescriptor(object);
			addExpandRatioPropertyDescriptor(object);
			addHiddenPropertyDescriptor(object);
			addHideablePropertyDescriptor(object);
			addSortablePropertyDescriptor(object);
			addWidthPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Property Id feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addPropertyIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMementoColumn_propertyId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMementoColumn_propertyId_feature", "_UI_CxGridMementoColumn_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN__PROPERTY_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editable feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addEditablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMementoColumn_editable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMementoColumn_editable_feature", "_UI_CxGridMementoColumn_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN__EDITABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Expand Ratio feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addExpandRatioPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMementoColumn_expandRatio_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMementoColumn_expandRatio_feature", "_UI_CxGridMementoColumn_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Hidden feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHiddenPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMementoColumn_hidden_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMementoColumn_hidden_feature", "_UI_CxGridMementoColumn_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN__HIDDEN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Hideable feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHideablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMementoColumn_hideable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMementoColumn_hideable_feature", "_UI_CxGridMementoColumn_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN__HIDEABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sortable feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addSortablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMementoColumn_sortable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMementoColumn_sortable_feature", "_UI_CxGridMementoColumn_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN__SORTABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Width feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addWidthPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMementoColumn_width_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMementoColumn_width_feature", "_UI_CxGridMementoColumn_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN__WIDTH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns CxGridMementoColumn.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CxGridMementoColumn"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CxGridMementoColumn)object).getPropertyId();
		return label == null || label.length() == 0 ?
			getString("_UI_CxGridMementoColumn_type") :
			getString("_UI_CxGridMementoColumn_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CxGridMementoColumn.class)) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__PROPERTY_ID:
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EDITABLE:
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO:
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDDEN:
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDEABLE:
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__SORTABLE:
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__WIDTH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the resource locator
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CxGridEditPlugin.INSTANCE;
	}

}

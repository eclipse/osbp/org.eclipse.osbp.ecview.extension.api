/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable;
import org.eclipse.osbp.ecview.core.common.model.core.YBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YCssAble;
import org.eclipse.osbp.ecview.core.common.model.core.YEditable;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YEnable;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YMarginable;
import org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YSpacingable;
import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;
import org.eclipse.osbp.ecview.core.common.model.core.YValueBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YVisibleable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanServiceConsumer;
import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;
import org.eclipse.osbp.ecview.extension.model.*;
import org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent;
import org.eclipse.osbp.ecview.extension.model.YCollectionSuspect;
import org.eclipse.osbp.ecview.extension.model.YColumnInfo;
import org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout;
import org.eclipse.osbp.ecview.extension.model.YCustomDecimalField;
import org.eclipse.osbp.ecview.extension.model.YDefaultLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YIconComboBox;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField;
import org.eclipse.osbp.ecview.extension.model.YMaskedNumericField;
import org.eclipse.osbp.ecview.extension.model.YMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YPairComboBox;
import org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YQuantityTextField;
import org.eclipse.osbp.ecview.extension.model.YRichTextArea;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSubTypeBaseSuspect;
import org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;
import org.eclipse.osbp.ecview.extension.model.YTypedCompoundSuspect;
import org.eclipse.osbp.ecview.extension.model.YTypedSuspect;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage
 * @generated
 */
public class YECviewSwitch<T> extends Switch<T> {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected static YECviewPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YECviewSwitch() {
		if (modelPackage == null) {
			modelPackage = YECviewPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param ePackage
	 *            the e package
	 * @return whether this is a switch for the given package.
	 * @parameter ePackage the package in question.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case YECviewPackage.YSTRATEGY_LAYOUT: {
				YStrategyLayout yStrategyLayout = (YStrategyLayout)theEObject;
				T result = caseYStrategyLayout(yStrategyLayout);
				if (result == null) result = caseYEmbeddable(yStrategyLayout);
				if (result == null) result = caseYElement(yStrategyLayout);
				if (result == null) result = caseYCssAble(yStrategyLayout);
				if (result == null) result = caseYVisibleable(yStrategyLayout);
				if (result == null) result = caseYAuthorizationable(yStrategyLayout);
				if (result == null) result = caseYTaggable(yStrategyLayout);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YLAYOUTING_STRATEGY: {
				YLayoutingStrategy yLayoutingStrategy = (YLayoutingStrategy)theEObject;
				T result = caseYLayoutingStrategy(yLayoutingStrategy);
				if (result == null) result = caseYElement(yLayoutingStrategy);
				if (result == null) result = caseYTaggable(yLayoutingStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YDEFAULT_LAYOUTING_STRATEGY: {
				YDefaultLayoutingStrategy yDefaultLayoutingStrategy = (YDefaultLayoutingStrategy)theEObject;
				T result = caseYDefaultLayoutingStrategy(yDefaultLayoutingStrategy);
				if (result == null) result = caseYLayoutingStrategy(yDefaultLayoutingStrategy);
				if (result == null) result = caseYElement(yDefaultLayoutingStrategy);
				if (result == null) result = caseYTaggable(yDefaultLayoutingStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YFOCUSING_STRATEGY: {
				YFocusingStrategy yFocusingStrategy = (YFocusingStrategy)theEObject;
				T result = caseYFocusingStrategy(yFocusingStrategy);
				if (result == null) result = caseYElement(yFocusingStrategy);
				if (result == null) result = caseYTaggable(yFocusingStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY: {
				YDelegatingLayoutingStrategy yDelegatingLayoutingStrategy = (YDelegatingLayoutingStrategy)theEObject;
				T result = caseYDelegatingLayoutingStrategy(yDelegatingLayoutingStrategy);
				if (result == null) result = caseYLayoutingStrategy(yDelegatingLayoutingStrategy);
				if (result == null) result = caseYElement(yDelegatingLayoutingStrategy);
				if (result == null) result = caseYTaggable(yDelegatingLayoutingStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YDELEGATING_FOCUSING_STRATEGY: {
				YDelegatingFocusingStrategy yDelegatingFocusingStrategy = (YDelegatingFocusingStrategy)theEObject;
				T result = caseYDelegatingFocusingStrategy(yDelegatingFocusingStrategy);
				if (result == null) result = caseYFocusingStrategy(yDelegatingFocusingStrategy);
				if (result == null) result = caseYElement(yDelegatingFocusingStrategy);
				if (result == null) result = caseYTaggable(yDelegatingFocusingStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YSUSPECT: {
				YSuspect ySuspect = (YSuspect)theEObject;
				T result = caseYSuspect(ySuspect);
				if (result == null) result = caseYElement(ySuspect);
				if (result == null) result = caseYAuthorizationable(ySuspect);
				if (result == null) result = caseYTaggable(ySuspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YTYPED_SUSPECT: {
				YTypedSuspect yTypedSuspect = (YTypedSuspect)theEObject;
				T result = caseYTypedSuspect(yTypedSuspect);
				if (result == null) result = caseYSuspect(yTypedSuspect);
				if (result == null) result = caseYElement(yTypedSuspect);
				if (result == null) result = caseYAuthorizationable(yTypedSuspect);
				if (result == null) result = caseYTaggable(yTypedSuspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YTYPED_COMPOUND_SUSPECT: {
				YTypedCompoundSuspect yTypedCompoundSuspect = (YTypedCompoundSuspect)theEObject;
				T result = caseYTypedCompoundSuspect(yTypedCompoundSuspect);
				if (result == null) result = caseYTypedSuspect(yTypedCompoundSuspect);
				if (result == null) result = caseYSuspect(yTypedCompoundSuspect);
				if (result == null) result = caseYElement(yTypedCompoundSuspect);
				if (result == null) result = caseYAuthorizationable(yTypedCompoundSuspect);
				if (result == null) result = caseYTaggable(yTypedCompoundSuspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YSUB_TYPE_BASE_SUSPECT: {
				YSubTypeBaseSuspect ySubTypeBaseSuspect = (YSubTypeBaseSuspect)theEObject;
				T result = caseYSubTypeBaseSuspect(ySubTypeBaseSuspect);
				if (result == null) result = caseYTypedCompoundSuspect(ySubTypeBaseSuspect);
				if (result == null) result = caseYTypedSuspect(ySubTypeBaseSuspect);
				if (result == null) result = caseYSuspect(ySubTypeBaseSuspect);
				if (result == null) result = caseYElement(ySubTypeBaseSuspect);
				if (result == null) result = caseYAuthorizationable(ySubTypeBaseSuspect);
				if (result == null) result = caseYTaggable(ySubTypeBaseSuspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YSUB_TYPE_SUSPECT: {
				YSubTypeSuspect ySubTypeSuspect = (YSubTypeSuspect)theEObject;
				T result = caseYSubTypeSuspect(ySubTypeSuspect);
				if (result == null) result = caseYTypedCompoundSuspect(ySubTypeSuspect);
				if (result == null) result = caseYTypedSuspect(ySubTypeSuspect);
				if (result == null) result = caseYSuspect(ySubTypeSuspect);
				if (result == null) result = caseYElement(ySubTypeSuspect);
				if (result == null) result = caseYAuthorizationable(ySubTypeSuspect);
				if (result == null) result = caseYTaggable(ySubTypeSuspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YLAYOUTING_INFO: {
				YLayoutingInfo yLayoutingInfo = (YLayoutingInfo)theEObject;
				T result = caseYLayoutingInfo(yLayoutingInfo);
				if (result == null) result = caseYElement(yLayoutingInfo);
				if (result == null) result = caseYTaggable(yLayoutingInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YSUSPECT_INFO: {
				YSuspectInfo ySuspectInfo = (YSuspectInfo)theEObject;
				T result = caseYSuspectInfo(ySuspectInfo);
				if (result == null) result = caseYElement(ySuspectInfo);
				if (result == null) result = caseYTaggable(ySuspectInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YBLOB_UPLOAD_COMPONENT: {
				YBlobUploadComponent yBlobUploadComponent = (YBlobUploadComponent)theEObject;
				T result = caseYBlobUploadComponent(yBlobUploadComponent);
				if (result == null) result = caseYInput(yBlobUploadComponent);
				if (result == null) result = caseYValueBindable(yBlobUploadComponent);
				if (result == null) result = caseYField(yBlobUploadComponent);
				if (result == null) result = caseYBindable(yBlobUploadComponent);
				if (result == null) result = caseYEmbeddable(yBlobUploadComponent);
				if (result == null) result = caseYEditable(yBlobUploadComponent);
				if (result == null) result = caseYEnable(yBlobUploadComponent);
				if (result == null) result = caseYFocusable(yBlobUploadComponent);
				if (result == null) result = caseYElement(yBlobUploadComponent);
				if (result == null) result = caseYCssAble(yBlobUploadComponent);
				if (result == null) result = caseYVisibleable(yBlobUploadComponent);
				if (result == null) result = caseYAuthorizationable(yBlobUploadComponent);
				if (result == null) result = caseYTaggable(yBlobUploadComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YCUSTOM_DECIMAL_FIELD: {
				YCustomDecimalField yCustomDecimalField = (YCustomDecimalField)theEObject;
				T result = caseYCustomDecimalField(yCustomDecimalField);
				if (result == null) result = caseYInput(yCustomDecimalField);
				if (result == null) result = caseYValueBindable(yCustomDecimalField);
				if (result == null) result = caseYField(yCustomDecimalField);
				if (result == null) result = caseYBindable(yCustomDecimalField);
				if (result == null) result = caseYEmbeddable(yCustomDecimalField);
				if (result == null) result = caseYEditable(yCustomDecimalField);
				if (result == null) result = caseYEnable(yCustomDecimalField);
				if (result == null) result = caseYFocusable(yCustomDecimalField);
				if (result == null) result = caseYElement(yCustomDecimalField);
				if (result == null) result = caseYCssAble(yCustomDecimalField);
				if (result == null) result = caseYVisibleable(yCustomDecimalField);
				if (result == null) result = caseYAuthorizationable(yCustomDecimalField);
				if (result == null) result = caseYTaggable(yCustomDecimalField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YI1_8N_COMBO_BOX: {
				YI18nComboBox yi18nComboBox = (YI18nComboBox)theEObject;
				T result = caseYI18nComboBox(yi18nComboBox);
				if (result == null) result = caseYInput(yi18nComboBox);
				if (result == null) result = caseYCollectionBindable(yi18nComboBox);
				if (result == null) result = caseYSelectionBindable(yi18nComboBox);
				if (result == null) result = caseYField(yi18nComboBox);
				if (result == null) result = caseYBindable(yi18nComboBox);
				if (result == null) result = caseYEmbeddable(yi18nComboBox);
				if (result == null) result = caseYEditable(yi18nComboBox);
				if (result == null) result = caseYEnable(yi18nComboBox);
				if (result == null) result = caseYFocusable(yi18nComboBox);
				if (result == null) result = caseYElement(yi18nComboBox);
				if (result == null) result = caseYCssAble(yi18nComboBox);
				if (result == null) result = caseYVisibleable(yi18nComboBox);
				if (result == null) result = caseYAuthorizationable(yi18nComboBox);
				if (result == null) result = caseYTaggable(yi18nComboBox);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YICON_COMBO_BOX: {
				YIconComboBox yIconComboBox = (YIconComboBox)theEObject;
				T result = caseYIconComboBox(yIconComboBox);
				if (result == null) result = caseYInput(yIconComboBox);
				if (result == null) result = caseYCollectionBindable(yIconComboBox);
				if (result == null) result = caseYSelectionBindable(yIconComboBox);
				if (result == null) result = caseYBeanServiceConsumer(yIconComboBox);
				if (result == null) result = caseYField(yIconComboBox);
				if (result == null) result = caseYBindable(yIconComboBox);
				if (result == null) result = caseYEmbeddable(yIconComboBox);
				if (result == null) result = caseYEditable(yIconComboBox);
				if (result == null) result = caseYEnable(yIconComboBox);
				if (result == null) result = caseYFocusable(yIconComboBox);
				if (result == null) result = caseYElement(yIconComboBox);
				if (result == null) result = caseYCssAble(yIconComboBox);
				if (result == null) result = caseYVisibleable(yIconComboBox);
				if (result == null) result = caseYAuthorizationable(yIconComboBox);
				if (result == null) result = caseYTaggable(yIconComboBox);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YQUANTITY_TEXT_FIELD: {
				YQuantityTextField yQuantityTextField = (YQuantityTextField)theEObject;
				T result = caseYQuantityTextField(yQuantityTextField);
				if (result == null) result = caseYInput(yQuantityTextField);
				if (result == null) result = caseYValueBindable(yQuantityTextField);
				if (result == null) result = caseYField(yQuantityTextField);
				if (result == null) result = caseYBindable(yQuantityTextField);
				if (result == null) result = caseYEmbeddable(yQuantityTextField);
				if (result == null) result = caseYEditable(yQuantityTextField);
				if (result == null) result = caseYEnable(yQuantityTextField);
				if (result == null) result = caseYFocusable(yQuantityTextField);
				if (result == null) result = caseYElement(yQuantityTextField);
				if (result == null) result = caseYCssAble(yQuantityTextField);
				if (result == null) result = caseYVisibleable(yQuantityTextField);
				if (result == null) result = caseYAuthorizationable(yQuantityTextField);
				if (result == null) result = caseYTaggable(yQuantityTextField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YCOLLECTION_SUSPECT: {
				YCollectionSuspect yCollectionSuspect = (YCollectionSuspect)theEObject;
				T result = caseYCollectionSuspect(yCollectionSuspect);
				if (result == null) result = caseYTypedSuspect(yCollectionSuspect);
				if (result == null) result = caseYSuspect(yCollectionSuspect);
				if (result == null) result = caseYElement(yCollectionSuspect);
				if (result == null) result = caseYAuthorizationable(yCollectionSuspect);
				if (result == null) result = caseYTaggable(yCollectionSuspect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YCOLUMN_INFO: {
				YColumnInfo yColumnInfo = (YColumnInfo)theEObject;
				T result = caseYColumnInfo(yColumnInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YCONTENT_SENSITIVE_LAYOUT: {
				YContentSensitiveLayout yContentSensitiveLayout = (YContentSensitiveLayout)theEObject;
				T result = caseYContentSensitiveLayout(yContentSensitiveLayout);
				if (result == null) result = caseYLayout(yContentSensitiveLayout);
				if (result == null) result = caseYSpacingable(yContentSensitiveLayout);
				if (result == null) result = caseYMarginable(yContentSensitiveLayout);
				if (result == null) result = caseYEmbeddable(yContentSensitiveLayout);
				if (result == null) result = caseYEditable(yContentSensitiveLayout);
				if (result == null) result = caseYEnable(yContentSensitiveLayout);
				if (result == null) result = caseYElement(yContentSensitiveLayout);
				if (result == null) result = caseYCssAble(yContentSensitiveLayout);
				if (result == null) result = caseYVisibleable(yContentSensitiveLayout);
				if (result == null) result = caseYAuthorizationable(yContentSensitiveLayout);
				if (result == null) result = caseYTaggable(yContentSensitiveLayout);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YRICH_TEXT_AREA: {
				YRichTextArea yRichTextArea = (YRichTextArea)theEObject;
				T result = caseYRichTextArea(yRichTextArea);
				if (result == null) result = caseYInput(yRichTextArea);
				if (result == null) result = caseYValueBindable(yRichTextArea);
				if (result == null) result = caseYField(yRichTextArea);
				if (result == null) result = caseYBindable(yRichTextArea);
				if (result == null) result = caseYEmbeddable(yRichTextArea);
				if (result == null) result = caseYEditable(yRichTextArea);
				if (result == null) result = caseYEnable(yRichTextArea);
				if (result == null) result = caseYFocusable(yRichTextArea);
				if (result == null) result = caseYElement(yRichTextArea);
				if (result == null) result = caseYCssAble(yRichTextArea);
				if (result == null) result = caseYVisibleable(yRichTextArea);
				if (result == null) result = caseYAuthorizationable(yRichTextArea);
				if (result == null) result = caseYTaggable(yRichTextArea);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YMASKED_TEXT_FIELD: {
				YMaskedTextField yMaskedTextField = (YMaskedTextField)theEObject;
				T result = caseYMaskedTextField(yMaskedTextField);
				if (result == null) result = caseYInput(yMaskedTextField);
				if (result == null) result = caseYValueBindable(yMaskedTextField);
				if (result == null) result = caseYField(yMaskedTextField);
				if (result == null) result = caseYBindable(yMaskedTextField);
				if (result == null) result = caseYEmbeddable(yMaskedTextField);
				if (result == null) result = caseYEditable(yMaskedTextField);
				if (result == null) result = caseYEnable(yMaskedTextField);
				if (result == null) result = caseYFocusable(yMaskedTextField);
				if (result == null) result = caseYElement(yMaskedTextField);
				if (result == null) result = caseYCssAble(yMaskedTextField);
				if (result == null) result = caseYVisibleable(yMaskedTextField);
				if (result == null) result = caseYAuthorizationable(yMaskedTextField);
				if (result == null) result = caseYTaggable(yMaskedTextField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YPREFIXED_MASKED_TEXT_FIELD: {
				YPrefixedMaskedTextField yPrefixedMaskedTextField = (YPrefixedMaskedTextField)theEObject;
				T result = caseYPrefixedMaskedTextField(yPrefixedMaskedTextField);
				if (result == null) result = caseYInput(yPrefixedMaskedTextField);
				if (result == null) result = caseYValueBindable(yPrefixedMaskedTextField);
				if (result == null) result = caseYField(yPrefixedMaskedTextField);
				if (result == null) result = caseYBindable(yPrefixedMaskedTextField);
				if (result == null) result = caseYEmbeddable(yPrefixedMaskedTextField);
				if (result == null) result = caseYEditable(yPrefixedMaskedTextField);
				if (result == null) result = caseYEnable(yPrefixedMaskedTextField);
				if (result == null) result = caseYFocusable(yPrefixedMaskedTextField);
				if (result == null) result = caseYElement(yPrefixedMaskedTextField);
				if (result == null) result = caseYCssAble(yPrefixedMaskedTextField);
				if (result == null) result = caseYVisibleable(yPrefixedMaskedTextField);
				if (result == null) result = caseYAuthorizationable(yPrefixedMaskedTextField);
				if (result == null) result = caseYTaggable(yPrefixedMaskedTextField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YMASKED_NUMERIC_FIELD: {
				YMaskedNumericField yMaskedNumericField = (YMaskedNumericField)theEObject;
				T result = caseYMaskedNumericField(yMaskedNumericField);
				if (result == null) result = caseYInput(yMaskedNumericField);
				if (result == null) result = caseYValueBindable(yMaskedNumericField);
				if (result == null) result = caseYField(yMaskedNumericField);
				if (result == null) result = caseYBindable(yMaskedNumericField);
				if (result == null) result = caseYEmbeddable(yMaskedNumericField);
				if (result == null) result = caseYEditable(yMaskedNumericField);
				if (result == null) result = caseYEnable(yMaskedNumericField);
				if (result == null) result = caseYFocusable(yMaskedNumericField);
				if (result == null) result = caseYElement(yMaskedNumericField);
				if (result == null) result = caseYCssAble(yMaskedNumericField);
				if (result == null) result = caseYVisibleable(yMaskedNumericField);
				if (result == null) result = caseYAuthorizationable(yMaskedNumericField);
				if (result == null) result = caseYTaggable(yMaskedNumericField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YMASKED_DECIMAL_FIELD: {
				YMaskedDecimalField yMaskedDecimalField = (YMaskedDecimalField)theEObject;
				T result = caseYMaskedDecimalField(yMaskedDecimalField);
				if (result == null) result = caseYInput(yMaskedDecimalField);
				if (result == null) result = caseYValueBindable(yMaskedDecimalField);
				if (result == null) result = caseYField(yMaskedDecimalField);
				if (result == null) result = caseYBindable(yMaskedDecimalField);
				if (result == null) result = caseYEmbeddable(yMaskedDecimalField);
				if (result == null) result = caseYEditable(yMaskedDecimalField);
				if (result == null) result = caseYEnable(yMaskedDecimalField);
				if (result == null) result = caseYFocusable(yMaskedDecimalField);
				if (result == null) result = caseYElement(yMaskedDecimalField);
				if (result == null) result = caseYCssAble(yMaskedDecimalField);
				if (result == null) result = caseYVisibleable(yMaskedDecimalField);
				if (result == null) result = caseYAuthorizationable(yMaskedDecimalField);
				if (result == null) result = caseYTaggable(yMaskedDecimalField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YECviewPackage.YPAIR_COMBO_BOX: {
				YPairComboBox yPairComboBox = (YPairComboBox)theEObject;
				T result = caseYPairComboBox(yPairComboBox);
				if (result == null) result = caseYInput(yPairComboBox);
				if (result == null) result = caseYCollectionBindable(yPairComboBox);
				if (result == null) result = caseYSelectionBindable(yPairComboBox);
				if (result == null) result = caseYBeanServiceConsumer(yPairComboBox);
				if (result == null) result = caseYField(yPairComboBox);
				if (result == null) result = caseYBindable(yPairComboBox);
				if (result == null) result = caseYEmbeddable(yPairComboBox);
				if (result == null) result = caseYEditable(yPairComboBox);
				if (result == null) result = caseYEnable(yPairComboBox);
				if (result == null) result = caseYFocusable(yPairComboBox);
				if (result == null) result = caseYElement(yPairComboBox);
				if (result == null) result = caseYCssAble(yPairComboBox);
				if (result == null) result = caseYVisibleable(yPairComboBox);
				if (result == null) result = caseYAuthorizationable(yPairComboBox);
				if (result == null) result = caseYTaggable(yPairComboBox);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YStrategy Layout</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YStrategy Layout</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYStrategyLayout(YStrategyLayout object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YLayouting Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YLayouting Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYLayoutingStrategy(YLayoutingStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YDefault Layouting Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YDefault Layouting Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYDefaultLayoutingStrategy(YDefaultLayoutingStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YFocusing Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YFocusing Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYFocusingStrategy(YFocusingStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YDelegating Layouting Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YDelegating Layouting Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYDelegatingLayoutingStrategy(YDelegatingLayoutingStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YDelegating Focusing Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YDelegating Focusing Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYDelegatingFocusingStrategy(YDelegatingFocusingStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSuspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSuspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSuspect(YSuspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YLayouting Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YLayouting Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYLayoutingInfo(YLayoutingInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSuspect Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSuspect Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSuspectInfo(YSuspectInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YBlob Upload Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YBlob Upload Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYBlobUploadComponent(YBlobUploadComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YTyped Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YTyped Suspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYTypedSuspect(YTypedSuspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YTyped Compound Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YTyped Compound Suspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYTypedCompoundSuspect(YTypedCompoundSuspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSub Type Base Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSub Type Base Suspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSubTypeBaseSuspect(YSubTypeBaseSuspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSub Type Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSub Type Suspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSubTypeSuspect(YSubTypeSuspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YCustom Decimal Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YCustom Decimal Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYCustomDecimalField(YCustomDecimalField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YI1 8n Combo Box</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YI1 8n Combo Box</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYI18nComboBox(YI18nComboBox object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YIcon Combo Box</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YIcon Combo Box</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYIconComboBox(YIconComboBox object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YQuantity Text Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YQuantity Text Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYQuantityTextField(YQuantityTextField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YCollection Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YCollection Suspect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYCollectionSuspect(YCollectionSuspect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YColumn Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YColumn Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYColumnInfo(YColumnInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YContent Sensitive Layout</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YContent Sensitive Layout</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYContentSensitiveLayout(YContentSensitiveLayout object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YRich Text Area</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YRich Text Area</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYRichTextArea(YRichTextArea object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YMasked Text Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YMasked Text Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYMaskedTextField(YMaskedTextField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YPrefixed Masked Text Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YPrefixed Masked Text Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYPrefixedMaskedTextField(YPrefixedMaskedTextField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YMasked Numeric Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YMasked Numeric Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYMaskedNumericField(YMaskedNumericField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YMasked Decimal Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YMasked Decimal Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYMaskedDecimalField(YMaskedDecimalField object) {
		return null;
	}
	
	/**
	 * Returns the result of interpreting the object as an instance of '<em>YPair Combo Box</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YPair Combo Box</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYPairComboBox(YPairComboBox object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYTaggable(YTaggable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYElement(YElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YCss Able</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YCss Able</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYCssAble(YCssAble object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YVisibleable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YVisibleable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYVisibleable(YVisibleable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YAuthorizationable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YAuthorizationable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYAuthorizationable(YAuthorizationable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YEmbeddable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YEmbeddable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYEmbeddable(YEmbeddable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YEditable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YEditable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYEditable(YEditable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YEnable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YEnable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYEnable(YEnable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YFocusable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YFocusable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYFocusable(YFocusable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YField</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YField</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYField(YField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YInput</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YInput</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYInput(YInput object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YBindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YBindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYBindable(YBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YValue Bindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YValue Bindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYValueBindable(YValueBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YCollection Bindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YCollection Bindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYCollectionBindable(YCollectionBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSelection Bindable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSelection Bindable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSelectionBindable(YSelectionBindable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YBean Service Consumer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YBean Service Consumer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYBeanServiceConsumer(YBeanServiceConsumer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YLayout</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YLayout</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYLayout(YLayout object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSpacingable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSpacingable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSpacingable(YSpacingable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YMarginable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YMarginable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYMarginable(YMarginable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

}

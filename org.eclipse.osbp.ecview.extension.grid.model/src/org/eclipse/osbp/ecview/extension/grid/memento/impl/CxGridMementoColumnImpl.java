/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento.impl;

import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Column</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl#getPropertyId <em>Property Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl#isEditable <em>Editable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl#getExpandRatio <em>Expand Ratio</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl#isHidden <em>Hidden</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl#isHideable <em>Hideable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl#isSortable <em>Sortable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoColumnImpl#getWidth <em>Width</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridMementoColumnImpl extends MinimalEObjectImpl.Container implements CxGridMementoColumn {
	
	/**
	 * The default value of the '{@link #getPropertyId() <em>Property Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyId()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPropertyId() <em>Property Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyId()
	 * @generated
	 * @ordered
	 */
	protected String propertyId = PROPERTY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isEditable() <em>Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEditable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EDITABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEditable() <em>Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEditable()
	 * @generated
	 * @ordered
	 */
	protected boolean editable = EDITABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getExpandRatio() <em>Expand Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpandRatio()
	 * @generated
	 * @ordered
	 */
	protected static final int EXPAND_RATIO_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getExpandRatio() <em>Expand Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpandRatio()
	 * @generated
	 * @ordered
	 */
	protected int expandRatio = EXPAND_RATIO_EDEFAULT;

	/**
	 * The default value of the '{@link #isHidden() <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDDEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHidden() <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHidden()
	 * @generated
	 * @ordered
	 */
	protected boolean hidden = HIDDEN_EDEFAULT;

	/**
	 * The default value of the '{@link #isHideable() <em>Hideable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDEABLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isHideable() <em>Hideable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideable()
	 * @generated
	 * @ordered
	 */
	protected boolean hideable = HIDEABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #isSortable() <em>Sortable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSortable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SORTABLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isSortable() <em>Sortable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSortable()
	 * @generated
	 * @ordered
	 */
	protected boolean sortable = SORTABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int WIDTH_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected int width = WIDTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected CxGridMementoColumnImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridMementoPackage.Literals.CX_GRID_MEMENTO_COLUMN;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getPropertyId()
	 *         <em>Property Id</em>}' attribute
	 * @generated
	 */
	public String getPropertyId() {
		return propertyId;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newPropertyId
	 *            the new cached value of the '{@link #getPropertyId()
	 *            <em>Property Id</em>}' attribute
	 * @generated
	 */
	public void setPropertyId(String newPropertyId) {
		String oldPropertyId = propertyId;
		propertyId = newPropertyId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__PROPERTY_ID, oldPropertyId, propertyId));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #isEditable() <em>Editable</em>}'
	 *         attribute
	 * @generated
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newEditable
	 *            the new cached value of the '{@link #isEditable()
	 *            <em>Editable</em>}' attribute
	 * @generated
	 */
	public void setEditable(boolean newEditable) {
		boolean oldEditable = editable;
		editable = newEditable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EDITABLE, oldEditable, editable));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getExpandRatio()
	 *         <em>Expand Ratio</em>}' attribute
	 * @generated
	 */
	public int getExpandRatio() {
		return expandRatio;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newExpandRatio
	 *            the new cached value of the '{@link #getExpandRatio()
	 *            <em>Expand Ratio</em>}' attribute
	 * @generated
	 */
	public void setExpandRatio(int newExpandRatio) {
		int oldExpandRatio = expandRatio;
		expandRatio = newExpandRatio;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO, oldExpandRatio, expandRatio));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #isHidden() <em>Hidden</em>}'
	 *         attribute
	 * @generated
	 */
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newHidden
	 *            the new cached value of the '{@link #isHidden()
	 *            <em>Hidden</em>}' attribute
	 * @generated
	 */
	public void setHidden(boolean newHidden) {
		boolean oldHidden = hidden;
		hidden = newHidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDDEN, oldHidden, hidden));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #isHideable() <em>Hideable</em>}'
	 *         attribute
	 * @generated
	 */
	public boolean isHideable() {
		return hideable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newHideable
	 *            the new cached value of the '{@link #isHideable()
	 *            <em>Hideable</em>}' attribute
	 * @generated
	 */
	public void setHideable(boolean newHideable) {
		boolean oldHideable = hideable;
		hideable = newHideable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDEABLE, oldHideable, hideable));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #isSortable() <em>Sortable</em>}'
	 *         attribute
	 * @generated
	 */
	public boolean isSortable() {
		return sortable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newSortable
	 *            the new cached value of the '{@link #isSortable()
	 *            <em>Sortable</em>}' attribute
	 * @generated
	 */
	public void setSortable(boolean newSortable) {
		boolean oldSortable = sortable;
		sortable = newSortable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__SORTABLE, oldSortable, sortable));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getWidth() <em>Width</em>}'
	 *         attribute
	 * @generated
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newWidth
	 *            the new cached value of the '{@link #getWidth()
	 *            <em>Width</em>}' attribute
	 * @generated
	 */
	public void setWidth(int newWidth) {
		int oldWidth = width;
		width = newWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__WIDTH, oldWidth, width));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__PROPERTY_ID:
				return getPropertyId();
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EDITABLE:
				return isEditable();
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO:
				return getExpandRatio();
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDDEN:
				return isHidden();
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDEABLE:
				return isHideable();
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__SORTABLE:
				return isSortable();
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__WIDTH:
				return getWidth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__PROPERTY_ID:
				setPropertyId((String)newValue);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EDITABLE:
				setEditable((Boolean)newValue);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO:
				setExpandRatio((Integer)newValue);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDDEN:
				setHidden((Boolean)newValue);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDEABLE:
				setHideable((Boolean)newValue);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__SORTABLE:
				setSortable((Boolean)newValue);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__WIDTH:
				setWidth((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__PROPERTY_ID:
				setPropertyId(PROPERTY_ID_EDEFAULT);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EDITABLE:
				setEditable(EDITABLE_EDEFAULT);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO:
				setExpandRatio(EXPAND_RATIO_EDEFAULT);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDDEN:
				setHidden(HIDDEN_EDEFAULT);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDEABLE:
				setHideable(HIDEABLE_EDEFAULT);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__SORTABLE:
				setSortable(SORTABLE_EDEFAULT);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__WIDTH:
				setWidth(WIDTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__PROPERTY_ID:
				return PROPERTY_ID_EDEFAULT == null ? propertyId != null : !PROPERTY_ID_EDEFAULT.equals(propertyId);
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EDITABLE:
				return editable != EDITABLE_EDEFAULT;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__EXPAND_RATIO:
				return expandRatio != EXPAND_RATIO_EDEFAULT;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDDEN:
				return hidden != HIDDEN_EDEFAULT;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__HIDEABLE:
				return hideable != HIDEABLE_EDEFAULT;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__SORTABLE:
				return sortable != SORTABLE_EDEFAULT;
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN__WIDTH:
				return width != WIDTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (propertyId: ");
		result.append(propertyId);
		result.append(", editable: ");
		result.append(editable);
		result.append(", expandRatio: ");
		result.append(expandRatio);
		result.append(", hidden: ");
		result.append(hidden);
		result.append(", hideable: ");
		result.append(hideable);
		result.append(", sortable: ");
		result.append(sortable);
		result.append(", width: ");
		result.append(width);
		result.append(')');
		return result.toString();
	}

}
/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YLayouting Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getLayout <em>Layout</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getContent <em>Content</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getActiveSuspectInfos <em>Active Suspect Infos</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getFirstFocus <em>First Focus</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYLayoutingInfo()
 * @model
 * @generated
 */
public interface YLayoutingInfo extends YElement {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Layout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout</em>' reference.
	 * @see #setLayout(YStrategyLayout)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYLayoutingInfo_Layout()
	 * @model
	 * @generated
	 */
	YStrategyLayout getLayout();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getLayout <em>Layout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout</em>' reference.
	 * @see #getLayout()
	 * @generated
	 */
	void setLayout(YStrategyLayout value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' containment reference.
	 * @see #setContent(YEmbeddable)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYLayoutingInfo_Content()
	 * @model containment="true"
	 * @generated
	 */
	YEmbeddable getContent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getContent <em>Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' containment reference.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(YEmbeddable value);

	/**
	 * Returns the value of the '<em><b>Active Suspect Infos</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Suspect Infos</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Suspect Infos</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYLayoutingInfo_ActiveSuspectInfos()
	 * @model containment="true"
	 * @generated
	 */
	EList<YSuspectInfo> getActiveSuspectInfos();

	/**
	 * Returns the value of the '<em><b>First Focus</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Focus</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Focus</em>' reference.
	 * @see #setFirstFocus(YSuspectInfo)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYLayoutingInfo_FirstFocus()
	 * @model
	 * @generated
	 */
	YSuspectInfo getFirstFocus();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo#getFirstFocus <em>First Focus</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Focus</em>' reference.
	 * @see #getFirstFocus()
	 * @generated
	 */
	void setFirstFocus(YSuspectInfo value);

	/**
	 * Creates a new suspect.
	 *
	 * @param suspect
	 *            the suspect
	 * @return the y suspect info
	 */
	YSuspectInfo createSuspectInfo(YSuspect suspect);
	
	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	YView getView();

}

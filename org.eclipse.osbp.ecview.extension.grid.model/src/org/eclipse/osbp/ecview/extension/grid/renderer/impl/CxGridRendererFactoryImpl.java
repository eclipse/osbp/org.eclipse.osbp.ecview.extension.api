/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer.impl;

import org.eclipse.osbp.ecview.extension.grid.renderer.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridRendererFactoryImpl extends EFactoryImpl implements CxGridRendererFactory {
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static CxGridRendererFactory init() {
		try {
			CxGridRendererFactory theCxGridRendererFactory = (CxGridRendererFactory)EPackage.Registry.INSTANCE.getEFactory(CxGridRendererPackage.eNS_URI);
			if (theCxGridRendererFactory != null) {
				return theCxGridRendererFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CxGridRendererFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridRendererFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CxGridRendererPackage.CX_GRID_DELEGATE_RENDERER: return createCxGridDelegateRenderer();
			case CxGridRendererPackage.CX_GRID_DATE_RENDERER: return createCxGridDateRenderer();
			case CxGridRendererPackage.CX_GRID_HTML_RENDERER: return createCxGridHtmlRenderer();
			case CxGridRendererPackage.CX_GRID_NUMBER_RENDERER: return createCxGridNumberRenderer();
			case CxGridRendererPackage.CX_GRID_PROGRESS_BAR_RENDERER: return createCxGridProgressBarRenderer();
			case CxGridRendererPackage.CX_GRID_TEXT_RENDERER: return createCxGridTextRenderer();
			case CxGridRendererPackage.CX_GRID_BUTTON_RENDERER: return createCxGridButtonRenderer();
			case CxGridRendererPackage.CX_GRID_BLOB_IMAGE_RENDERER: return createCxGridBlobImageRenderer();
			case CxGridRendererPackage.CX_GRID_IMAGE_RENDERER: return createCxGridImageRenderer();
			case CxGridRendererPackage.CX_GRID_RENDERER_CLICK_EVENT: return createCxGridRendererClickEvent();
			case CxGridRendererPackage.CX_GRID_BOOLEAN_RENDERER: return createCxGridBooleanRenderer();
			case CxGridRendererPackage.CX_GRID_QUANTITY_RENDERER: return createCxGridQuantityRenderer();
			case CxGridRendererPackage.CX_GRID_PRICE_RENDERER: return createCxGridPriceRenderer();
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER: return createCxGridIndicatorRenderer();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER: return createCxGridNestedConverter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid delegate renderer
	 * @generated
	 */
	public CxGridDelegateRenderer createCxGridDelegateRenderer() {
		CxGridDelegateRendererImpl cxGridDelegateRenderer = new CxGridDelegateRendererImpl();
		return cxGridDelegateRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid date renderer
	 * @generated
	 */
	public CxGridDateRenderer createCxGridDateRenderer() {
		CxGridDateRendererImpl cxGridDateRenderer = new CxGridDateRendererImpl();
		return cxGridDateRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid html renderer
	 * @generated
	 */
	public CxGridHtmlRenderer createCxGridHtmlRenderer() {
		CxGridHtmlRendererImpl cxGridHtmlRenderer = new CxGridHtmlRendererImpl();
		return cxGridHtmlRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid number renderer
	 * @generated
	 */
	public CxGridNumberRenderer createCxGridNumberRenderer() {
		CxGridNumberRendererImpl cxGridNumberRenderer = new CxGridNumberRendererImpl();
		return cxGridNumberRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid progress bar renderer
	 * @generated
	 */
	public CxGridProgressBarRenderer createCxGridProgressBarRenderer() {
		CxGridProgressBarRendererImpl cxGridProgressBarRenderer = new CxGridProgressBarRendererImpl();
		return cxGridProgressBarRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid text renderer
	 * @generated
	 */
	public CxGridTextRenderer createCxGridTextRenderer() {
		CxGridTextRendererImpl cxGridTextRenderer = new CxGridTextRendererImpl();
		return cxGridTextRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid button renderer
	 * @generated
	 */
	public CxGridButtonRenderer createCxGridButtonRenderer() {
		CxGridButtonRendererImpl cxGridButtonRenderer = new CxGridButtonRendererImpl();
		return cxGridButtonRenderer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridBlobImageRenderer createCxGridBlobImageRenderer() {
		CxGridBlobImageRendererImpl cxGridBlobImageRenderer = new CxGridBlobImageRendererImpl();
		return cxGridBlobImageRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid image renderer
	 * @generated
	 */
	public CxGridImageRenderer createCxGridImageRenderer() {
		CxGridImageRendererImpl cxGridImageRenderer = new CxGridImageRendererImpl();
		return cxGridImageRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid renderer click event
	 * @generated
	 */
	public CxGridRendererClickEvent createCxGridRendererClickEvent() {
		CxGridRendererClickEventImpl cxGridRendererClickEvent = new CxGridRendererClickEventImpl();
		return cxGridRendererClickEvent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid boolean renderer
	 * @generated
	 */
	public CxGridBooleanRenderer createCxGridBooleanRenderer() {
		CxGridBooleanRendererImpl cxGridBooleanRenderer = new CxGridBooleanRendererImpl();
		return cxGridBooleanRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid quantity renderer
	 * @generated
	 */
	public CxGridQuantityRenderer createCxGridQuantityRenderer() {
		CxGridQuantityRendererImpl cxGridQuantityRenderer = new CxGridQuantityRendererImpl();
		return cxGridQuantityRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid price renderer
	 * @generated
	 */
	public CxGridPriceRenderer createCxGridPriceRenderer() {
		CxGridPriceRendererImpl cxGridPriceRenderer = new CxGridPriceRendererImpl();
		return cxGridPriceRenderer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid indicator renderer
	 * @generated
	 */
	public CxGridIndicatorRenderer createCxGridIndicatorRenderer() {
		CxGridIndicatorRendererImpl cxGridIndicatorRenderer = new CxGridIndicatorRendererImpl();
		return cxGridIndicatorRenderer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridNestedConverter createCxGridNestedConverter() {
		CxGridNestedConverterImpl cxGridNestedConverter = new CxGridNestedConverterImpl();
		return cxGridNestedConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid renderer package
	 * @generated
	 */
	public CxGridRendererPackage getCxGridRendererPackage() {
		return (CxGridRendererPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CxGridRendererPackage getPackage() {
		return CxGridRendererPackage.eINSTANCE;
	}

}
/**
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>renderer</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridRendererTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new CxGridRendererTests("renderer Tests");
		suite.addTestSuite(CxGridButtonRendererTest.class);
		suite.addTestSuite(CxGridBlobImageRendererTest.class);
		suite.addTestSuite(CxGridImageRendererTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridRendererTests(String name) {
		super(name);
	}

} //CxGridRendererTests

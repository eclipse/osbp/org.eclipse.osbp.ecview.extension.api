/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.visibility.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl;
import org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl;
import org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityFactory;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class YVisibilityPackageImpl extends EPackageImpl implements YVisibilityPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yAuthorizationVisibilityProcessorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ySubTypeVisibilityProcessorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private YVisibilityPackageImpl() {
		super(eNS_URI, YVisibilityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link YVisibilityPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the y visibility package
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static YVisibilityPackage init() {
		if (isInited) return (YVisibilityPackage)EPackage.Registry.INSTANCE.getEPackage(YVisibilityPackage.eNS_URI);

		// Obtain or create and register package
		YVisibilityPackageImpl theYVisibilityPackage = (YVisibilityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof YVisibilityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new YVisibilityPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ExtDatatypesPackage.eINSTANCE.eClass();
		ExtensionModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		YECviewPackageImpl theYECviewPackage = (YECviewPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(YECviewPackage.eNS_URI) instanceof YECviewPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(YECviewPackage.eNS_URI) : YECviewPackage.eINSTANCE);
		YConverterPackageImpl theYConverterPackage = (YConverterPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(YConverterPackage.eNS_URI) instanceof YConverterPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(YConverterPackage.eNS_URI) : YConverterPackage.eINSTANCE);

		// Create package meta-data objects
		theYVisibilityPackage.createPackageContents();
		theYECviewPackage.createPackageContents();
		theYConverterPackage.createPackageContents();

		// Initialize created meta-data
		theYVisibilityPackage.initializePackageContents();
		theYECviewPackage.initializePackageContents();
		theYConverterPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theYVisibilityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(YVisibilityPackage.eNS_URI, theYVisibilityPackage);
		return theYVisibilityPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y authorization visibility processor
	 * @generated
	 */
	public EClass getYAuthorizationVisibilityProcessor() {
		return yAuthorizationVisibilityProcessorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYSubTypeVisibilityProcessor() {
		return ySubTypeVisibilityProcessorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSubTypeVisibilityProcessor_TypeQualifiedName() {
		return (EAttribute)ySubTypeVisibilityProcessorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSubTypeVisibilityProcessor_Type() {
		return (EAttribute)ySubTypeVisibilityProcessorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYSubTypeVisibilityProcessor_Target() {
		return (EReference)ySubTypeVisibilityProcessorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y visibility factory
	 * @generated
	 */
	public YVisibilityFactory getYVisibilityFactory() {
		return (YVisibilityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		yAuthorizationVisibilityProcessorEClass = createEClass(YAUTHORIZATION_VISIBILITY_PROCESSOR);

		ySubTypeVisibilityProcessorEClass = createEClass(YSUB_TYPE_VISIBILITY_PROCESSOR);
		createEAttribute(ySubTypeVisibilityProcessorEClass, YSUB_TYPE_VISIBILITY_PROCESSOR__TYPE_QUALIFIED_NAME);
		createEAttribute(ySubTypeVisibilityProcessorEClass, YSUB_TYPE_VISIBILITY_PROCESSOR__TYPE);
		createEReference(ySubTypeVisibilityProcessorEClass, YSUB_TYPE_VISIBILITY_PROCESSOR__TARGET);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		VisibilityPackage theVisibilityPackage = (VisibilityPackage)EPackage.Registry.INSTANCE.getEPackage(VisibilityPackage.eNS_URI);
		CoreModelPackage theCoreModelPackage = (CoreModelPackage)EPackage.Registry.INSTANCE.getEPackage(CoreModelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		yAuthorizationVisibilityProcessorEClass.getESuperTypes().add(theVisibilityPackage.getYVisibilityProcessor());
		ySubTypeVisibilityProcessorEClass.getESuperTypes().add(theVisibilityPackage.getYVisibilityProcessor());

		// Initialize classes and features; add operations and parameters
		initEClass(yAuthorizationVisibilityProcessorEClass, YAuthorizationVisibilityProcessor.class, "YAuthorizationVisibilityProcessor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ySubTypeVisibilityProcessorEClass, YSubTypeVisibilityProcessor.class, "YSubTypeVisibilityProcessor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYSubTypeVisibilityProcessor_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YSubTypeVisibilityProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYSubTypeVisibilityProcessor_Type(), g1, "type", null, 0, 1, YSubTypeVisibilityProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSubTypeVisibilityProcessor_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 1, 1, YSubTypeVisibilityProcessor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

}

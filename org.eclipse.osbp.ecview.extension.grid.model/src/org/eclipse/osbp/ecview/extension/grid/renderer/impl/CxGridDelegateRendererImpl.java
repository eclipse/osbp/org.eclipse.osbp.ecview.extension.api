/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;
import org.eclipse.osbp.ecview.extension.grid.util.CxGridUtil;
import org.eclipse.emf.common.notify.Notification;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Cx Grid Delegate Renderer</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridDelegateRendererImpl#getDelegateId <em>Delegate Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridDelegateRendererImpl extends CxGridRendererImpl implements
		CxGridDelegateRenderer {
	
	/**
	 * The default value of the '{@link #getDelegateId() <em>Delegate Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelegateId()
	 * @generated
	 * @ordered
	 */
	protected static final String DELEGATE_ID_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDelegateId() <em>Delegate Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelegateId()
	 * @generated
	 * @ordered
	 */
	protected String delegateId = DELEGATE_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected CxGridDelegateRendererImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridRendererPackage.Literals.CX_GRID_DELEGATE_RENDERER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getDelegateId()
	 *         <em>Delegate Id</em>}' attribute
	 * @generated
	 */
	public String getDelegateId() {
		return delegateId;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newDelegateId
	 *            the new cached value of the '{@link #getDelegateId()
	 *            <em>Delegate Id</em>}' attribute
	 * @generated
	 */
	public void setDelegateId(String newDelegateId) {
		String oldDelegateId = delegateId;
		delegateId = newDelegateId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_DELEGATE_RENDERER__DELEGATE_ID, oldDelegateId, delegateId));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_DELEGATE_RENDERER__DELEGATE_ID:
				return getDelegateId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_DELEGATE_RENDERER__DELEGATE_ID:
				setDelegateId((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_DELEGATE_RENDERER__DELEGATE_ID:
				setDelegateId(DELEGATE_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_DELEGATE_RENDERER__DELEGATE_ID:
				return DELEGATE_ID_EDEFAULT == null ? delegateId != null : !DELEGATE_ID_EDEFAULT.equals(delegateId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (delegateId: ");
		result.append(delegateId);
		result.append(')');
		return result.toString();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererImpl#getGrid()
	 */
	@Override
	public CxGrid getGrid() {
		return CxGridUtil.getGrid(this);
	}

}
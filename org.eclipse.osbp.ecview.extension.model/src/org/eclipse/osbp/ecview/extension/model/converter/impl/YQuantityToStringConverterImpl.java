/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YStringToStringMapImpl;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YQuantity To String Converter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getAmountPropertyPath <em>Amount Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getUomPropertyPath <em>Uom Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getUomCodeRelativePropertyPath <em>Uom Code Relative Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl#getQuantityTypeQualifiedName <em>Quantity Type Qualified Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YQuantityToStringConverterImpl extends MinimalEObjectImpl.Container implements YQuantityToStringConverter {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * The default value of the '{@link #getAmountPropertyPath() <em>Amount Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmountPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected static final String AMOUNT_PROPERTY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAmountPropertyPath() <em>Amount Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmountPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected String amountPropertyPath = AMOUNT_PROPERTY_PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getUomPropertyPath() <em>Uom Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUomPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected static final String UOM_PROPERTY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUomPropertyPath() <em>Uom Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUomPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected String uomPropertyPath = UOM_PROPERTY_PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getUomCodeRelativePropertyPath() <em>Uom Code Relative Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUomCodeRelativePropertyPath()
	 * @generated
	 * @ordered
	 */
	protected static final String UOM_CODE_RELATIVE_PROPERTY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUomCodeRelativePropertyPath() <em>Uom Code Relative Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUomCodeRelativePropertyPath()
	 * @generated
	 * @ordered
	 */
	protected String uomCodeRelativePropertyPath = UOM_CODE_RELATIVE_PROPERTY_PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityTypeQualifiedName() <em>Quantity Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_TYPE_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityTypeQualifiedName() <em>Quantity Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String quantityTypeQualifiedName = QUANTITY_TYPE_QUALIFIED_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YQuantityToStringConverterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YConverterPackage.Literals.YQUANTITY_TO_STRING_CONVERTER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getId() <em>Id</em>}' attribute
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newId
	 *            the new cached value of the '{@link #getId() <em>Id</em>}'
	 *            attribute
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getName() <em>Name</em>}'
	 *         attribute
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newName
	 *            the new cached value of the '{@link #getName() <em>Name</em>}'
	 *            attribute
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTags() <em>Tags</em>}'
	 *         attribute list
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getProperties()
	 *         <em>Properties</em>}' map
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP, YStringToStringMapImpl.class, this, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getAmountPropertyPath()
	 *         <em>Amount Property Path</em>}' attribute
	 * @generated
	 */
	public String getAmountPropertyPath() {
		return amountPropertyPath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newAmountPropertyPath
	 *            the new cached value of the '{@link #getAmountPropertyPath()
	 *            <em>Amount Property Path</em>}' attribute
	 * @generated
	 */
	public void setAmountPropertyPath(String newAmountPropertyPath) {
		String oldAmountPropertyPath = amountPropertyPath;
		amountPropertyPath = newAmountPropertyPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH, oldAmountPropertyPath, amountPropertyPath));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getUomPropertyPath()
	 *         <em>Uom Property Path</em>}' attribute
	 * @generated
	 */
	public String getUomPropertyPath() {
		return uomPropertyPath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newUomPropertyPath
	 *            the new cached value of the '{@link #getUomPropertyPath()
	 *            <em>Uom Property Path</em>}' attribute
	 * @generated
	 */
	public void setUomPropertyPath(String newUomPropertyPath) {
		String oldUomPropertyPath = uomPropertyPath;
		uomPropertyPath = newUomPropertyPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH, oldUomPropertyPath, uomPropertyPath));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '
	 *         {@link #getUomCodeRelativePropertyPath()
	 *         <em>Uom Code Relative Property Path</em>}' attribute
	 * @generated
	 */
	public String getUomCodeRelativePropertyPath() {
		return uomCodeRelativePropertyPath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newUomCodeRelativePropertyPath
	 *            the new cached value of the '
	 *            {@link #getUomCodeRelativePropertyPath()
	 *            <em>Uom Code Relative Property Path</em>}' attribute
	 * @generated
	 */
	public void setUomCodeRelativePropertyPath(String newUomCodeRelativePropertyPath) {
		String oldUomCodeRelativePropertyPath = uomCodeRelativePropertyPath;
		uomCodeRelativePropertyPath = newUomCodeRelativePropertyPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH, oldUomCodeRelativePropertyPath, uomCodeRelativePropertyPath));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getQuantityTypeQualifiedName()
	 *         <em>Quantity Type Qualified Name</em>}' attribute
	 * @generated
	 */
	public String getQuantityTypeQualifiedName() {
		return quantityTypeQualifiedName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newQuantityTypeQualifiedName
	 *            the new cached value of the '
	 *            {@link #getQuantityTypeQualifiedName()
	 *            <em>Quantity Type Qualified Name</em>}' attribute
	 * @generated
	 */
	public void setQuantityTypeQualifiedName(String newQuantityTypeQualifiedName) {
		String oldQuantityTypeQualifiedName = quantityTypeQualifiedName;
		quantityTypeQualifiedName = newQuantityTypeQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME, oldQuantityTypeQualifiedName, quantityTypeQualifiedName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__TAGS:
				return getTags();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__ID:
				return getId();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__NAME:
				return getName();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH:
				return getAmountPropertyPath();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH:
				return getUomPropertyPath();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH:
				return getUomCodeRelativePropertyPath();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME:
				return getQuantityTypeQualifiedName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__ID:
				setId((String)newValue);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__NAME:
				setName((String)newValue);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH:
				setAmountPropertyPath((String)newValue);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH:
				setUomPropertyPath((String)newValue);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH:
				setUomCodeRelativePropertyPath((String)newValue);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME:
				setQuantityTypeQualifiedName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__TAGS:
				getTags().clear();
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__ID:
				setId(ID_EDEFAULT);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__PROPERTIES:
				getProperties().clear();
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH:
				setAmountPropertyPath(AMOUNT_PROPERTY_PATH_EDEFAULT);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH:
				setUomPropertyPath(UOM_PROPERTY_PATH_EDEFAULT);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH:
				setUomCodeRelativePropertyPath(UOM_CODE_RELATIVE_PROPERTY_PATH_EDEFAULT);
				return;
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME:
				setQuantityTypeQualifiedName(QUANTITY_TYPE_QUALIFIED_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__TAGS:
				return tags != null && !tags.isEmpty();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH:
				return AMOUNT_PROPERTY_PATH_EDEFAULT == null ? amountPropertyPath != null : !AMOUNT_PROPERTY_PATH_EDEFAULT.equals(amountPropertyPath);
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH:
				return UOM_PROPERTY_PATH_EDEFAULT == null ? uomPropertyPath != null : !UOM_PROPERTY_PATH_EDEFAULT.equals(uomPropertyPath);
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH:
				return UOM_CODE_RELATIVE_PROPERTY_PATH_EDEFAULT == null ? uomCodeRelativePropertyPath != null : !UOM_CODE_RELATIVE_PROPERTY_PATH_EDEFAULT.equals(uomCodeRelativePropertyPath);
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME:
				return QUANTITY_TYPE_QUALIFIED_NAME_EDEFAULT == null ? quantityTypeQualifiedName != null : !QUANTITY_TYPE_QUALIFIED_NAME_EDEFAULT.equals(quantityTypeQualifiedName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", amountPropertyPath: ");
		result.append(amountPropertyPath);
		result.append(", uomPropertyPath: ");
		result.append(uomPropertyPath);
		result.append(", uomCodeRelativePropertyPath: ");
		result.append(uomCodeRelativePropertyPath);
		result.append(", quantityTypeQualifiedName: ");
		result.append(quantityTypeQualifiedName);
		result.append(')');
		return result.toString();
	}

}

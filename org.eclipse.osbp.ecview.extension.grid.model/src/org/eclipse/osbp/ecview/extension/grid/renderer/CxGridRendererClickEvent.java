/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Click Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getRenderer <em>Renderer</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getLastClickTime <em>Last Click Time</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridRendererClickEvent()
 * @model
 * @generated
 */
public interface CxGridRendererClickEvent extends EObject {
	
	/**
	 * Returns the value of the '<em><b>Renderer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Renderer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Renderer</em>' reference.
	 * @see #setRenderer(CxGridRenderer)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridRendererClickEvent_Renderer()
	 * @model required="true"
	 * @generated
	 */
	CxGridRenderer getRenderer();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getRenderer <em>Renderer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Renderer</em>' reference.
	 * @see #getRenderer()
	 * @generated
	 */
	void setRenderer(CxGridRenderer value);

	/**
	 * Returns the value of the '<em><b>Last Click Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Click Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Click Time</em>' attribute.
	 * @see #setLastClickTime(long)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridRendererClickEvent_LastClickTime()
	 * @model
	 * @generated
	 */
	long getLastClickTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent#getLastClickTime <em>Last Click Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Click Time</em>' attribute.
	 * @see #getLastClickTime()
	 * @generated
	 */
	void setLastClickTime(long value);

}
/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YSuspect Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getSuspect <em>Suspect</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getBindings <em>Bindings</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getNextFocus <em>Next Focus</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getPreviousFocus <em>Previous Focus</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getVisibilityProcessors <em>Visibility Processors</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspectInfo()
 * @model
 * @generated
 */
public interface YSuspectInfo extends YElement {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Suspect</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suspect</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suspect</em>' reference.
	 * @see #setSuspect(YSuspect)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspectInfo_Suspect()
	 * @model
	 * @generated
	 */
	YSuspect getSuspect();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getSuspect <em>Suspect</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suspect</em>' reference.
	 * @see #getSuspect()
	 * @generated
	 */
	void setSuspect(YSuspect value);

	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspectInfo_Bindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<YBinding> getBindings();

	/**
	 * Returns the value of the '<em><b>Next Focus</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getPreviousFocus <em>Previous Focus</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next Focus</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Focus</em>' reference.
	 * @see #setNextFocus(YSuspectInfo)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspectInfo_NextFocus()
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getPreviousFocus
	 * @model opposite="previousFocus"
	 * @generated
	 */
	YSuspectInfo getNextFocus();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getNextFocus <em>Next Focus</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Focus</em>' reference.
	 * @see #getNextFocus()
	 * @generated
	 */
	void setNextFocus(YSuspectInfo value);

	/**
	 * Returns the value of the '<em><b>Previous Focus</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getNextFocus <em>Next Focus</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous Focus</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Focus</em>' reference.
	 * @see #setPreviousFocus(YSuspectInfo)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspectInfo_PreviousFocus()
	 * @see org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getNextFocus
	 * @model opposite="nextFocus"
	 * @generated
	 */
	YSuspectInfo getPreviousFocus();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getPreviousFocus <em>Previous Focus</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Previous Focus</em>' reference.
	 * @see #getPreviousFocus()
	 * @generated
	 */
	void setPreviousFocus(YSuspectInfo value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(YEmbeddable)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspectInfo_Target()
	 * @model
	 * @generated
	 */
	YEmbeddable getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(YEmbeddable value);

	/**
	 * Returns the value of the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility Processors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility Processors</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspectInfo_VisibilityProcessors()
	 * @model containment="true"
	 * @generated
	 */
	EList<YVisibilityProcessor> getVisibilityProcessors();

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	YView getView();

}

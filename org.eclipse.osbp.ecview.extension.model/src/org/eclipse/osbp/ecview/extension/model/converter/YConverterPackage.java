/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory
 * @model kind="package"
 * @generated
 */
public interface YConverterPackage extends EPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "converter";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.de/ecview/v1/extension/converter";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "converter";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	YConverterPackage eINSTANCE = org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YObjectToStringConverterImpl <em>YObject To String Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YObjectToStringConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYObjectToStringConverter()
	 * @generated
	 */
	int YOBJECT_TO_STRING_CONVERTER = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YOBJECT_TO_STRING_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YOBJECT_TO_STRING_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YOBJECT_TO_STRING_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YOBJECT_TO_STRING_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The number of structural features of the '<em>YObject To String Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YOBJECT_TO_STRING_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToByteArrayConverterImpl <em>YString To Byte Array Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToByteArrayConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYStringToByteArrayConverter()
	 * @generated
	 */
	int YSTRING_TO_BYTE_ARRAY_CONVERTER = 1;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_BYTE_ARRAY_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_BYTE_ARRAY_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_BYTE_ARRAY_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_BYTE_ARRAY_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The number of structural features of the '<em>YString To Byte Array Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_BYTE_ARRAY_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YCustomDecimalConverterImpl <em>YCustom Decimal Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YCustomDecimalConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYCustomDecimalConverter()
	 * @generated
	 */
	int YCUSTOM_DECIMAL_CONVERTER = 2;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Base Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_CONVERTER__BASE_UNIT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YCustom Decimal Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YCUSTOM_DECIMAL_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConverterImpl <em>YNumeric To Resource Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYNumericToResourceConverter()
	 * @generated
	 */
	int YNUMERIC_TO_RESOURCE_CONVERTER = 3;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Configs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONVERTER__CONFIGS = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YNumeric To Resource Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConverterImpl <em>YString To Resource Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYStringToResourceConverter()
	 * @generated
	 */
	int YSTRING_TO_RESOURCE_CONVERTER = 4;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Configs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONVERTER__CONFIGS = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YString To Resource Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConfigImpl <em>YNumeric To Resource Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConfigImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYNumericToResourceConfig()
	 * @generated
	 */
	int YNUMERIC_TO_RESOURCE_CONFIG = 5;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONFIG__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Compare</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONFIG__COMPARE = 1;

	/**
	 * The feature id for the '<em><b>Resource Theme Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH = 2;

	/**
	 * The number of structural features of the '<em>YNumeric To Resource Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_RESOURCE_CONFIG_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConfigImpl <em>YString To Resource Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConfigImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYStringToResourceConfig()
	 * @generated
	 */
	int YSTRING_TO_RESOURCE_CONFIG = 6;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONFIG__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Compare</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONFIG__COMPARE = 1;

	/**
	 * The feature id for the '<em><b>Resource Theme Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH = 2;

	/**
	 * The number of structural features of the '<em>YString To Resource Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSTRING_TO_RESOURCE_CONFIG_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToUomoConverterImpl <em>YNumeric To Uomo Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToUomoConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYNumericToUomoConverter()
	 * @generated
	 */
	int YNUMERIC_TO_UOMO_CONVERTER = 9;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl <em>YPrice To String Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYPriceToStringConverter()
	 * @generated
	 */
	int YPRICE_TO_STRING_CONVERTER = 7;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Value Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Currency Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER__TYPE = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>YPrice To String Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YPRICE_TO_STRING_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl <em>YQuantity To String Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYQuantityToStringConverter()
	 * @generated
	 */
	int YQUANTITY_TO_STRING_CONVERTER = 8;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Amount Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uom Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Uom Code Relative Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Quantity Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>YQuantity To String Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YQUANTITY_TO_STRING_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_UOMO_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_UOMO_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_UOMO_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_UOMO_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The number of structural features of the '<em>YNumeric To Uomo Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YNUMERIC_TO_UOMO_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YDecimalToUomoConverterImpl <em>YDecimal To Uomo Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YDecimalToUomoConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYDecimalToUomoConverter()
	 * @generated
	 */
	int YDECIMAL_TO_UOMO_CONVERTER = 10;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDECIMAL_TO_UOMO_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDECIMAL_TO_UOMO_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDECIMAL_TO_UOMO_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDECIMAL_TO_UOMO_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The number of structural features of the '<em>YDecimal To Uomo Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YDECIMAL_TO_UOMO_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YSimpleDecimalConverterImpl <em>YSimple Decimal Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YSimpleDecimalConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYSimpleDecimalConverter()
	 * @generated
	 */
	int YSIMPLE_DECIMAL_CONVERTER = 11;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSIMPLE_DECIMAL_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSIMPLE_DECIMAL_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSIMPLE_DECIMAL_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSIMPLE_DECIMAL_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Number Format Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSIMPLE_DECIMAL_CONVERTER__NUMBER_FORMAT_PATTERN = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>YSimple Decimal Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSIMPLE_DECIMAL_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YVaaclipseUiThemeToStringConverterImpl <em>YVaaclipse Ui Theme To String Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YVaaclipseUiThemeToStringConverterImpl
	 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYVaaclipseUiThemeToStringConverter()
	 * @generated
	 */
	int YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER = 12;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER__TAGS = CoreModelPackage.YCONVERTER__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER__ID = CoreModelPackage.YCONVERTER__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER__NAME = CoreModelPackage.YCONVERTER__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER__PROPERTIES = CoreModelPackage.YCONVERTER__PROPERTIES;

	/**
	 * The number of structural features of the '<em>YVaaclipse Ui Theme To String Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER_FEATURE_COUNT = CoreModelPackage.YCONVERTER_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YObjectToStringConverter <em>YObject To String Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YObject To String Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YObjectToStringConverter
	 * @generated
	 */
	EClass getYObjectToStringConverter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YStringToByteArrayConverter <em>YString To Byte Array Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YString To Byte Array Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YStringToByteArrayConverter
	 * @generated
	 */
	EClass getYStringToByteArrayConverter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter <em>YCustom Decimal Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YCustom Decimal Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter
	 * @generated
	 */
	EClass getYCustomDecimalConverter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter#getBaseUnit <em>Base Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Unit</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter#getBaseUnit()
	 * @see #getYCustomDecimalConverter()
	 * @generated
	 */
	EAttribute getYCustomDecimalConverter_BaseUnit();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter <em>YNumeric To Resource Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YNumeric To Resource Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter
	 * @generated
	 */
	EClass getYNumericToResourceConverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter#getConfigs <em>Configs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configs</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter#getConfigs()
	 * @see #getYNumericToResourceConverter()
	 * @generated
	 */
	EReference getYNumericToResourceConverter_Configs();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter <em>YString To Resource Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YString To Resource Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter
	 * @generated
	 */
	EClass getYStringToResourceConverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter#getConfigs <em>Configs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configs</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter#getConfigs()
	 * @see #getYStringToResourceConverter()
	 * @generated
	 */
	EReference getYStringToResourceConverter_Configs();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig <em>YNumeric To Resource Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YNumeric To Resource Config</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig
	 * @generated
	 */
	EClass getYNumericToResourceConfig();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getValue()
	 * @see #getYNumericToResourceConfig()
	 * @generated
	 */
	EAttribute getYNumericToResourceConfig_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getCompare <em>Compare</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Compare</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getCompare()
	 * @see #getYNumericToResourceConfig()
	 * @generated
	 */
	EAttribute getYNumericToResourceConfig_Compare();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getResourceThemePath <em>Resource Theme Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resource Theme Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig#getResourceThemePath()
	 * @see #getYNumericToResourceConfig()
	 * @generated
	 */
	EAttribute getYNumericToResourceConfig_ResourceThemePath();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig <em>YString To Resource Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YString To Resource Config</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig
	 * @generated
	 */
	EClass getYStringToResourceConfig();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig#getValue()
	 * @see #getYStringToResourceConfig()
	 * @generated
	 */
	EAttribute getYStringToResourceConfig_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig#getCompare <em>Compare</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Compare</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig#getCompare()
	 * @see #getYStringToResourceConfig()
	 * @generated
	 */
	EAttribute getYStringToResourceConfig_Compare();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig#getResourceThemePath <em>Resource Theme Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resource Theme Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig#getResourceThemePath()
	 * @see #getYStringToResourceConfig()
	 * @generated
	 */
	EAttribute getYStringToResourceConfig_ResourceThemePath();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter <em>YNumeric To Uomo Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YNumeric To Uomo Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter
	 * @generated
	 */
	EClass getYNumericToUomoConverter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter <em>YDecimal To Uomo Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YDecimal To Uomo Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter
	 * @generated
	 */
	EClass getYDecimalToUomoConverter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter <em>YSimple Decimal Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YSimple Decimal Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter
	 * @generated
	 */
	EClass getYSimpleDecimalConverter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter#getNumberFormatPattern <em>Number Format Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Format Pattern</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter#getNumberFormatPattern()
	 * @see #getYSimpleDecimalConverter()
	 * @generated
	 */
	EAttribute getYSimpleDecimalConverter_NumberFormatPattern();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YVaaclipseUiThemeToStringConverter <em>YVaaclipse Ui Theme To String Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YVaaclipse Ui Theme To String Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YVaaclipseUiThemeToStringConverter
	 * @generated
	 */
	EClass getYVaaclipseUiThemeToStringConverter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter <em>YPrice To String Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YPrice To String Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter
	 * @generated
	 */
	EClass getYPriceToStringConverter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getValuePropertyPath <em>Value Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getValuePropertyPath()
	 * @see #getYPriceToStringConverter()
	 * @generated
	 */
	EAttribute getYPriceToStringConverter_ValuePropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getCurrencyPropertyPath <em>Currency Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Currency Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getCurrencyPropertyPath()
	 * @see #getYPriceToStringConverter()
	 * @generated
	 */
	EAttribute getYPriceToStringConverter_CurrencyPropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getTypeQualifiedName()
	 * @see #getYPriceToStringConverter()
	 * @generated
	 */
	EAttribute getYPriceToStringConverter_TypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getType()
	 * @see #getYPriceToStringConverter()
	 * @generated
	 */
	EAttribute getYPriceToStringConverter_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter <em>YQuantity To String Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YQuantity To String Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter
	 * @generated
	 */
	EClass getYQuantityToStringConverter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getAmountPropertyPath <em>Amount Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Amount Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getAmountPropertyPath()
	 * @see #getYQuantityToStringConverter()
	 * @generated
	 */
	EAttribute getYQuantityToStringConverter_AmountPropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomPropertyPath <em>Uom Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uom Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomPropertyPath()
	 * @see #getYQuantityToStringConverter()
	 * @generated
	 */
	EAttribute getYQuantityToStringConverter_UomPropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomCodeRelativePropertyPath <em>Uom Code Relative Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uom Code Relative Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomCodeRelativePropertyPath()
	 * @see #getYQuantityToStringConverter()
	 * @generated
	 */
	EAttribute getYQuantityToStringConverter_UomCodeRelativePropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getQuantityTypeQualifiedName <em>Quantity Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getQuantityTypeQualifiedName()
	 * @see #getYQuantityToStringConverter()
	 * @generated
	 */
	EAttribute getYQuantityToStringConverter_QuantityTypeQualifiedName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	YConverterFactory getYConverterFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YObjectToStringConverterImpl <em>YObject To String Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YObjectToStringConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYObjectToStringConverter()
		 * @generated
		 */
		EClass YOBJECT_TO_STRING_CONVERTER = eINSTANCE.getYObjectToStringConverter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToByteArrayConverterImpl <em>YString To Byte Array Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToByteArrayConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYStringToByteArrayConverter()
		 * @generated
		 */
		EClass YSTRING_TO_BYTE_ARRAY_CONVERTER = eINSTANCE.getYStringToByteArrayConverter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YCustomDecimalConverterImpl <em>YCustom Decimal Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YCustomDecimalConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYCustomDecimalConverter()
		 * @generated
		 */
		EClass YCUSTOM_DECIMAL_CONVERTER = eINSTANCE.getYCustomDecimalConverter();

		/**
		 * The meta object literal for the '<em><b>Base Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YCUSTOM_DECIMAL_CONVERTER__BASE_UNIT = eINSTANCE.getYCustomDecimalConverter_BaseUnit();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConverterImpl <em>YNumeric To Resource Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYNumericToResourceConverter()
		 * @generated
		 */
		EClass YNUMERIC_TO_RESOURCE_CONVERTER = eINSTANCE.getYNumericToResourceConverter();

		/**
		 * The meta object literal for the '<em><b>Configs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YNUMERIC_TO_RESOURCE_CONVERTER__CONFIGS = eINSTANCE.getYNumericToResourceConverter_Configs();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConverterImpl <em>YString To Resource Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYStringToResourceConverter()
		 * @generated
		 */
		EClass YSTRING_TO_RESOURCE_CONVERTER = eINSTANCE.getYStringToResourceConverter();

		/**
		 * The meta object literal for the '<em><b>Configs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSTRING_TO_RESOURCE_CONVERTER__CONFIGS = eINSTANCE.getYStringToResourceConverter_Configs();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConfigImpl <em>YNumeric To Resource Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConfigImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYNumericToResourceConfig()
		 * @generated
		 */
		EClass YNUMERIC_TO_RESOURCE_CONFIG = eINSTANCE.getYNumericToResourceConfig();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YNUMERIC_TO_RESOURCE_CONFIG__VALUE = eINSTANCE.getYNumericToResourceConfig_Value();

		/**
		 * The meta object literal for the '<em><b>Compare</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YNUMERIC_TO_RESOURCE_CONFIG__COMPARE = eINSTANCE.getYNumericToResourceConfig_Compare();

		/**
		 * The meta object literal for the '<em><b>Resource Theme Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH = eINSTANCE.getYNumericToResourceConfig_ResourceThemePath();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConfigImpl <em>YString To Resource Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YStringToResourceConfigImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYStringToResourceConfig()
		 * @generated
		 */
		EClass YSTRING_TO_RESOURCE_CONFIG = eINSTANCE.getYStringToResourceConfig();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSTRING_TO_RESOURCE_CONFIG__VALUE = eINSTANCE.getYStringToResourceConfig_Value();

		/**
		 * The meta object literal for the '<em><b>Compare</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSTRING_TO_RESOURCE_CONFIG__COMPARE = eINSTANCE.getYStringToResourceConfig_Compare();

		/**
		 * The meta object literal for the '<em><b>Resource Theme Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSTRING_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH = eINSTANCE.getYStringToResourceConfig_ResourceThemePath();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToUomoConverterImpl <em>YNumeric To Uomo Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToUomoConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYNumericToUomoConverter()
		 * @generated
		 */
		EClass YNUMERIC_TO_UOMO_CONVERTER = eINSTANCE.getYNumericToUomoConverter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YDecimalToUomoConverterImpl <em>YDecimal To Uomo Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YDecimalToUomoConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYDecimalToUomoConverter()
		 * @generated
		 */
		EClass YDECIMAL_TO_UOMO_CONVERTER = eINSTANCE.getYDecimalToUomoConverter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YSimpleDecimalConverterImpl <em>YSimple Decimal Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YSimpleDecimalConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYSimpleDecimalConverter()
		 * @generated
		 */
		EClass YSIMPLE_DECIMAL_CONVERTER = eINSTANCE.getYSimpleDecimalConverter();

		/**
		 * The meta object literal for the '<em><b>Number Format Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSIMPLE_DECIMAL_CONVERTER__NUMBER_FORMAT_PATTERN = eINSTANCE.getYSimpleDecimalConverter_NumberFormatPattern();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YVaaclipseUiThemeToStringConverterImpl <em>YVaaclipse Ui Theme To String Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YVaaclipseUiThemeToStringConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYVaaclipseUiThemeToStringConverter()
		 * @generated
		 */
		EClass YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER = eINSTANCE.getYVaaclipseUiThemeToStringConverter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl <em>YPrice To String Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYPriceToStringConverter()
		 * @generated
		 */
		EClass YPRICE_TO_STRING_CONVERTER = eINSTANCE.getYPriceToStringConverter();

		/**
		 * The meta object literal for the '<em><b>Value Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH = eINSTANCE.getYPriceToStringConverter_ValuePropertyPath();

		/**
		 * The meta object literal for the '<em><b>Currency Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH = eINSTANCE.getYPriceToStringConverter_CurrencyPropertyPath();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME = eINSTANCE.getYPriceToStringConverter_TypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YPRICE_TO_STRING_CONVERTER__TYPE = eINSTANCE.getYPriceToStringConverter_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl <em>YQuantity To String Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YQuantityToStringConverterImpl
		 * @see org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterPackageImpl#getYQuantityToStringConverter()
		 * @generated
		 */
		EClass YQUANTITY_TO_STRING_CONVERTER = eINSTANCE.getYQuantityToStringConverter();

		/**
		 * The meta object literal for the '<em><b>Amount Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH = eINSTANCE.getYQuantityToStringConverter_AmountPropertyPath();

		/**
		 * The meta object literal for the '<em><b>Uom Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH = eINSTANCE.getYQuantityToStringConverter_UomPropertyPath();

		/**
		 * The meta object literal for the '<em><b>Uom Code Relative Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH = eINSTANCE.getYQuantityToStringConverter_UomCodeRelativePropertyPath();

		/**
		 * The meta object literal for the '<em><b>Quantity Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME = eINSTANCE.getYQuantityToStringConverter_QuantityTypeQualifiedName();

	}

}

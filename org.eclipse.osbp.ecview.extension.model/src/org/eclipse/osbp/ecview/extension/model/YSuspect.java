/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable;
import org.eclipse.osbp.ecview.core.common.model.core.YCommand;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YSuspect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getImageI18nKey <em>Image I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getValueBindingEndpoints <em>Value Binding Endpoints</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getVisibilityProcessors <em>Visibility Processors</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getCommands <em>Commands</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getAssocNewiatedElements <em>Assoc Newiated Elements</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getAssociatedBindings <em>Associated Bindings</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getContainerValueBindingEndpoint <em>Container Value Binding Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getGroupName <em>Group Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getStyleName <em>Style Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect()
 * @model
 * @generated
 */
public interface YSuspect extends YElement, YAuthorizationable {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #setLabelI18nKey(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_LabelI18nKey()
	 * @model
	 * @generated
	 */
	String getLabelI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getLabelI18nKey <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #getLabelI18nKey()
	 * @generated
	 */
	void setLabelI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Image I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image I1 8n Key</em>' attribute.
	 * @see #setImageI18nKey(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_ImageI18nKey()
	 * @model
	 * @generated
	 */
	String getImageI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getImageI18nKey <em>Image I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image I1 8n Key</em>' attribute.
	 * @see #getImageI18nKey()
	 * @generated
	 */
	void setImageI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Value Binding Endpoints</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Binding Endpoints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Binding Endpoints</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_ValueBindingEndpoints()
	 * @model containment="true"
	 * @generated
	 */
	EList<YBindingEndpoint> getValueBindingEndpoints();

	/**
	 * Returns the value of the '<em><b>Visibility Processors</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility Processors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility Processors</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_VisibilityProcessors()
	 * @model containment="true"
	 * @generated
	 */
	EList<YVisibilityProcessor> getVisibilityProcessors();

	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.core.YCommand}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_Commands()
	 * @model containment="true"
	 * @generated
	 */
	EList<YCommand> getCommands();

	/**
	 * Returns the value of the '<em><b>Assoc Newiated Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assoc Newiated Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assoc Newiated Elements</em>' reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_AssocNewiatedElements()
	 * @model transient="true"
	 * @generated
	 */
	EList<YEmbeddable> getAssocNewiatedElements();

	/**
	 * Returns the value of the '<em><b>Associated Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Bindings</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_AssociatedBindings()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	EList<YBinding> getAssociatedBindings();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);
	
	/**
	 * Returns the value of the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Value Binding Endpoint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Value Binding Endpoint</em>' containment reference.
	 * @see #setContainerValueBindingEndpoint(YValueBindingEndpoint)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_ContainerValueBindingEndpoint()
	 * @model containment="true"
	 * @generated
	 */
	YValueBindingEndpoint getContainerValueBindingEndpoint();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getContainerValueBindingEndpoint <em>Container Value Binding Endpoint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Value Binding Endpoint</em>' containment reference.
	 * @see #getContainerValueBindingEndpoint()
	 * @generated
	 */
	void setContainerValueBindingEndpoint(YValueBindingEndpoint value);

	/**
	 * Returns the value of the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Name</em>' attribute.
	 * @see #setGroupName(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_GroupName()
	 * @model
	 * @generated
	 */
	String getGroupName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getGroupName <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Name</em>' attribute.
	 * @see #getGroupName()
	 * @generated
	 */
	void setGroupName(String value);

	/**
	 * Returns the value of the '<em><b>Style Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Style Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Style Name</em>' attribute.
	 * @see #setStyleName(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSuspect_StyleName()
	 * @model
	 * @generated
	 */
	String getStyleName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSuspect#getStyleName <em>Style Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Style Name</em>' attribute.
	 * @see #getStyleName()
	 * @generated
	 */
	void setStyleName(String value);

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	YView getView();

}

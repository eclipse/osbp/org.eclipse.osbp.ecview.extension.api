/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;


import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;

import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityFactory;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.model.YSuspectInfo} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YSuspectInfoItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	
	/**
<<<<<<< HEAD
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
=======
>>>>>>> branch 'development' of ssh://lunifera@80.156.28.28/osbpgit/org.eclipse.osbp.ecview.extension.git
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public YSuspectInfoItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addSuspectPropertyDescriptor(object);
			addNextFocusPropertyDescriptor(object);
			addPreviousFocusPropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_id_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_name_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YTaggable_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YTaggable_tags_feature", "_UI_YTaggable_type"),
				 CoreModelPackage.Literals.YTAGGABLE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Suspect feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addSuspectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspectInfo_suspect_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspectInfo_suspect_feature", "_UI_YSuspectInfo_type"),
				 YECviewPackage.Literals.YSUSPECT_INFO__SUSPECT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Next Focus feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addNextFocusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspectInfo_nextFocus_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspectInfo_nextFocus_feature", "_UI_YSuspectInfo_type"),
				 YECviewPackage.Literals.YSUSPECT_INFO__NEXT_FOCUS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Previous Focus feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addPreviousFocusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspectInfo_previousFocus_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspectInfo_previousFocus_feature", "_UI_YSuspectInfo_type"),
				 YECviewPackage.Literals.YSUSPECT_INFO__PREVIOUS_FOCUS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuspectInfo_target_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuspectInfo_target_feature", "_UI_YSuspectInfo_type"),
				 YECviewPackage.Literals.YSUSPECT_INFO__TARGET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YELEMENT__PROPERTIES);
			childrenFeatures.add(YECviewPackage.Literals.YSUSPECT_INFO__BINDINGS);
			childrenFeatures.add(YECviewPackage.Literals.YSUSPECT_INFO__VISIBILITY_PROCESSORS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param object
	 *            the object
	 * @param child
	 *            the child
	 * @return the child feature
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YSuspectInfo.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 *
	 * @param object
	 *            the object
	 * @return the image
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YSuspectInfo"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YSuspectInfo)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YSuspectInfo_type") :
			getString("_UI_YSuspectInfo_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YSuspectInfo.class)) {
			case YECviewPackage.YSUSPECT_INFO__TAGS:
			case YECviewPackage.YSUSPECT_INFO__ID:
			case YECviewPackage.YSUSPECT_INFO__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case YECviewPackage.YSUSPECT_INFO__PROPERTIES:
			case YECviewPackage.YSUSPECT_INFO__BINDINGS:
			case YECviewPackage.YSUSPECT_INFO__VISIBILITY_PROCESSORS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YELEMENT__PROPERTIES,
				 CoreModelFactory.eINSTANCE.create(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT_INFO__BINDINGS,
				 BindingFactory.eINSTANCE.createYValueBinding()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT_INFO__BINDINGS,
				 BindingFactory.eINSTANCE.createYListBinding()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT_INFO__VISIBILITY_PROCESSORS,
				 YVisibilityFactory.eINSTANCE.createYAuthorizationVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT_INFO__VISIBILITY_PROCESSORS,
				 YVisibilityFactory.eINSTANCE.createYSubTypeVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT_INFO__VISIBILITY_PROCESSORS,
				 VisibilityFactory.eINSTANCE.createYVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YSUSPECT_INFO__VISIBILITY_PROCESSORS,
				 ExtensionModelFactory.eINSTANCE.createYKanbanVisibilityProcessor()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the resource locator
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}

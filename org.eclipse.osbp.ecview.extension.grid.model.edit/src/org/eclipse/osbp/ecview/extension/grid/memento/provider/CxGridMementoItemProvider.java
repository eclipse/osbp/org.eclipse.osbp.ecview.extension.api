/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.memento.provider;


import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoFactory;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage;

import org.eclipse.osbp.ecview.extension.grid.provider.CxGridEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMemento} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridMementoItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public CxGridMementoItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addGridIdPropertyDescriptor(object);
			addHeaderVisiblePropertyDescriptor(object);
			addFilterVisiblePropertyDescriptor(object);
			addFooterVisiblePropertyDescriptor(object);
			addEditorEnabledPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_id_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_name_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YTaggable_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YTaggable_tags_feature", "_UI_YTaggable_type"),
				 CoreModelPackage.Literals.YTAGGABLE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Grid Id feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addGridIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMemento_gridId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMemento_gridId_feature", "_UI_CxGridMemento_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO__GRID_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Header Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHeaderVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMemento_headerVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMemento_headerVisible_feature", "_UI_CxGridMemento_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO__HEADER_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Filter Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addFilterVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMemento_filterVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMemento_filterVisible_feature", "_UI_CxGridMemento_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO__FILTER_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Footer Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addFooterVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMemento_footerVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMemento_footerVisible_feature", "_UI_CxGridMemento_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO__FOOTER_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editor Enabled feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addEditorEnabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridMemento_editorEnabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridMemento_editorEnabled_feature", "_UI_CxGridMemento_type"),
				 CxGridMementoPackage.Literals.CX_GRID_MEMENTO__EDITOR_ENABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YELEMENT__PROPERTIES);
			childrenFeatures.add(CxGridMementoPackage.Literals.CX_GRID_MEMENTO__COLUMNS);
			childrenFeatures.add(CxGridMementoPackage.Literals.CX_GRID_MEMENTO__SORT_ORDERS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param object
	 *            the object
	 * @param child
	 *            the child
	 * @return the child feature
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CxGridMemento.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 *
	 * @param object
	 *            the object
	 * @return the image
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CxGridMemento"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CxGridMemento)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_CxGridMemento_type") :
			getString("_UI_CxGridMemento_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CxGridMemento.class)) {
			case CxGridMementoPackage.CX_GRID_MEMENTO__TAGS:
			case CxGridMementoPackage.CX_GRID_MEMENTO__ID:
			case CxGridMementoPackage.CX_GRID_MEMENTO__NAME:
			case CxGridMementoPackage.CX_GRID_MEMENTO__GRID_ID:
			case CxGridMementoPackage.CX_GRID_MEMENTO__HEADER_VISIBLE:
			case CxGridMementoPackage.CX_GRID_MEMENTO__FILTER_VISIBLE:
			case CxGridMementoPackage.CX_GRID_MEMENTO__FOOTER_VISIBLE:
			case CxGridMementoPackage.CX_GRID_MEMENTO__EDITOR_ENABLED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO__PROPERTIES:
			case CxGridMementoPackage.CX_GRID_MEMENTO__COLUMNS:
			case CxGridMementoPackage.CX_GRID_MEMENTO__SORT_ORDERS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YELEMENT__PROPERTIES,
				 CoreModelFactory.eINSTANCE.create(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(CxGridMementoPackage.Literals.CX_GRID_MEMENTO__COLUMNS,
				 CxGridMementoFactory.eINSTANCE.createCxGridMementoColumn()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridMementoPackage.Literals.CX_GRID_MEMENTO__SORT_ORDERS,
				 CxGridMementoFactory.eINSTANCE.createCxGridMementoSortable()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the resource locator
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CxGridEditPlugin.INSTANCE;
	}

}

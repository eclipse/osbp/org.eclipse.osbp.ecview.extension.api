/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid;

import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Cell</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#isUseHTML <em>Use HTML</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getElement <em>Element</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaCell()
 * @model
 * @generated
 */
public interface CxGridMetaCell extends YElement, CxGridProvider {
	
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getUsedInMetaCells <em>Used In Meta Cells</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(CxGridColumn)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaCell_Target()
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getUsedInMetaCells
	 * @model opposite="usedInMetaCells"
	 * @generated
	 */
	CxGridColumn getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(CxGridColumn value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaCell_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #setLabelI18nKey(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaCell_LabelI18nKey()
	 * @model
	 * @generated
	 */
	String getLabelI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabelI18nKey <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #getLabelI18nKey()
	 * @generated
	 */
	void setLabelI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Use HTML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use HTML</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use HTML</em>' attribute.
	 * @see #setUseHTML(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaCell_UseHTML()
	 * @model
	 * @generated
	 */
	boolean isUseHTML();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#isUseHTML <em>Use HTML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use HTML</em>' attribute.
	 * @see #isUseHTML()
	 * @generated
	 */
	void setUseHTML(boolean value);

	/**
	 * Returns the value of the '<em><b>Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' containment reference.
	 * @see #setElement(YEmbeddable)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridMetaCell_Element()
	 * @model containment="true"
	 * @generated
	 */
	YEmbeddable getElement();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getElement <em>Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' containment reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(YEmbeddable value);

}
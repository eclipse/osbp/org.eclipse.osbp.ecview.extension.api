/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.api;

import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;
import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeDefinition;

import com.vaadin.ui.UI;

import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;

/**
 * Is used to set the focus to a field.
 */
public interface IFocusingStrategy {

	/**
	 * Calculates the next focused field and sets the focus. The passed source
	 * and target parameter are the UI-Kit specific UI-elements that are
	 * involved in the key stroke event. To support hybrid applications based on
	 * ECView and native UI-Code, the original objects are passed.
	 * <p>
	 * Use {@link IWidgetAssocationsService} to map the given elements to their
	 * model elements.
	 *
	 * @param source
	 *            - the element the keystroke listener was registered at. Type
	 *            is the original type of the UI-element sent the event. So it
	 *            is UI-Kit specific.
	 * @param target
	 *            - the element where the keystroke occurred. Type is the
	 *            original type of the UI-element sent the event. So it is
	 *            UI-Kit specific.
	 * @param yLayout
	 *            the y layout
	 */
	void focus(Object source, Object target, YStrategyLayout yLayout);

	/**
	 * Needs to return the keystroke definition used to observe key strokes that
	 * invoke focusing at this strategy. These key strokes are being ignored, if
	 * there is a more special definition of them in the StrategyLayout.
	 *
	 * @return the key stroke definition
	 */
	KeyStrokeDefinition getKeyStrokeDefinition();
	
	/**
	 * Focus a field by id or the first focusable field if null.
	 *
	 * @param yView the y view
	 * @param fieldId the field id
	 */
	void focusField(YView yView, String fieldId);
}
